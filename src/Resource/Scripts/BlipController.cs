﻿using CitizenFX.Core;
using System.Threading;
using System.Threading.Tasks;
using static CitizenFX.Core.Native.API;
using static Resource.ResourceAPI;

namespace Resource.Scripts
{
    public class BlipController
    {

        public async Task Execute(object[] args, CancellationToken token)
        {

            while (!token.IsCancellationRequested)
            {
                if (HasCharacter())
                {
                    if (Game.PlayerPed.IsInVehicle())
                    {
                        var currentVehicle = Game.PlayerPed.CurrentVehicle;
                        var personalVehicle = GetPersonalVehicle();
                        if (personalVehicle != null)
                        {
                            if (currentVehicle == personalVehicle)
                            {
                                foreach (var blip in currentVehicle.AttachedBlips)
                                {
                                    blip.Alpha = 0;
                                }
                            }
                        }
                    }
                    else
                    {
                        var personalVehicle = GetPersonalVehicle();
                        if (personalVehicle != null)
                        {
                            foreach (var blip in personalVehicle.AttachedBlips)
                            {
                                blip.Alpha = 255;
                            }
                        }
                    }
                }
                await Delay(0);
            }
        }
    }
}
