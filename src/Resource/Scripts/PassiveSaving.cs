﻿using CitizenFX.Core;
using System;
using System.Threading;
using System.Threading.Tasks;
using static CitizenFX.Core.Native.API;
using static Resource.ResourceAPI;

namespace Resource.Scripts
{
    public class PassiveSaving
    {

        public async Task Execute(object[] args, CancellationToken token)
        {
            var nextSaveTime = DateTime.UtcNow.AddMinutes(1);
            while (!token.IsCancellationRequested)
            {
                if (DateTime.UtcNow > nextSaveTime)
                {
                    var id = "PlayerInfo";
                    if (DoesDataFileExist(id))
                    {
                        SelectDataFile(id);
                        SetDataFileValue("PlayerPosition", Game.PlayerPed.Position);
                    }
                    StartNewScript("SaveNow");
                    while (GetInstanceCountOfScript("SaveNow") > 0)
                    {
                        await Delay(1);
                    }
                    nextSaveTime = DateTime.UtcNow.AddMinutes(1);
                }
                await Delay(0);
            }

        }
    }
}
