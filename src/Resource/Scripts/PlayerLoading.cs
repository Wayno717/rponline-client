﻿
using CitizenFX.Core;
using System.Threading;
using System.Threading.Tasks;
using static CitizenFX.Core.Native.API;
using static Resource.ResourceAPI;

namespace Resource.Scripts
{
    internal class PlayerLoading
    {
        public async Task Execute(object[] args, CancellationToken token)
        {

            if (!IsSaveInProgress())
            {
                // attempt to get the player id
                // fish for the save files from the player id
                StartNewScript("LoadDefaultOnline");
                while (GetInstanceCountOfScript("LoadDefaultOnline") > 0)
                {
                    await Delay(1);
                }
                await PullSaveFromCloud(); // Sends a load request to the server
                if (HasSaveLoaded())
                {
                    int character = CreateNewCharacter();
                    SetCharacter(character);

                    if (DoesDataFileExist("PlayerInfo"))
                    {
                        SelectDataFile("PlayerInfo");
                        Game.PlayerPed.Health = GetDataFileValue<int>("PlayerHealth");
                        Game.PlayerPed.Position = GetDataFileValue<Vector3>("PlayerPosition");
                        SetCharacterBankBalance(GetDataFileValue<long>("BankBalance"));
                        SetCharacterWalletBalance(GetDataFileValue<long>("WalletBalance"));
                    }
                    else
                    {
                        LogDebug("PlayerStats does not exist");
                    }
                    LogDebug(GetSelectedDataFile());

                    if (DoesDataFileExist("PlayerStats"))
                    {
                        SelectDataFile("PlayerStats");
                        var x = GetDataFileValue<int>("MP0_WALLET_BALANCE");
                        var y = GetDataFileValue<int>("BANK_BALANCE");

                        SetStatLong("WALLET_BALANCE", x);
                        SetStatLong("BANK_BALANCE", x);
                    }
                    else
                    {
                        LogDebug("PlayerStats does not exist");
                    }
                    LogDebug(GetSelectedDataFile());

                    if (DoesDataFileExist("CharacterLooks"))
                    {
                        SelectDataFile("CharacterLooks");
                        var mother = GetDataFileValue<int>("Mother");
                        LogDebug("DASDSAUIODHBAUIDJABNDJASDDANSJKDBNSADJK:::: " + mother);
                        var father = GetDataFileValue<int>("Father");
                        var resemblence = GetDataFileValue<float>("Resemblance");
                        var skintone = GetDataFileValue<float>("SkinTone");

                        SetPedLooks(Game.PlayerPed.Handle, mother,
                            father,
                             resemblence * 0.1f,
                           skintone * 0.1f);
                    }
                    else
                    {
                        LogDebug("CharacterLooks does not exist");
                    }
                    LogDebug(GetSelectedDataFile());

                    if (DoesDataFileExist("PlayerOutfit"))
                    {
                        //EngineAPI.SelectDataFile("PlayerOutfit");
                        //var outfit = EngineAPI.GetDataFileValue<CharacterOutfit>("CharacterOutfit");
                        //var components = outfit.Components;
                        //for (int i = 0; i < components.Length; i++)
                        //{
                        //    var component = components[i];
                        //    API.SetPedComponentVariation(Game.PlayerPed.Handle, i, component.ComponentIndex, component.ComponentTexture, component.ComponentPallet);

                        //}
                    }
                    else
                    {
                        LogDebug("PlayerOutfit does not exist");
                    }

                    LogDebug(GetSelectedDataFile());

                    if (DoesDataFileExist("PlayerVehicle"))
                    {
                        SelectDataFile("PlayerVehicle");
                        if (DoesDataFileValueExist("VehicleHash"))
                        {
                            LogDebug("vehicleASD:::: ");
                            var pv = (VehicleHash)(uint)GetDataFileValue<int>("VehicleHash");
                            LogDebug("vehicleASD:::: " + pv);
                            var position = GetDataFileValue<Vector3>("VehiclePosition");
                            var vehicle = await World.CreateVehicle(new Model(pv), Game.PlayerPed.Position);
                            vehicle.PlaceOnNextStreet();
                            vehicle.IsPersistent = true;
                            if (vehicle.AttachedBlips.Length == 0)
                                vehicle.AttachBlip();
                            SetCharacterPersonalVehicle(vehicle.Handle);
                        }
                    }
                    else
                    {
                        LogDebug("PlayerVehicle does not exist");
                    }

                    LogDebug(GetSelectedDataFile());
                    if (DoesDataFileExist("PlayerWeapon"))
                    {
                        SelectDataFile("PlayerWeapon");
                        if (DoesDataFileValueExist("WeaponHash"))
                        {
                            var hash = (WeaponHash)GetDataFileValue<uint>("WeaponHash");
                            var ammo = GetDataFileValue<int>("WeaponAmmo");
                            Game.PlayerPed.Weapons.Give(hash, ammo, true, true);
                        }
                    }
                    else
                    {
                        LogDebug("PlayerWeapon does not exist");
                    }
                    LogDebug(GetSelectedDataFile());

                    StartNewScript("LoadStats");

                }
                else
                {
                    LogDebug("No save file to load from, attempting to create a save...");
                    /// If the player doesnt have basic info, that means the player does not have a save
                    if (!DoesDataFileExist("PlayerInfo"))
                    {
                        StartNewScript("PlayerSetup");
                        while (GetInstanceCountOfScript("PlayerSetup") > 0)
                        {
                            await Delay(1);
                        }
                        StartNewScript("SaveNow");
                        while (GetInstanceCountOfScript("SaveNow") > 0)
                        {
                            await Delay(1);
                        }
                    }
                    StartNewScript("PlayerLoading");
                }

            }

        }

    }
}
