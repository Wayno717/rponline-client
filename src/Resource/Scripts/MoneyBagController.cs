﻿using CitizenFX.Core;
using CitizenFX.Core.Native;
using System.Threading;
using static CitizenFX.Core.Native.API;
using static Resource.ResourceAPI;
using System.Threading.Tasks;

namespace Resource.Scripts
{
    public class MoneyBagController
    {
        private bool _updated;

        public async Task Execute(object[] args, CancellationToken token)
        {
            // Dupe protection
            if (GetInstanceCountOfScript("MoneyBagController") > 1)
                return;

            while (!token.IsCancellationRequested)
            {
                if (GetEventExists(this, "CEventNetworkPlayerCollectedAmbientPickup"))
                {
                    var test = GetEventData(this, "CEventNetworkPlayerCollectedAmbientPickup");
                    foreach (var item in test)
                    {
                        LogDebug(item);
                    }
                    var id = int.Parse(test[3].ToString());
                    var money = int.Parse(test[1].ToString());
                    if (id == GetHashKey("xm_prop_x17_bag_01b"))
                    {
                        if (GetPedDrawableVariation(Game.PlayerPed.Handle, 5) != 45)
                        {
                            GiveMoneyBag();
                        }
                        AddValueToWalletBalance(money);
                    }
                }

                if (GetCharacterWalletBalance() > GetCharacterMaxWalletBalance())
                {
                    var _value = (int)(GetCharacterWalletBalance() - GetCharacterMaxWalletBalance());
                    SubtractValueFromWalletBalance(_value);
                    var pickup = await World.CreateAmbientPickup(PickupType.MoneyDepBag, Game.PlayerPed.Position + Game.PlayerPed.ForwardVector * 2, new Model("xm_prop_x17_bag_01b"), _value);
                    pickup.AttachBlip();
                    pickup.IsPersistent = true;
                }

                if (GetPedDrawableVariation(Game.PlayerPed.Handle, 5) == 45 && !_updated)
                {
                    SetCharacterMaxWalletBalance(1000000);
                    _updated = true;
                }
                else if (GetPedDrawableVariation(Game.PlayerPed.Handle, 5) != 45 && _updated)
                {
                    SetCharacterMaxWalletBalance(1000);
                    _updated = false;
                }

                await Delay(0);
            }
        }

        private void GiveMoneyBag()
        {
            SetPedComponentVariation(Game.PlayerPed.Handle, 5, 45, 0, 0);
        }
    }
}
