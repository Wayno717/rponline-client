﻿using CitizenFX.Core;
using System.Threading;
using System.Threading.Tasks;
using static CitizenFX.Core.Native.API;
using static Resource.ResourceAPI;

namespace Resource.Scripts.Testing
{
    public class MarkerTest
    {

        public async Task Execute(object[] args, CancellationToken token)
        {
            // Dupe protection
            if (GetInstanceCountOfScript("MarkerTest") > 1)
                return;

            var handle = CreateMarker(Game.PlayerPed.Position);

            while (!token.IsCancellationRequested)
            {
                DrawMarker(handle);
                if (IsInMarker(handle, Game.PlayerPed.Handle))
                {
                    break;
                }
                await Delay(0);
            }
            DeleteMarker(handle);
        }
    }
}
