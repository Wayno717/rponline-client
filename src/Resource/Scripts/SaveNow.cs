﻿using CitizenFX.Core.UI;
using System.Threading;
using System.Threading.Tasks;
using static CitizenFX.Core.Native.API;
using static Resource.ResourceAPI;


namespace Resource.Scripts
{
    public class SaveNow
    {

        public async Task Execute(object[] args, CancellationToken token)
        {
            // Dupe protection
            if (GetInstanceCountOfScript("SaveNow") > 1)
                return;
            Screen.LoadingPrompt.Show("Saving...", LoadingSpinnerType.SocialClubSaving);
            await SendSaveToCloud();
            await Delay(1000);
            Screen.LoadingPrompt.Hide();
        }
    }
}
