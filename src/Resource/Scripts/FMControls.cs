﻿using CitizenFX.Core;
using System.Threading;
using System.Threading.Tasks;
using static CitizenFX.Core.Native.API;
using static Resource.ResourceAPI;

namespace Resource.Scripts
{
    public class FMControls
    {
        public async Task Execute(object[] args, CancellationToken token)
        {
            while (!token.IsCancellationRequested)
            {
                if (Game.IsControlJustReleased(0, Control.InteractionMenu))
                {
                    StartNewScript("UIInteractionMenu");
                }
                await Delay(0);
            }
        }
    }
}
