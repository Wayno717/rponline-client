﻿
using Proline.OnlineEngine.Core;

using System.Threading;
using System.Threading.Tasks;
using static CitizenFX.Core.Native.API;
using static Resource.ResourceAPI;

namespace Resource.Scripts
{
    public class LoadStats
    {
        public async Task Execute(object[] args, CancellationToken token)
        {
            if (HasCharacter())
            {
                var stats = GetCharacterStats();
                if (stats == null)
                    return;
                var walletBalanceStat = MPStat.GetStat<long>("MP0_WALLET_BALANCE");
                var bankBalanceStat = MPStat.GetStat<long>("BANK_BALANCE");


                var walletBalance = stats["WALLET_BALANCE"];
                var bankBalance = stats["BANK_BALANCE"];

                LogDebug(walletBalance);
                LogDebug(bankBalance);

                walletBalanceStat.SetValue(GetCharacterWalletBalance());
                bankBalanceStat.SetValue(GetCharacterBankBalance());
            }
        }
    }
}
