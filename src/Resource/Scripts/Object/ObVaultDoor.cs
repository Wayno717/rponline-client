using CitizenFX.Core;
using CitizenFX.Core.Native;
using CitizenFX.Core.UI;
using System.Threading;
using System.Threading.Tasks;
using static CitizenFX.Core.Native.API;
using static Resource.ResourceAPI;

namespace Resource.Scripts.Object
{
    public class ObVaultDoor
    {
        public ObVaultDoor()
        {
        }

        public Entity LocalEntity { get; set; }

        public async Task Execute(object[] args, CancellationToken token)
        {
            var handle = (int)args[0];
            LocalEntity = new Prop(handle);

            //API.DoorControl((uint)LocalEntity.Model.Hash, LocalEntity.Position.X, LocalEntity.Position.Y, LocalEntity.Position.Z, true,
            //    1f, 1f, 1f);

            while (!token.IsCancellationRequested)
            {
                Screen.DisplayHelpTextThisFrame("Press ~INPUT_CONTEXT~ to open the vault door");

                if (Game.IsControlJustPressed(0, Control.Context))
                {
                    CreateModelSwap(LocalEntity.Position.X, LocalEntity.Position.Y, LocalEntity.Position.Z, 1f,
                   (uint)LocalEntity.Model.Hash, (uint)GetHashKey("hei_prop_heist_sec_door"), true);
                    await Delay(5000);
                    RequestAnimDict("anim@heists@fleeca_bank@bank_vault_door");
                    while (!HasAnimDictLoaded("anim@heists@fleeca_bank@bank_vault_door"))
                        await Delay(0);
                    PlayEntityAnim(LocalEntity.Handle, "bank_vault_door_opens", "anim@heists@fleeca_bank@bank_vault_door", 4f,
                        false, true, false, 0f, 8);
                    ForceEntityAiAndAnimationUpdate(LocalEntity.Handle);
                    RequestScriptAudioBank("vault_door", false);
                    PlaySoundFromCoord(-1, "vault_unlock", Game.PlayerPed.Position.X,
                        Game.PlayerPed.Position.Y, Game.PlayerPed.Position.Z,
                        "dlc_heist_fleeca_bank_door_sounds", false, 0, false);
                    await Delay(500);
                    LogDebug("Checking if the animation is playing\n");
                    while (IsEntityPlayingAnim(LocalEntity.Handle, "anim@heists@fleeca_bank@bank_vault_door",
                        "bank_vault_door_opens", 3))
                    {
                        LogDebug("Animation is playing\n");
                        if (GetEntityAnimCurrentTime(LocalEntity.Handle, "anim@heists@fleeca_bank@bank_vault_door",
                                "bank_vault_door_opens") >= 1f)
                        {
                            var test = GetEntityRotation(LocalEntity.Handle, 2) + new Vector3(0, 0, -80);
                            FreezeEntityPosition(LocalEntity.Handle, true);
                            StopEntityAnim(LocalEntity.Handle, "bank_vault_door_opens",
                                "anim@heists@fleeca_bank@bank_vault_door", -1000f);
                            SetEntityRotation(LocalEntity.Handle, test.X, test.Y, test.Z, 2, true);
                            ForceEntityAiAndAnimationUpdate(LocalEntity.Handle);
                            SetModelAsNoLongerNeeded((uint)GetHashKey("hei_prop_heist_sec_door"));
                            LogDebug("Finished");
                        }

                        await Delay(0);
                    }
                    break;
                }
                await Delay(0);
            }
        }
    }
}