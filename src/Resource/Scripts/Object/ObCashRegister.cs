using CitizenFX.Core;
using CitizenFX.Core.Native;
using CitizenFX.Core.UI;
using System.Threading;
using System.Threading.Tasks;
using static CitizenFX.Core.Native.API;
using static Resource.ResourceAPI;

namespace Resource.Scripts.Object
{
    public class ObCashRegister
    {

        public ObCashRegister()
        {
        }

        public Prop LocalEntity { get; private set; }

        public async Task Execute(object[] args, CancellationToken token)
        {

            var handle = (int)args[0];
            LocalEntity = new Prop(handle);
            while (!token.IsCancellationRequested)
            {
                if (LocalEntity.Exists())
                {
                    if (LocalEntity.Model == GetHashKey("prop_till_01_dam"))
                    {
                        LogDebug(((uint)LocalEntity.Model.Hash).ToString());
                        LogDebug(((uint)LocalEntity.Model.Hash).ToString());
                    }

                    if (!LocalEntity.HasBeenDamagedByAnyWeapon())
                    {
                        Screen.DisplayHelpTextThisFrame("Press ~INPUT_CONTEXT~ To do something");
                        if (Game.IsControlJustPressed(0, Control.Context))
                        {
                            CreateModelSwap(LocalEntity.Position.X, LocalEntity.Position.Y, LocalEntity.Position.Z,
                                1f,
                                (uint)GetHashKey("prop_till_01"), (uint)GetHashKey("prop_till_01_dam"), true);
                            await World.CreateAmbientPickup(PickupType.MoneyDepBag,
                                GetSafePickupCoords(LocalEntity.Position.X, LocalEntity.Position.Y,
                                    LocalEntity.Position.Z, 0,
                                    0), new Model("prop_money_bag_01"), 0);
                            RemoveModelSwap(LocalEntity.Position.X, LocalEntity.Position.Y, LocalEntity.Position.Z,
                                1f,
                                (uint)GetHashKey("prop_till_01"), (uint)GetHashKey("prop_till_01_dam"), true);

                            MarkScriptAsNoLongerNeeded(this);
                            TerminateScriptInstance(this);
                        }
                    }
                    else
                    {
                        await World.CreateAmbientPickup(PickupType.MoneyDepBag,
                            GetSafePickupCoords(LocalEntity.Position.X, LocalEntity.Position.Y, LocalEntity.Position.Z,
                                0, 0),
                            new Model("prop_money_bag_01"), 0);

                        MarkScriptAsNoLongerNeeded(this);
                        TerminateScriptInstance(this);
                    }
                }
                else
                {
                    MarkScriptAsNoLongerNeeded(this);
                    TerminateScriptInstance(this);
                }
                await Delay(0);
            }

        }
    }
}