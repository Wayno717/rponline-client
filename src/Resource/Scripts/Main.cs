﻿using CitizenFX.Core;
using CitizenFX.Core.Native;
using CitizenFX.Core.UI;
using System.Threading;
using System.Threading.Tasks;
using static CitizenFX.Core.Native.API;
using static Resource.ResourceAPI;

namespace Resource.Scripts
{
    public class Main
    {

        public async Task Execute(object[] args, CancellationToken token)
        {


            var state = 0;
            // Setup the world
            SetInstancePriorityMode(true);
            SetInstancePriorityHint(3);
            // Enable Trains





            SwitchTrainTrack(0, true);
            SwitchTrainTrack(3, true);
            SetTrainTrackSpawnFrequency(0, 120000);
            SetRandomTrains(true);
            while (!token.IsCancellationRequested)
            {
                switch (state)
                {
                    case 0:
                        {
                            DoScreenFadeOut(100);
                            ResurrectPed(Game.PlayerPed.Handle);
                            SwitchOutPlayer(Game.PlayerPed.Handle, 1, 1);
                            SetEntityCoordsNoOffset(Game.PlayerPed.Handle, -1336.161f, -3044.03f, 13.94f, false, false, false);
                            FreezeEntityPosition(Game.PlayerPed.Handle, true);
                            Game.PlayerPed.Position = new Vector3(0, 0, 70);
                            Screen.LoadingPrompt.Show("Loading Freemode...");
                            await Delay(5000);
                            state = 1;
                        }
                        break;
                    case 1:
                        {
                            Screen.LoadingPrompt.Hide();
                            ShutdownLoadingScreen();
                            SetNoLoadingScreen(true);
                            DoScreenFadeIn(500);

                            if (GetInstanceCountOfScript("UIMainMenu") == 0)
                            {
                                StartNewScript("UIMainMenu");
                                while (GetInstanceCountOfScript("UIMainMenu") > 0)
                                {
                                    await Delay(0);
                                }
                            }

                            if (ResourceAPI.GetInstanceCountOfScript("CharacterCreator") > 0)
                            {
                                state = 999;
                                break;
                            }
                            else if (ResourceAPI.GetInstanceCountOfScript("PlayerLoading") > 0)
                            {
                                while (ResourceAPI.GetInstanceCountOfScript("PlayerLoading") > 0)
                                {
                                    await ResourceAPI.Delay(0);
                                }
                                FreezeEntityPosition(Game.PlayerPed.Handle, false);
                                SwitchInPlayer(Game.PlayerPed.Handle);
                                while (IsPlayerSwitchInProgress())
                                {
                                    await Delay(0);
                                }
                                state = 2;
                            }

                        }
                        break;
                    case 2:
                        {
                            //EngineAPI.StartNewScript("ReArmouredTruck");
                            StartNewScript("Freemode");
                            StartNewScript("FMVechicleExporter");
                            StartNewScript("PlayerDeath");
                            StartNewScript("UIPlayerSwitch");
                            StartNewScript("FMControls");
                            StartNewScript("VehicleFuel");
                            StartNewScript("PassiveSaving");
                            StartNewScript("UIFreemodeHUD");
                            StartNewScript("BlipController");
                            StartNewScript("CharacterApts");
                            StartNewScript("MoneyBagController");
                            LogDebug($"Calling Task ID for API {Thread.CurrentThread.ManagedThreadId}");
                        }
                        state = 3;
                        break;
                    case 3:

                        //if(API.GetInteriorFromEntity(Game.PlayerPed.Handle) > 0)
                        //{
                        //    if (Game.PlayerPed.IsInVehicle())
                        //    {
                        //        if(Game.IsControlJustPressed(0, Control.MoveUpDown))
                        //        {
                        //            Game.PlayerPed.CurrentVehicle.Position = new Vector3(0, 0, 70);
                        //        }
                        //    }
                        //}
                        break;
                    case 999:
                        while (GetInstanceCountOfScript("CharacterCreator") > 0)
                        {
                            await Delay(1);
                        }


                        while (GetInstanceCountOfScript("StartIntro") > 0)
                        {
                            await Delay(1);
                        }
                        state = 2;

                        break;
                }
                if (state == 3)
                    break;

                await Delay(0);
            }
        }
    }
}


// Game.PlayerPed.Weapons.Give(WeaponHash.Parachute, 0, true, true);
// Game.PlayerPed.Position = new Vector3(0, 0, 1800);
// Game.PlayerPed.HasGravity = true;
// API.SetGravityLevel(2);
// log.Debug(json);