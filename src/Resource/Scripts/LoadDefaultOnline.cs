﻿using CitizenFX.Core;
using System.Threading;
using System.Threading.Tasks;
using static CitizenFX.Core.Native.API;
using static Resource.ResourceAPI;

namespace Resource.Scripts
{
    public class LoadDefaultOnline
    {
        public async Task Execute(object[] args, CancellationToken token)
        {
            await Game.Player.ChangeModel(new Model(1885233650));
            if (!HasSaveLoaded())
            {
                StartNewScript("LoadDefaultStats");
                while (GetInstanceCountOfScript("LoadDefaultStats") > 0)
                {
                    await Delay(1);
                }
                SetCharacter(CreateNewCharacter());
            }

            SetPedOutfit("mp_m_defaultoutfit", Game.PlayerPed.Handle);
        }


    }
}
