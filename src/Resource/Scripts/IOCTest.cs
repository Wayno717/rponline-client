﻿using System.Threading;
using System.Threading.Tasks;
using static CitizenFX.Core.Native.API;
using static Resource.ResourceAPI;

namespace Resource.Scripts
{
    public class IOCTest
    {

        public async Task Execute(object[] args, CancellationToken token)
        {
            // Dupe protection
            if (GetInstanceCountOfScript("IOCTest") > 1)
                return;


            while (!token.IsCancellationRequested)
            {
                DeletePersonalVehicle();
                await Delay(0);
            }
        }
    }
}
