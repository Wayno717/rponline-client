﻿using Common.Logging;
using Resource.Component;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Resource
{
    public static partial class ResourceAPI
    {
        private static readonly ILog log = Proline.Resource.Logging.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType); 

        private static T GetComponent<T>()
        {
            var component = ResourceScriptImpl.GetComponent<T>();
            //log.Debug(component.GetHashCode());
            return component;
        }
    }
}
