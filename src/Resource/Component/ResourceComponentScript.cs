using CitizenFX.Core.Native;
using Common.Logging; 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Resource.Component
{
    public abstract class ResourceComponentScript 
    {
        protected static ILog log = Proline.Resource.Logging.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public abstract Task OnExecute();
    }
}
