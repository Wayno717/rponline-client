﻿using Proline.Resource.Framework;
using Resource.Component.CShopCatalogue.Commands;
using Resource.Component.CShopCatalogue.Scripts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Resource.Component;
using Resource.Component.CGameLogic;
using CitizenFX.Core;
using Newtonsoft.Json;
using Resource.Component.CDataStream;

namespace Resource.Component.CShopCatalogue
{
    internal class CShopCatalogueComponent :  ResourceComponent, ICShopCatalogueComponent
    {
        private ResourceScript script;

        public CShopCatalogueComponent(ResourceScript script) : base(nameof(CShopCatalogueComponent), null)
        {
            this.script = script;
        }
        public override void OnLoad()
        {
            RegisterScript<InitCore>();

            script.RegisterCommand<BuyRandomOutfitCommand>();
            script.RegisterCommand<BuyRandomVehicleCommand>();
            script.RegisterCommand<BuyRandomWeaponCommand>();
            script.RegisterCommand<BuyVehicleCommand>();
        }

        public override void OnStart()
        {

        }
    }
}
