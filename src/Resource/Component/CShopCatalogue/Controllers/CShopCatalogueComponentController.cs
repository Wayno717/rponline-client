﻿using CitizenFX.Core;
using Common.Logging;
using Newtonsoft.Json;
using Proline.Resource.Framework;
using Resource.Component.CDataStream;
using Resource.Component.CGameLogic;
using System;
using System.Threading.Tasks;

namespace Resource.Component.CShopCatalogue
{
    internal class CShopCatalogueComponentController : ICShopCatalogueComponentController
    {
        private readonly ICGameLogicComponentController gameComponent;
        private readonly ICDataStreamComponentController dataComponent;

        protected static ILog log = Proline.Resource.Logging.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public CShopCatalogueComponentController(ICGameLogicComponentController gameComponent, ICDataStreamComponentController dataComponent)
        {
            this.gameComponent = gameComponent;
            this.dataComponent = dataComponent; 
        }

        public void BuyVehicle(string vehicleName)
        {
            try
            {
                if (gameComponent.HasCharacter())
                {
                    var manager = CatalougeManager.GetInstance();
                    var catalouge = manager.GetCatalouge("VehicleCatalouge");
                    if (catalouge == null)
                        throw new Exception("Catalouge not found");
                    var vci = (VehicleCatalougeItem)catalouge.GetItem(vehicleName);

                    if (vci == null)
                        throw new Exception("Catalouge Item not found");
                    if (gameComponent.HasBankBalance(vci.Price))
                    {
                        var currentVehicle = gameComponent.GetPersonalVehicle();
                        if (currentVehicle != null)
                        {
                            foreach (var item in currentVehicle.AttachedBlips)
                            {
                                item.Delete();
                            }
                            currentVehicle.IsPersistent = false;
                            currentVehicle.Delete();
                        }


                        var task = World.CreateVehicle(new Model(vci.Model), World.GetNextPositionOnStreet(Game.PlayerPed.Position));
                        task.ContinueWith(e =>
                        {
                            var createdVehicle = e.Result;
                            gameComponent.SetCharacterPersonalVehicle(createdVehicle.Handle);

                            var id = "PlayerVehicle";
                            dataComponent.CreateDataFile();
                            dataComponent.AddDataFileValue("VehicleHash", createdVehicle.Model.Hash);
                            dataComponent.AddDataFileValue("VehiclePosition", JsonConvert.SerializeObject(createdVehicle.Position));
                            createdVehicle.IsPersistent = true;
                            if (createdVehicle.AttachedBlips.Length == 0)
                                createdVehicle.AttachBlip();
                            dataComponent.SaveDataFile(id);
                            gameComponent.SubtractValueFromBankBalance(vci.Price);
                        });

                    }
                }

            }
            catch (Exception e)
            {
                log.Error(e);
            }
        }

        public async Task DeleteVehicle(string name)
        {
            //try
            //{
            //    var client = new EventClient("rponline-server");
            //    var x = await client.MakeRequest("DeleteItem", new DeleteItemRequest() { Value = name });
            //}
            //catch (Exception e)
            //{

            //    throw;
            //}
        }



        public async Task<string> GetVehicleNames()
        {
            //try
            //{
            //    var result = "";
            //    var client = new EventClient("rponline-server");
            //    var args = await client.MakeRequest("ListItems", new ListItemsRequest());
            //    var xy = args.ToString();
            //    var list = JsonConvert.DeserializeObject<List<ShopItem>>(xy);
            //    var names = list.Select(y => y.Name).ToArray();
            //    result = string.Join(",", names);
            //    return result;
            //}
            //catch (Exception e)
            //{

            //    log.Error(e);
            //}
            return "";
        }

        public async Task PurchuseVehicle(string name)
        {
            //try
            //{
            //    // var ec = new EventClient("ResoureName")
            //    // var result = await ec.MakeRequest("PurchuseVehicle", new object())
            //    // MakeRequest 
            //    // - Take object in, create new EventRequest() { }
            //    // define a callback name, Resource/CallingMethodName/GUID 
            //    // EventHandlers.Add("TemperaryEventName", new Action<Player, string>(HandleResponse));
            //    // Basescript.TriggerServerEvent("Resource/Game/TargetAPIName", jsonOfRequest)
            //    // Awaits result, timeout of 10 seconds

            //    // HandleResponse will sit idle and once it gets a response, will fill the respective response object

            //    var client = new EventClient("rponline-server");
            //    var x = await client.MakeRequest("DeleteItem", new DeleteItemRequest() { Value = name });
            //}
            //catch (Exception e)
            //{

            //    throw;
            //}
        }

        public async Task PutVehicle(string name)
        {
            try
            {
                //var client = new EventClient("rponline-server");
                //var x = await client.MakeRequest("PutItem", new PutItemRequest() { Value = name });
            }
            catch (Exception e)
            {

                throw;
            }
        }
    }
}