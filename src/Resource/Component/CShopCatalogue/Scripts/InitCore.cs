﻿
using Newtonsoft.Json;
using Proline.Resource.Framework;
using Proline.Resource.IO;
using System.Threading.Tasks;
using Resource.Component;

namespace Resource.Component.CShopCatalogue.Scripts
{
    internal class InitCore : ResourceComponentScript
    {
        public override async Task OnExecute()
        {
            var data = ResourceFile.ReadAll("data/catalogue/catalogue-vehicles.json");

            log.Debug(data);
            CatalougeManager.GetInstance().Register("VehicleCatalouge", JsonConvert.DeserializeObject<VehicleCatalouge>(data));



        }
    }
}
