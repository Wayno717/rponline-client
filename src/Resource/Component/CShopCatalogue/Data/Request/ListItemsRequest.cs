﻿namespace Resource.Component.CShopCatalogue.Data.Request
{
    public class ListItemsRequest
    {
        public string Value { get; set; }
    }
}
