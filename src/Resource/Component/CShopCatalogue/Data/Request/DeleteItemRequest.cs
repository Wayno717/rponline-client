﻿namespace Resource.Component.CShopCatalogue.Data.Request
{
    public class DeleteItemRequest
    {
        public string Value { get; set; }
    }
}
