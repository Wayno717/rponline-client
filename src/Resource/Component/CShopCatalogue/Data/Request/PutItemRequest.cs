﻿namespace Resource.Component.CShopCatalogue.Data.Request
{
    public class PutItemRequest
    {
        public string Value { get; set; }
    }
}
