﻿namespace Resource.Component.CShopCatalogue.Data.Response
{
    public class ListItemsResponse
    {
        public string Items { get; set; }
    }
}
