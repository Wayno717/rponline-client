﻿namespace Resource.Component.CShopCatalogue
{
    public class ShopItem
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public long Price { get; set; }
    }
}
