﻿using Common.Logging;
using Proline.Resource.Framework;

namespace Resource.Component.CShopCatalogue.Commands
{
    public class BuyVehicleCommand : ResourceCommand
    {
        //private static readonly ILog log = Proline.Resource.Logging.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public BuyVehicleCommand()
        {
        }

        protected override void OnCommandExecute(params object[] args)
        {
            if (args.Length > 0)
            {
                var vehicle = args[0].ToString();
                ResourceAPI.BuyVehicle(vehicle);
            }

        }
    }
}
