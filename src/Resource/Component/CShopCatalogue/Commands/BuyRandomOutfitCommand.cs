﻿using CitizenFX.Core;
using Proline.Resource.Framework;

namespace Resource.Component.CShopCatalogue.Commands
{
    public class BuyRandomOutfitCommand : ResourceCommand
    {
        public BuyRandomOutfitCommand()
        {
        }

        protected override void OnCommandExecute(params object[] args)
        {
            if (ResourceAPI.GetCharacterBankBalance() > 250)
            {
                Game.Player.Character.Style.RandomizeOutfit();
                Game.Player.Character.Style.RandomizeProps();
                ResourceAPI.SetCharacterBankBalance(250);
            }
        }
    }
}
