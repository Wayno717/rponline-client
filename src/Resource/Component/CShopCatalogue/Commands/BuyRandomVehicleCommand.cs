﻿using CitizenFX.Core;
using Newtonsoft.Json;


using Proline.Resource.Framework;
using System;

namespace Resource.Component.CShopCatalogue.Commands
{
    public class BuyRandomVehicleCommand : ResourceCommand
    {
        public BuyRandomVehicleCommand()
        {
        }

        protected override void OnCommandExecute(params object[] args)
        {

            if (ResourceAPI.GetCharacterBankBalance() > 250)
            {
                if (ResourceAPI.HasCharacter())
                {
                    if (ResourceAPI.GetPersonalVehicle() != null)
                    {
                        ResourceAPI.DeletePersonalVehicle();
                    }


                    ResourceAPI.SetCharacterBankBalance(250);
                    Array values = Enum.GetValues(typeof(VehicleHash));
                    Random random = new Random();
                    VehicleHash randomBar = (VehicleHash)values.GetValue(random.Next(values.Length));
                    var task = World.CreateVehicle(new Model(randomBar), World.GetNextPositionOnStreet(Game.PlayerPed.Position));
                    task.ContinueWith(e =>
                    {
                        var vehicle = e.Result;
                        ResourceAPI.SetCharacterPersonalVehicle(vehicle.Handle);

                        var id = "PlayerVehicle";
                        ResourceAPI.CreateDataFile();
                        ResourceAPI.AddDataFileValue("VehicleHash", vehicle.Model.Hash);
                        ResourceAPI.AddDataFileValue("VehiclePosition", JsonConvert.SerializeObject(vehicle.Position));
                        vehicle.IsPersistent = true;
                        if (vehicle.AttachedBlips.Length == 0)
                            vehicle.AttachBlip();
                        ResourceAPI.SaveDataFile(id);

                    });
                }


            }
        }
    }
}
