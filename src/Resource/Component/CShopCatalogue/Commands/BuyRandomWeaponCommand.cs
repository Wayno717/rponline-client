﻿using CitizenFX.Core;
using Proline.Resource.Framework;
using System;

namespace Resource.Component.CShopCatalogue.Commands
{
    public class BuyRandomWeaponCommand : ResourceCommand
    {
        public BuyRandomWeaponCommand()
        {
        }

        protected override void OnCommandExecute(params object[] args)
        {
            if (ResourceAPI.GetCharacterBankBalance() > 250)
            {

                Array values = Enum.GetValues(typeof(WeaponHash));
                Random random = new Random();
                WeaponHash randomBar = (WeaponHash)values.GetValue(random.Next(values.Length));
                var ammo = random.Next(1, 250);
                Game.PlayerPed.Weapons.Give(randomBar, ammo, true, true);

                var id = "PlayerWeapon";
                ResourceAPI.CreateDataFile();
                ResourceAPI.AddDataFileValue("WeaponHash", randomBar);
                ResourceAPI.AddDataFileValue("WeaponAmmo", ammo);
                ResourceAPI.SaveDataFile(id);

                ResourceAPI.SubtractValueFromBankBalance(250);
            }
        }
    }
}
