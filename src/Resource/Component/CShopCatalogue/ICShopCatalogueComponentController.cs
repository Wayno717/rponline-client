﻿using System.Threading.Tasks;

namespace Resource.Component.CShopCatalogue
{
    public interface ICShopCatalogueComponentController 
    {
        void BuyVehicle(string vehicleName);
        Task DeleteVehicle(string name);
        Task<string> GetVehicleNames();
        Task PurchuseVehicle(string name);
        Task PutVehicle(string name);
    }
}