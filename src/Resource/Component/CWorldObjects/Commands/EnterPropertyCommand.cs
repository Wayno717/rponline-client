﻿using Proline.Resource.Framework;

namespace Resource.Component.CWorldObjects.Commands
{
    public class EnterPropertyCommand : ResourceCommand
    {
        public EnterPropertyCommand()
        {
        }

        protected override void OnCommandExecute(params object[] args)
        {
            if (args.Length == 3)
            {
                ResourceAPI.EnterProperty(args[0].ToString(), args[1].ToString(), args[2].ToString());
            }
        }
    }
}
