﻿using CitizenFX.Core;
using Proline.Resource.Framework;

namespace Resource.Component.CWorldObjects.Commands
{
    public class TpToNeariestEntranceCommand : ResourceCommand
    {
        public TpToNeariestEntranceCommand()
        {
        }

        protected override void OnCommandExecute(params object[] args)
        {
            var entrance = ResourceAPI.GetBuildingPosition(ResourceAPI.GetNearestBuilding());
            Game.PlayerPed.Position = entrance;
        }
    }
}
