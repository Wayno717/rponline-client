﻿using Common.Logging;

using Newtonsoft.Json;
using Proline.Resource.Framework;
using Proline.Resource.IO;
using Resource.Component.CWorldObjects.Data.Ownership;
using System.Collections.Generic;
using System.Reflection;
using System.Threading.Tasks;
using Resource.Component;

namespace Resource.Component.CWorldObjects.Scripts
{
    public class InitCore : ResourceComponentScript
    {

        protected static ILog log = Proline.Resource.Logging.LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public async override Task OnExecute()
        {

            try
            {
                log.Info("CALLEDAS");
                var apartmentData = ResourceFile.ReadAll("data/world/apt_properties.json");
                var pm = PropertyManager.GetInstance();
                var apartmentInteriors = JsonConvert.DeserializeObject<List<PropertyMetadata>>(apartmentData);
                foreach (var item in apartmentInteriors)
                {
                    pm.Register(item.Id, item);
                }
            }
            catch (System.Exception e)
            {
                log.Debug(e);
            }

        }


    }
}
