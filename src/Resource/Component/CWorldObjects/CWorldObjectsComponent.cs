﻿using Proline.Resource.Framework;
using Resource.Component.CWorldObjects.Commands;
using Resource.Component.CWorldObjects.Scripts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Resource.Component;
using Proline.Resource.IO;
using Newtonsoft.Json;
using Resource.Component.CWorldObjects.Data.Ownership;
using CitizenFX.Core;
using CitizenFX.Core.Native;
using Resource.Component.CWorldObjects.Data;
using Common.Logging;

namespace Resource.Component.CWorldObjects
{
    internal class CWorldObjectsComponent : ResourceComponent, ICWorldObjectsComponent
    {
        private ResourceScript script;


        public CWorldObjectsComponent(ResourceScript script) : base(nameof(CWorldObjectsComponent), null)
        {
            this.script = script;
        }

        public override void OnLoad()
        {
            RegisterScript<InitCore>();


            script.RegisterCommand<EnterPropertyCommand>();
            script.RegisterCommand<ExitPropertyCommand>();
            script.RegisterCommand<PopulateSixGarageRandomCommand>();
            script.RegisterCommand<PopulateTenGarageRandomCommand>();
            script.RegisterCommand<PopulateTwoGarageRandomCommand>();
            script.RegisterCommand<TpToNeariestEntranceCommand>();
        }

        public override void OnStart()
        {

        }
    }
}
