﻿using CitizenFX.Core;
using CitizenFX.Core.Native;
using Common.Logging;
using Newtonsoft.Json;
using Proline.Resource.IO;
using Resource.Component.CWorldObjects.Data;
using Resource.Component.CWorldObjects.Data.Ownership;
using System;
using System.Linq;

namespace Resource.Component.CWorldObjects
{
    internal class CWorldObjectsComponentController : ICWorldObjectsComponentController
    {
        protected static ILog log = Proline.Resource.Logging.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public int CreateMarker(Vector3 position, float activationRange = 2f)
        {

            try
            {
                var instance = MarkerManager.GetInstance();
                var marker = new Marker(position, new Vector3(0, 0, 0), Vector3.One, MarkerType.DebugSphere, System.Drawing.Color.FromArgb(150, 255, 255, 255));
                marker.ActivationRange = activationRange;
                return instance.AddMarker(marker);
            }
            catch (Exception e)
            {
                log.Error(e);
            }
            return 0;
        }

        public void DeleteMarker(int handle)
        {

            try
            {
                var instance = MarkerManager.GetInstance();
                var marker = instance.GetMarker(handle);
                instance.RemoveMarker(handle);
            }
            catch (Exception e)
            {
                log.Error(e);
            }
        }

        public void DrawMarker(int handle)
        {

            try
            {
                var instance = MarkerManager.GetInstance();
                var marker = instance.GetMarker(handle);
                marker.Draw();
            }
            catch (Exception e)
            {
                log.Error(e);
            }
        }

        public string EnterBuilding(string buildingId, string buildingEntrance)
        {

            try
            {
                var resourceData2 = ResourceFile.ReadAll($"data/world/buildings/{buildingId}.json");
                var buildingMetaData = JsonConvert.DeserializeObject<BuildingMetadata>(resourceData2);
                var entrance = buildingMetaData.AccessPoints.FirstOrDefault(e => e.Id.Equals(buildingEntrance));
                if (string.IsNullOrEmpty(entrance.Function))
                    return "Apartment";
                return entrance.Function;
            }
            catch (Exception e)
            {
                log.Error(e);
            }
            return "";
        }


        public Vector3 EnterInterior(string interiorId, string entranceId)
        {

            try
            {
                var resourceData2 = ResourceFile.ReadAll($"data/world/interiors/{interiorId}.json");
                var buildingMetaData = JsonConvert.DeserializeObject<InteriorMetadata>(resourceData2);
                var entryPoint = buildingMetaData.AccessPoints.FirstOrDefault(e => e.Id.Equals(entranceId));
                return entryPoint.OnFoot.Position;
            }
            catch (Exception e)
            {
                log.Error(e);
            }
            return Vector3.One;
        }

        // Properties -> Linkage (Building/Garage) -> Building & Interior -> Entrances/Exit & Points
        // apt_dpheights_he_01 apt_dpheights_01 apt_dpheights_ped_frt_ent_01
        // apt_dpheights_he_01 gar_dpheights_01 gar_dpheights_veh_frt_ent_01
        public Vector3 EnterProperty(string propertyId, string propertyPart, string entranceId)
        {

            try
            {
                if (string.IsNullOrEmpty(propertyId))
                    return Vector3.One;
                //var pm = PropertyManager.GetInstance();
                //var property = pm.GetProperty(propertyId);
                //var type = property.Type;
                //var garageString = property.Garage;
                //var aptString = property.Apartment;
                 

                //var folderName = GetPropertyPartType(propertyPart);
                //log.Debug(folderName);
                //log.Debug(propertyPart);

                //// Load the link file
                //var resourceData1 = ResourceFile.ReadAll($"data/world/{folderName}/{propertyPart}.json");
                //var buildingInteriorLink = JsonConvert.DeserializeObject<BuildingInteriorLink>(resourceData1);
                //var targetEntryPointString = buildingInteriorLink.ExteriorEntrances[entranceId];

                //log.Debug(folderName);
                //log.Debug(buildingInteriorLink.Interior);
                //// Load interior file
                //var resourceData2 = ResourceFile.ReadAll($"data/world/interiors/{buildingInteriorLink.Interior}.json");
                //var interiorMetadata = JsonConvert.DeserializeObject<InteriorMetadata>(resourceData2);
                //var targetEntryPoint = interiorMetadata.EntrancePoints.FirstOrDefault(e => e.Id.Equals(targetEntryPointString));
                //return Vector3.One;

            }
            catch (Exception e)
            {
                log.Error(e);
            }
            return Vector3.One;
        }
        public Vector3 ExitBuilding(string buildingId, string accessPoint, int type = 0)
        {

            try
            {
                var resourceData2 = ResourceFile.ReadAll($"data/world/buildings/{buildingId}.json");
                var buildingMetaData = JsonConvert.DeserializeObject<BuildingMetadata>(resourceData2);
                var entrance = buildingMetaData.AccessPoints.FirstOrDefault(e => e.Id.Equals(accessPoint));
                log.Debug(accessPoint);
                log.Debug(entrance.Id);
                Vector3 pos = Vector3.One;
                switch (type)
                {
                    case 0: pos = entrance.ExitOnFoot.Position; break;
                    case 1: pos = entrance.ExitInVehicle.Position; break;
                }
                return pos;
            }
            catch (Exception e)
            {
                log.Error(e);
            }
            return Vector3.One;
        }

        public string ExitInterior(string interiorId, string exitId)
        {
            var resourceData2 = ResourceFile.ReadAll($"data/world/interiors/{interiorId}.json");
            var interiorMetadata = JsonConvert.DeserializeObject<InteriorMetadata>(resourceData2);
            var targetEntryPoint = interiorMetadata.AccessPoints.FirstOrDefault(e => e.Id.Equals(exitId));
            return targetEntryPoint.Tag;
        }
        //apt_high_pier_01_ext_ped_01
        public string ExitProperty(string propertyId, string propertyPart, string exitId)
        {

            try
            {
                if (string.IsNullOrEmpty(propertyId))
                    return "";
                //var pm = PropertyManager.GetInstance();
                //var property = pm.GetProperty(propertyId);
                //var type = property.Type;
                //var garageString = property.Garage;
                //var aptString = property.Apartment;
                 

                //var propertyType = propertyPart.Split('_')[0];
                //var abc = GetPropertyPartType(propertyPart);

                //log.Debug("tes");
                //log.Debug(exitId);

                //var resourceData1 = ResourceFile.ReadAll($"data/world/{abc}/{propertyPart}.json");
                //var buildingInteriorLink = JsonConvert.DeserializeObject<BuildingInteriorLink>(resourceData1);
                //var targetEntryPointString = buildingInteriorLink.InteriorExits[exitId];


                //var resourceData2 = ResourceFile.ReadAll($"data/world/buildings/{buildingInteriorLink.Building}.json");
                //var interiorMetadata = JsonConvert.DeserializeObject<BuildingMetadata>(resourceData2);
                //var targetEntryPoint = interiorMetadata.ExitPoints.FirstOrDefault(e => e.Id.Equals(targetEntryPointString));

                //log.Debug("tes");
                //log.Debug(propertyPart);

                //Game.PlayerPed.Position = targetEntryPoint.Position;
                //return targetEntryPoint.Id;
            }
            catch (Exception e)
            {
                log.Error(e);
            }
            return "";
        }

        public Vector3 GetBuildingEntrance(string buildingId, int entranceId = 0)
        {

            try
            {
                string resourceData3 = null;
                resourceData3 = ResourceFile.ReadAll($"data/world/buildings/{buildingId}.json");
                var interiorMetadata = JsonConvert.DeserializeObject<BuildingMetadata>(resourceData3);
                var targetEntryPoint = interiorMetadata.AccessPoints[entranceId];
                return targetEntryPoint.DoorPosition;
            }
            catch (Exception e)
            {
                log.Error(e);
            }
            return Vector3.One;
        }


        public string GetBuildingEntranceString(string buildingId, int entranceId = 0)
        {

            try
            {
                string resourceData3 = null;
                resourceData3 = ResourceFile.ReadAll($"data/world/buildings/{buildingId}.json");
                var interiorMetadata = JsonConvert.DeserializeObject<BuildingMetadata>(resourceData3);
                var targetEntryPoint = interiorMetadata.AccessPoints[entranceId];
                return targetEntryPoint.Id;
            }
            catch (Exception e)
            {
                log.Error(e);
            }
            return "";
        }

        public Vector3 GetBuildingInterior(string buildingId)
        {

            try
            {
                var resourceData2 = ResourceFile.ReadAll($"data/world/interiors/{buildingId}.json");
                var buildingMetaData = JsonConvert.DeserializeObject<InteriorMetadata>(resourceData2);
                return new Vector3(buildingMetaData.WorldPos.X, buildingMetaData.WorldPos.Y, 0f);
            }
            catch (Exception e)
            {
                log.Error(e);
            }
            return Vector3.One;
        }

        public Vector3 GetBuildingPosition(string buildingId)
        {

            try
            {
                var resourceData2 = ResourceFile.ReadAll($"data/world/buildings/{buildingId}.json");
                var buildingMetaData = JsonConvert.DeserializeObject<BuildingMetadata>(resourceData2);
                return new Vector3(buildingMetaData.WorldPos.X, buildingMetaData.WorldPos.Y, 0f);
            }
            catch (Exception e)
            {
                log.Error(e);
            }
            return Vector3.One;
        }
        public Vector3 GetBuildingWorldPos(string buildingId)
        {

            try
            {
                var resourceData2 = ResourceFile.ReadAll($"data/world/buildings/{buildingId}.json");
                var buildingMetaData = JsonConvert.DeserializeObject<BuildingMetadata>(resourceData2);
                return new Vector3(buildingMetaData.WorldPos.X, buildingMetaData.WorldPos.Y, 0);
            }
            catch (Exception e)
            {
                log.Error(e);
            }
            return Vector3.One;
        }

        public Vector3 GetBuildingWorldPos(object neariestBulding)
        {
            throw new NotImplementedException();
        }

        public Vector3 GetInteriorExit(string interiorId, int exitId = 0)
        {

            try
            {
                string resourceData3 = null;
                resourceData3 = ResourceFile.ReadAll($"data/world/interiors/{interiorId}.json");
                var interiorMetadata = JsonConvert.DeserializeObject<InteriorMetadata>(resourceData3);
                var targetEntryPoint = interiorMetadata.AccessPoints[exitId];
                return targetEntryPoint.DoorPosition;
            }
            catch (Exception e)
            {
                log.Error(e);
            }
            return Vector3.One;
        }

        public string GetInteriorExitString(string interiorId, int exitId = 0)
        {

            try
            {
                string resourceData3 = null;
                resourceData3 = ResourceFile.ReadAll($"data/world/interiors/{interiorId}.json");
                var interiorMetadata = JsonConvert.DeserializeObject<InteriorMetadata>(resourceData3);
                var targetEntryPoint = interiorMetadata.AccessPoints[exitId];
                return targetEntryPoint.Id;
            }
            catch (Exception e)
            {
                log.Error(e);
            }
            return "";
        }

        public string GetNearestBuilding()
        {

            try
            {
                var resourceData = ResourceFile.ReadAll($"data/world/buildings.json");
                var worldBuildings = JsonConvert.DeserializeObject<string[]>(resourceData);
                var distance = 99999f;
                var entranceString = "";
                var playPos = new Vector2(Game.PlayerPed.Position.X, Game.PlayerPed.Position.Y);
                foreach (var item in worldBuildings)
                {
                    var resourceData2 = ResourceFile.ReadAll($"data/world/buildings/{item}.json");
                    var buildingMetaData = JsonConvert.DeserializeObject<BuildingMetadata>(resourceData2);
                    var d = API.GetDistanceBetweenCoords(buildingMetaData.WorldPos.X,
                        buildingMetaData.WorldPos.Y, 0, playPos.X, playPos.Y, 0, false);
                    if (d < distance)
                    {
                        distance = d;
                        entranceString = buildingMetaData.Id;
                    }
                }
                return entranceString;
            }
            catch (Exception e)
            {
                log.Error(e);
            }
            return null;
        }


        public string GetNearestBuildingEntrance(string building)
        {

            try
            {
                if (string.IsNullOrEmpty(building))
                    building = GetNearestBuilding();
                var resourceData2 = ResourceFile.ReadAll($"data/world/buildings/{building}.json");
                var interiorMetadata = JsonConvert.DeserializeObject<BuildingMetadata>(resourceData2);
                var distance = 99999f;
                var entranceString = "";
                foreach (var item in interiorMetadata.AccessPoints)
                {
                    var newDistance = World.GetDistance(item.DoorPosition, Game.PlayerPed.Position);
                    if (World.GetDistance(item.DoorPosition, Game.PlayerPed.Position) < distance)
                    {
                        distance = newDistance;
                        entranceString = item.Id;
                    }
                }
                return entranceString;
            }
            catch (Exception e)
            {
                log.Error(e);
            }
            return null;
        }


        public string GetNearestInterior()
        {

            try
            {
                var resourceData = ResourceFile.ReadAll($"data/world/interiors.json");
                var worldBuildings = JsonConvert.DeserializeObject<string[]>(resourceData);
                var distance = 99999f;
                var entranceString = "";
                var playPos = new Vector2(Game.PlayerPed.Position.X, Game.PlayerPed.Position.Y);
                foreach (var item in worldBuildings)
                {
                    var resourceData2 = ResourceFile.ReadAll($"data/world/interiors/{item}.json");
                    var buildingMetaData = JsonConvert.DeserializeObject<InteriorMetadata>(resourceData2);
                    var d = API.GetDistanceBetweenCoords(buildingMetaData.WorldPos.X,
                        buildingMetaData.WorldPos.Y, 0, playPos.X, playPos.Y, 0, false);
                    if (d < distance)
                    {
                        distance = d;
                        entranceString = buildingMetaData.Id;
                    }
                }
                return entranceString;
            }
            catch (Exception e)
            {
                log.Error(e);
            }
            return null;
        }
        public string GetNearestInteriorExit(string interiorId = null)
        {

            try
            {
                if (string.IsNullOrEmpty(interiorId))
                    interiorId = GetNearestInterior();
                var resourceData2 = ResourceFile.ReadAll($"data/world/interiors/{interiorId}.json");
                var interiorMetadata = JsonConvert.DeserializeObject<InteriorMetadata>(resourceData2);
                var distance = 99999f;
                var entranceString = "";
                foreach (var item in interiorMetadata.AccessPoints)
                {
                    var newDistance = World.GetDistance(item.DoorPosition, Game.PlayerPed.Position);
                    if (World.GetDistance(item.DoorPosition, Game.PlayerPed.Position) < distance)
                    {
                        distance = newDistance;
                        entranceString = item.Id;
                    }
                }
                return entranceString;
            }
            catch (Exception e)
            {
                log.Error(e);
            }
            return null;
        }

        public int GetNumOfBuldingEntrances(string buildingId)
        {
            log.Info(buildingId);
            try
            {
                string resourceData3 = null;
                resourceData3 = ResourceFile.ReadAll($"data/world/buildings/{buildingId}.json");
                var interiorMetadata = JsonConvert.DeserializeObject<BuildingMetadata>(resourceData3);
                return interiorMetadata.AccessPoints.Count;
            }
            catch (Exception e)
            {
                log.Error(e);
            }
            return 0;
        }

        public int GetNumOfInteriorExits(string interiorId)
        {

            try
            {
                string resourceData = null;
                resourceData = ResourceFile.ReadAll($"data/world/interiors/{interiorId}.json");
                var interiorMetadata = JsonConvert.DeserializeObject<InteriorMetadata>(resourceData);
                return interiorMetadata.AccessPoints.Count;
            }
            catch (Exception e)
            {
                log.Error(e);
            }
            return 0;
        }

        public string GetPropertyApartment(string propertyId)
        {

            try
            {
                if (string.IsNullOrEmpty(propertyId))
                    return "";
                var pm = PropertyManager.GetInstance();
                var property = pm.GetProperty(propertyId);
                return property.Apartment;
            }
            catch (Exception e)
            {
                log.Error(e);
            }
            return "";

        }

        public string GetPropertyBuilding(string propertyId)
        {

            try
            {
                log.Info(propertyId);
                if (string.IsNullOrEmpty(propertyId))
                    return "";
                var pm = PropertyManager.GetInstance();
                var property = pm.GetProperty(propertyId);
                return property.Building;
            }
            catch (Exception e)
            {
                log.Error(e);
            }
            return "";
        }

        public string GetPropertyEntry(string propertyPart, string functionType, string entranceString)
        {

            try
            {
                var resourceData1 = ResourceFile.ReadAll($"data/world/{functionType}/{propertyPart}.json");
                var buildingInteriorLink = JsonConvert.DeserializeObject<BuildingInteriorLink>(resourceData1);
                var targetEntryPointString = buildingInteriorLink.ExteriorEntrances[entranceString];
                return targetEntryPointString;
            }
            catch (Exception e)
            {
                log.Error(e);
            }
            return "";

        }



        public string GetPropertyExit(string propertyPart, string functionType, string neariestExit)
        {

            try
            {
                var resourceData1 = ResourceFile.ReadAll($"data/world/{functionType}/{propertyPart}.json");
                var buildingInteriorLink = JsonConvert.DeserializeObject<BuildingInteriorLink>(resourceData1);
                var targetEntryPointString = buildingInteriorLink.InteriorExits[neariestExit];
                return targetEntryPointString;
            }
            catch (Exception e)
            {
                log.Error(e);
            }
            return "";
        }

        public string GetPropertyGarage(string propertyId)
        {

            try
            {
                if (string.IsNullOrEmpty(propertyId))
                    return "";
                var pm = PropertyManager.GetInstance();
                var property = pm.GetProperty(propertyId);
                return property.Garage;
            }
            catch (Exception e)
            {
                log.Error(e);
            }
            return "";

        }

        public string GetPropertyGarageLayout(string propertyId)
        {

            try
            {
                if (string.IsNullOrEmpty(propertyId))
                    return "";
                var pm = PropertyManager.GetInstance();
                var property = pm.GetProperty(propertyId);
                return property.Layout;
            }
            catch (Exception e)
            {
                log.Error(e);
            }
            return "";

        }

        public int GetPropertyGarageLimit(string propertyId)
        {
            if (string.IsNullOrEmpty(propertyId))
                return 0;
            var pm = PropertyManager.GetInstance();
            var property = pm.GetProperty(propertyId);
            return property.VehicleCap;
        }

        public string GetPropertyInterior(string propertyId, string propertyType)
        {

            try
            {
                var pm = PropertyManager.GetInstance();
                var property = pm.GetProperty(propertyId);
                var resourceData1 = ResourceFile.ReadAll($"data/world/{propertyType}/{propertyId}.json");
                var buildingInteriorLink = JsonConvert.DeserializeObject<BuildingInteriorLink>(resourceData1);
                return buildingInteriorLink.Interior;

            }
            catch (Exception e)
            {
                log.Error(e);
            }
            return "";
        }


        public string GetPropertyPartType(string partName)
        {

            try
            {
                var propertyType = partName.Split('_')[0];
                switch (propertyType)
                {
                    case "apt":
                        return "apartments";
                        break;
                    case "gar":
                        return "garages";
                        break;
                }
            }
            catch (Exception e)
            {
                log.Error(e);
            }
            return "";
        }

        public bool IsInMarker(int handle, int obj)
        {

            try
            {
                var instance = MarkerManager.GetInstance();
                var marker = instance.GetMarker(handle);
                var prop = Entity.FromHandle(obj);
                return World.GetDistance(prop.Position, marker.Position) < marker.ActivationRange;
            }
            catch (Exception e)
            {
                log.Error(e);
            }
            return false;
        }

        public void PlaceVehicleInGarageSlot(string propertyId, int index, Entity vehicle)
        {

            try
            {
                var garage = GetPropertyGarage(propertyId);
                var layout = GetPropertyGarageLayout(propertyId);
                var limit = GetPropertyGarageLimit(propertyId);
                if (index > limit)
                    return;
                var resourceData1 = ResourceFile.ReadAll($"data/world/garages/{garage}.json");
                var garageInterior = JsonConvert.DeserializeObject<BuildingInteriorLink>(resourceData1);

                var resourceData2 = ResourceFile.ReadAll($"data/world/interiors/{garageInterior.Interior}.json");
                var interior = JsonConvert.DeserializeObject<InteriorMetadata>(resourceData2);


                var resourceData3 = ResourceFile.ReadAll($"data/world/garages/layouts/{layout}.json");
                var garageLayout = JsonConvert.DeserializeObject<GarageLayout>(resourceData3);


                log.Debug(garageLayout.VehicleSlots.Count());
                log.Debug(index);
                var slot = garageLayout.VehicleSlots[index];
                if (slot == null)
                    throw new Exception($"Slot not found");
                vehicle.Position = slot.Position;
                vehicle.Heading = slot.Heading;
            }
            catch (Exception e)
            {
                log.Error(e);
            }
        }
    }
}