using CitizenFX.Core.Native;
using Common.Logging; 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Resource.Component
{
    public abstract class ResourceComponent : IResourceComponent
    {
        private List<Type> _scripts;
        protected static ILog log = Proline.Resource.Logging.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public List<Type> Scripts
        {
            get
            {

                if (_scripts == null)
                    _scripts = new List<Type>();
                return _scripts;
            }
        }

        public string Name { get; }
        public string[] Depedencies { get; }
         
        protected ResourceComponent(string name, params string[] depedencies) 
        {
            Name = name;
            Depedencies = depedencies;
        }

        protected void RegisterScript<T>() where T : ResourceComponentScript, new()
        {
            Scripts.Add(typeof(T));
        }

        public async Task OnExecuteScript(string scriptName)
        {
            if (string.IsNullOrEmpty(scriptName))
            {
                throw new ArgumentException($"'{nameof(scriptName)}' cannot be null or empty.", nameof(scriptName));
            }

            if (Scripts == null)
                return;

            var scriptType = Scripts.FirstOrDefault(e => e.Name.Equals(scriptName, StringComparison.CurrentCultureIgnoreCase));
            if ((object)scriptType == null)
            {
                log.Debug($"No script {scriptName} found in component");
                return;
            }

            try
            {
                var script = (ResourceComponentScript)Activator.CreateInstance(scriptType);
                await script.OnExecute();
            }
            catch (Exception e)
            {
                log.Info(e);
            }
        }

        public virtual void OnLoad() { }

        public virtual void OnStart() { }
    }
}
