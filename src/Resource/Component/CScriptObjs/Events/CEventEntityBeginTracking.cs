﻿using CitizenFX.Core.Native;
using Common.Logging;
using Proline.Resource.Framework;
using Resource.Component.CScriptObjs;


namespace Resource.Component.CScriptObjs.Events
{
    public class CEventEntityBeginTracking : ResourceEvent
    {
        private static readonly ILog log = Proline.Resource.Logging.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected override void OnEventTriggered(params object[] args)
        {
            //log.Debug(this.GetType().Name + " Invoked");
            if (args == null || args.Length == 0)
                return;
            //log.Debug(args[0]);
            var handle = int.Parse(args[0].ToString());

            var _sm = ScriptObjectManager.GetInstance();
            if (!API.DoesEntityExist(handle))
                return;
            var modelHash = API.GetEntityModel(handle);
            if (!_sm.ContainsSO(handle) && _sm.ContainsKey(modelHash))
            {
                log.Debug(handle + " Oh boy, we found a matching script object with that model hash from that handle, time to track it");
                _sm.AddSO(handle, new ScriptObject()
                {
                    Data = _sm.Get(modelHash),
                    Handle = handle,
                    State = 0,
                });
            }
        }
    }
}
