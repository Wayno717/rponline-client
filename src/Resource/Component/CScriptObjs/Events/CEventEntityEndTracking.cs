﻿using CitizenFX.Core.Native;
using Common.Logging;
using Proline.Resource.Framework;
using Resource.Component.CScriptObjs;


namespace Resource.Component.CScriptObjs.Events
{
    public class CEventEntityEndTracking : ResourceEvent
    {
        private static readonly ILog log = Proline.Resource.Logging.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        protected override void OnEventTriggered(params object[] args)
        {
            //log.Debug(this.GetType().Name + " Invoked");
            if (args == null || args.Length == 0)
                return;
            var handle = int.Parse(args[0].ToString());

            var _sm = ScriptObjectManager.GetInstance();

            if (API.DoesEntityExist(handle))
                return;
            var modelHash = API.GetEntityModel(handle);
            if (!_sm.ContainsKey(modelHash))
                return;
            if (_sm.ContainsKey(handle))
                _sm.Remove(handle);
        }
    }
}
