﻿using Proline.Resource.Framework;
using Resource.Component.CScriptObjs.Events;
using Resource.Component.CScriptObjs.Scripts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Resource.Component;

namespace Resource.Component.CScriptObjs
{
    internal class CScriptObjsComponent : ResourceComponent, IResourceComponent, ICScriptObjsComponent
    {
        private ResourceScript script;

        public CScriptObjsComponent(ResourceScript script) : base(nameof(CScriptObjsComponent), null)
        {
            this.script = script;
        }

        public override void OnLoad()
        {
            script.RegisterEvent<CEventEntityBeginTracking>();
            script.RegisterEvent<CEventEntityEndTracking>();

            RegisterScript<InitCore>();
            RegisterScript<InitSession>();
        }

        public override void OnStart()
        {

        }
    }
}
