﻿
using Newtonsoft.Json;
using Proline.Resource.Framework;
using Proline.Resource.IO;
using Resource.Component.CScriptObjs.Data;
using System.Collections.Generic;
using System.Threading.Tasks;
using Resource.Component;

namespace Resource.Component.CScriptObjs.Scripts
{
    public class InitCore : ResourceComponentScript
    {

        public override async Task OnExecute()
        {
            var data2 = ResourceFile.ReadAll("data/brain/script_objs.json");

            log.Debug(data2);
            var objs = JsonConvert.DeserializeObject<ScriptObjectData[]>(data2);
            var sm = ScriptObjectManager.GetInstance();

            foreach (var item in objs)
            {
                var hash = string.IsNullOrEmpty(item.ModelHash) ? item.ModelName : CitizenFX.Core.Native.API.GetHashKey(item.ModelHash);
                if (!sm.ContainsKey(hash))
                    sm.Add(hash, new List<ScriptObjectData>());
                sm.Get(hash).Add(item);
            }
        }
    }
}
