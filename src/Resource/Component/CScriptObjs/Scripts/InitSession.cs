﻿using CitizenFX.Core;

using Proline.Resource.Framework;
using System.Threading.Tasks;
using Resource.Component;

namespace Resource.Component.CScriptObjs.Scripts
{
    public class InitSession : ResourceComponentScript
    {
        public override async Task OnExecute()
        {
            var task = Task.Factory.StartNew(async () =>
            {
                var gc = new ProcessScriptObjs();
                while (true)
                {
                    await gc.Execute();
                    await BaseScript.Delay(1000);
                }
            });
        }
    }
}
