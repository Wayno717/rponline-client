﻿using CitizenFX.Core;
using Resource.Component.CGameLogic.Data;

namespace Resource.Component.CPoolObjects
{
    internal interface ICPoolObjectsComponentController 
    {
        int[] GetAllExistingPoolObjects();
        int[] GetEntityHandlesByTypes(int type);
        Entity GetNeariestEntity(int type);
    }
}