﻿using Proline.Resource.Framework;
using Resource.Component.CPoolObjects.Scripts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Resource.Component;
using CitizenFX.Core;
using Resource.Component.CGameLogic.Data;
using CitizenFX.Core.Native;

namespace Resource.Component.CPoolObjects
{
    internal class CPoolObjectsComponent : ResourceComponent, ICPoolObjectsComponent
    {
        public CPoolObjectsComponent() : base(nameof(CPoolObjectsComponent), null)
        {
        }
    }
}
