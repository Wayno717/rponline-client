﻿using CitizenFX.Core;
using CitizenFX.Core.Native;
using Common.Logging;
using Proline.Resource.Framework;
using Resource.Component.CGameLogic.Data;
using Resource.Component.CPoolObjects.Scripts;
using System;
using System.Collections.Generic;

namespace Resource.Component.CPoolObjects
{
    internal class CPoolObjectsComponentController
    {
        private readonly ResourceScript script;

        protected static ILog log = Proline.Resource.Logging.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public CPoolObjectsComponentController(ResourceScript script)
        {
            this.script = script;
        }

        public int[] GetAllExistingPoolObjects()
        {
            return PoolObjectManager.TrackedHandles;
        }


        public int[] GetEntityHandlesByTypes(int type)
        {
            try
            {
                var array = new List<int>();
                int entHandle = -1;
                int handle;
                switch ((EntityType)type)
                {
                    case EntityType.PROP:
                        handle = API.FindFirstObject(ref entHandle);
                        array.Add(entHandle);
                        entHandle = -1;
                        while (API.FindNextObject(handle, ref entHandle))
                        {
                            array.Add(entHandle);
                            entHandle = -1;
                        }
                        API.EndFindObject(handle);
                        break;
                    case EntityType.PED:
                        handle = API.FindFirstPed(ref entHandle);
                        array.Add(entHandle);
                        entHandle = -1;
                        while (API.FindNextPed(handle, ref entHandle))
                        {
                            array.Add(entHandle);
                            entHandle = -1;
                        }

                        API.EndFindPed(handle);
                        break;
                    case EntityType.VEHICLE:
                        handle = API.FindFirstVehicle(ref entHandle);
                        array.Add(entHandle);
                        entHandle = -1;
                        while (API.FindNextVehicle(handle, ref entHandle))
                        {
                            array.Add(entHandle);
                            entHandle = -1;
                        }

                        API.EndFindVehicle(handle);
                        break;
                }
                return array.ToArray();
            }
            catch (Exception)
            {

                throw;
            }

        }

        public Entity GetNeariestEntity(int type)
        {

            try
            {
                Entity _entity = null;
                var _closestDistance = 99999f;
                var handles = GetEntityHandlesByTypes(type);
                foreach (var item in handles)
                {
                    var entity = Entity.FromHandle(item);
                    var distance = World.GetDistance(entity.Position, Game.PlayerPed.Position);
                    if (distance < _closestDistance)
                    {
                        log.Debug("Found a vehicle");
                        _entity = entity;
                        _closestDistance = distance;
                    }
                }
                return _entity;
            }
            catch (Exception e)
            {
                log.Error(e);
            }
            return null;
        }
         
    }
}