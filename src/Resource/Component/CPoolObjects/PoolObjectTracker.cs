﻿using CitizenFX.Core.Native;
using Common.Logging;
using Proline.Resource.Framework;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Resource.Component.CPoolObjects
{
    public class PoolObjectTracker
    {
        private static readonly ILog log = Proline.Resource.Logging.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public PoolObjectTracker()
        {
            _trackedHandles = new HashSet<int>();
        }
        private HashSet<int> _trackedHandles;

        public async Task Execute()
        {
            var oldHandles = _trackedHandles;
            _trackedHandles = new HashSet<int>();
            var currentHandles = ProcessWorldPoolItems();

            var addedHandles = currentHandles.Except(oldHandles);
            var removedHandles = oldHandles.Except(currentHandles);

            ProcessAddedHandles(addedHandles);
            ProcessRemovedHandles(removedHandles);
            //var x = addedHandles.Count();
            //var y = removedHandles.Count();
            //log.Debug($"Added Handles {x} Removed Handles {y}");

            _trackedHandles = currentHandles;
            PoolObjectManager.TrackedHandles = _trackedHandles.ToArray();
        }

        private void ProcessAddedHandles(IEnumerable<int> handles)
        {
            foreach (var item in handles)
            {
                ResourceEvent.TriggerEvent("CEventEntityBeginTracking", item);
                //log.Debug($"Added Handle {item} Found, Exists: {API.DoesEntityExist(item)}");
            }
        }

        private void ProcessRemovedHandles(IEnumerable<int> handles)
        {
            foreach (var item in handles)
            {
                ResourceEvent.TriggerEvent("CEventEntityEndTracking", item);
                //log.Debug($"Removed Handle {item} Found, Exists: {API.DoesEntityExist(item)}");
            }
        }

        private HashSet<int> ProcessWorldPoolItems()
        {
            var pickupHanldeFinder = new PickupHandleFinder();
            var objectHanldeFinder = new ObjHandleFinder();
            var vehicleHanldeFinder = new VehicleHandleFinder();
            var pedHanldeFinder = new PedHandleFinder();
            var handles = new HashSet<int>();

            while (pickupHanldeFinder.FindNextProp(out int handle))
            {
                handles.Add(handle);
            }

            while (objectHanldeFinder.FindNextProp(out int handle))
            {
                handles.Add(handle);
            }

            while (vehicleHanldeFinder.FindNextProp(out int handle))
            {
                handles.Add(handle);
            }

            while (pedHanldeFinder.FindNextProp(out int handle))
            {
                handles.Add(handle);
            }
            return handles;
        }
    }
}