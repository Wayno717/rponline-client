﻿using CitizenFX.Core;

namespace Resource.Component.CMissionManager
{
    internal interface ICMissionManagerComponentController 
    {
        bool BeginMission();
        void EndMission();
        bool GetMissionFlag();
        bool IsInMissionVehicle();
        void SetMissionFlag(bool enable);
        void TrackPoolObjectForMission(PoolObject obj);
    }
}