﻿using CitizenFX.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Resource.Component.CMissionManager
{
    internal class CMissionManagerComponent :  ResourceComponent, ICMissionManagerComponent
    {
        public CMissionManagerComponent() : base(nameof(CMissionManagerComponent), null)
        {
        }
    }
}
