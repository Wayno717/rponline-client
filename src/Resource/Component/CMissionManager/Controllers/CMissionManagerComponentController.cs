﻿using CitizenFX.Core;
using Common.Logging;
using System;

namespace Resource.Component.CMissionManager
{
    internal class CMissionManagerComponentController : ICMissionManagerComponentController
    {
        protected static ILog log = Proline.Resource.Logging.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public bool BeginMission()
        {
            try
            {
                if (GetMissionFlag()) return false;
                var instance = PoolObjectTracker.GetInstance();
                if (instance.IsTrackingObjects())
                {
                    instance.DeleteAllPoolObjects();
                    instance.ClearPoolObjects();
                }
                SetMissionFlag(true);
                return true;
            }
            catch (Exception e)
            {
                log.Error(e.ToString());
            }
            return false;
        }

        public void EndMission()
        {
            try
            {
                if (!GetMissionFlag()) return;
                var instance = PoolObjectTracker.GetInstance();
                if (instance.IsTrackingObjects())
                {
                    instance.DeleteAllPoolObjects();
                    instance.ClearPoolObjects();
                }
                SetMissionFlag(false);
            }
            catch (Exception e)
            {
                log.Error(e.ToString());
            }
        }
        public bool GetMissionFlag()
        {
            try
            {
                return MissionFlags.IsOnMission;
            }
            catch (Exception e)
            {
                log.Error(e.ToString());
            }
            return false;
        }
        public bool IsInMissionVehicle()
        {
            try
            {
                var instance = PoolObjectTracker.GetInstance();
                return Game.PlayerPed.IsInVehicle() && instance.ContainsPoolObject(Game.PlayerPed.CurrentVehicle);
            }
            catch (Exception e)
            {
                log.Error(e.ToString());
            }
            return false;
        }
        public void SetMissionFlag(bool enable)
        {
            try
            {
                MissionFlags.IsOnMission = enable;
            }
            catch (Exception e)
            {
                log.Error(e.ToString());
            }
        }
        public void TrackPoolObjectForMission(PoolObject obj)
        {
            try
            {
                if (!GetMissionFlag()) return;
                var instance = PoolObjectTracker.GetInstance();
                instance.TrackPoolObject(obj);
            }
            catch (Exception e)
            {
                log.Error(e.ToString());
            }
            return;
        }
    }
}