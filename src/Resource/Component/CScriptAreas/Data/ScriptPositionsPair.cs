﻿namespace Resource.Component.CScriptAreas.Data
{
    public class ScriptPositionsPair
    {
        public string ScriptName { get; set; }
        public float X { get; set; }
        public float Y { get; set; }
        public float Z { get; set; }
    }
}
