﻿using Newtonsoft.Json;
using Proline.Resource.Framework;
using Proline.Resource.IO;
using Resource.Component.CScriptAreas.Data;
using System.Threading.Tasks;
using Resource.Component;

namespace Resource.Component.CScriptAreas.Scripts
{
    public class InitCore : ResourceComponentScript
    {

        public override async Task OnExecute()
        {
            var instance = ScriptPositionManager.GetInstance();
            var data = ResourceFile.ReadAll("data/brain/script_pos.json");

            log.Debug(data);
            var scriptPosition = JsonConvert.DeserializeObject<ScriptPositions>(data);
            instance.AddScriptPositionPairs(scriptPosition.scriptPositionPairs);
            PosBlacklist.Create();
        }
    }
}
