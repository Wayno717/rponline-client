﻿using Proline.Resource.Framework;
using Resource.Component.CScriptAreas.Scripts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Resource.Component;

namespace Resource.Component.CScriptAreas
{
    internal class CScriptAreasComponent : ResourceComponent, IResourceComponent
    {
        private ResourceScript script;

        public CScriptAreasComponent(ResourceScript script) : base(nameof(CScriptAreasComponent), null)
        {
            this.script = script;
        }

        public override void OnLoad()
        {
            RegisterScript<InitCore>();
            RegisterScript<InitSession>();
        }

        public override void OnStart()
        {

        }
    }
}
