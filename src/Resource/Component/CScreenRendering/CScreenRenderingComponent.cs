﻿using CitizenFX.Core;
using CitizenFX.Core.Native;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Resource.Component.CScreenRendering
{
    internal class CScreenRenderingComponent : ResourceComponent, ICScreenRenderingComponent
    {
        public CScreenRenderingComponent() : base(nameof(CScreenRenderingComponent), null)
        {
        }

        public override void OnLoad()
        {

        }

        public override void OnStart()
        {

        }

    }
}
