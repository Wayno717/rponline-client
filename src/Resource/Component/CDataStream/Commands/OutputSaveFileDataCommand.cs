﻿
using CitizenFX.Core;
using Common.Logging;
using Proline.Resource.Framework;
using Resource.Component.CDataStream;

namespace Resource.Component.CDataStream.Commands
{
    public class OutputSaveFileDataCommand : ResourceCommand
    {
        private static readonly ILog log = Proline.Resource.Logging.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public OutputSaveFileDataCommand()
        {
        }

        protected override void OnCommandExecute(params object[] args)
        {
            if (args.Length > 0)
            {
                var identifier = args[0].ToString();
                var sm = DataFileManager.GetInstance();
                var save = sm.GetSave(Game.Player);
                var saveFile = save.GetSaveFile(identifier);
                log.Debug(saveFile.GetRawData());
            }
        }
    }
}
