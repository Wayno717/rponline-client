﻿
using Common.Logging;
using Newtonsoft.Json;

using Proline.Resource.Framework;
using Resource.Component.CDataStream;

namespace Resource.Component.CDataStream.Commands
{
    public class OutputActiveSaveFilePropertiesCommand : ResourceCommand
    {
        private static readonly ILog log = Proline.Resource.Logging.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public OutputActiveSaveFilePropertiesCommand()
        {
        }

        protected override void OnCommandExecute(params object[] args)
        {
            var sm = DataFileManager.GetInstance();
            var saveFile = sm.ActiveFile;
            foreach (var item in saveFile.Properties.Keys)
            {
                log.Debug(JsonConvert.SerializeObject(saveFile.Properties));
            }
        }
    }
}
