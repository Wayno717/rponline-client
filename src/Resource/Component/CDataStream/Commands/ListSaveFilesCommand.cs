﻿
using CitizenFX.Core;
using Common.Logging;
using Proline.Resource.Framework;
using Resource.Component.CDataStream;

namespace Resource.Component.CDataStream.Commands
{
    public class ListSaveFilesCommand : ResourceCommand
    {
        private static readonly ILog log = Proline.Resource.Logging.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public ListSaveFilesCommand()
        {
        }

        protected override void OnCommandExecute(params object[] args)
        {
            var sm = DataFileManager.GetInstance();
            var save = sm.GetSave(Game.Player);
            foreach (var saveFile in save.GetSaveFiles())
            {
                log.Debug(string.Format("{0},{1},{2}", saveFile.Name, saveFile.LastChanged, saveFile.HasUpdated));
            }
        }
    }
}
