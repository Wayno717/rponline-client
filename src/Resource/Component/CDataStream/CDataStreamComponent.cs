﻿using Proline.Resource.Framework;
using Resource.Component.CDataStream.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Resource.Component;
using Proline.Resource.IO;
using Newtonsoft.Json;
using Resource.Component.CDataStream.Data;
using CitizenFX.Core;

namespace Resource.Component.CDataStream
{
    internal class CDataStreamComponent : ResourceComponent, ICDataStreamComponent
    {
        private ResourceScript script;

        public CDataStreamComponent(ResourceScript script) : base(nameof(CDataStreamComponent), null)
        {
            this.script = script;
        }

        public override void OnLoad()
        {
            script.RegisterCommand<ListSaveFilesCommand>();
            script.RegisterCommand<OutputActiveSaveFilePropertiesCommand>();
            script.RegisterCommand<OutputSaveFileDataCommand>();
            script.RegisterCommand<SelectSaveFileCommand>();
        }

        public override void OnStart()
        {

        }


    }
}
