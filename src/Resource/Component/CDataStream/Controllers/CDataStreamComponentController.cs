﻿using CitizenFX.Core;
using Common.Logging;
using Newtonsoft.Json;
using Proline.Resource.IO;
using Resource.Component.CDataStream.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Resource.Component.CDataStream.Controllers
{
    internal class CDataStreamComponentController : ICDataStreamComponentController
    {
        protected static ILog log = Proline.Resource.Logging.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public void CreateDataFile()
        {

            try
            {
                var fm = DataFileManager.GetInstance();
                var saveFile = new SaveFile();
                saveFile.Name = "Tempname";
                saveFile.Properties = new Dictionary<string, object>();
                saveFile.LastChanged = DateTime.UtcNow;
                saveFile.HasUpdated = true;
                fm.TempFile = saveFile;
                fm.ActiveFile = saveFile;
            }
            catch (Exception e)
            {
                log.Error(e.ToString());
            }
        }

        public string GetSelectedDataFile()
        {

            try
            {
                var fm = DataFileManager.GetInstance();
                var saveFile = fm.ActiveFile;
                return saveFile.Name;
            }
            catch (Exception e)
            {
                log.Error(e.ToString());
            }
            return "";
        }

        public bool DoesDataFileExist(string id)
        {

            try
            {
                var fm = DataFileManager.GetInstance();
                var save = fm.GetSave(Game.Player);
                return save.GetSaveFile(id) != null;
            }
            catch (Exception e)
            {
                log.Error(e.ToString());
            }
            return false;
        }

        public void SaveDataFile(string identifier)
        {

            try
            {

                if (string.IsNullOrEmpty(identifier))
                {
                    throw new ArgumentException("Identitier argument cannot be null or empty");
                }

                var fm = DataFileManager.GetInstance();
                var id = identifier;//string.IsNullOrEmpty(identifier) ? "SaveFile" + index : identifier;
                var tempFile = fm.TempFile;
                tempFile.Name = id;
                if (fm.TempFile != null)
                {
                    fm.GetSave(Game.Player).InsertSaveFile(fm.TempFile);
                    fm.ClearTempFile();
                }
            }
            catch (Exception e)
            {
                log.Error(e.ToString());
            }
        }
        public void SelectDataFile(string identifier)
        {

            try
            {
                var fm = DataFileManager.GetInstance();
                var save = fm.GetSave(Game.Player);
                var dataFile = save.GetSaveFile(identifier);
                if (dataFile == null)
                    throw new Exception("Could not target a save file, save file does not exist " + identifier);
                fm.ActiveFile = dataFile;
            }
            catch (Exception e)
            {
                log.Error(e.ToString());
            }
        }
        public bool DoesDataFileValueExist(string key)
        {

            try
            {
                var fm = DataFileManager.GetInstance();
                var saveFile = fm.ActiveFile;
                var dictionary = saveFile.Properties;
                if (saveFile != null && dictionary != null)
                {
                    return dictionary.ContainsKey(key);
                }
            }
            catch (Exception e)
            {
                log.Error(e.ToString());
            }
            return false;
        }
        public void AddDataFileValue(string key, object value)
        {

            try
            {
                var fm = DataFileManager.GetInstance();
                var saveFile = fm.ActiveFile;
                if (saveFile == null) return;
                var dictionary = saveFile.Properties;
                if (dictionary == null)
                    return;
                if (!dictionary.ContainsKey(key))
                    dictionary.Add(key, value);
                saveFile.HasUpdated = true;
            }
            catch (Exception e)
            {
                log.Error(e.ToString());
            }
        }
        public void SetDataFileValue(string key, object value)
        {

            try
            {
                var fm = DataFileManager.GetInstance();
                var saveFile = fm.ActiveFile;
                if (saveFile == null)
                    throw new Exception("No save file has been targed");
                var dictionary = saveFile.Properties;
                if (dictionary == null)
                    throw new Exception("Save file does not have any property dictionary");
                if (dictionary.ContainsKey(key))
                    dictionary[key] = value;
                saveFile.HasUpdated = true;
            }
            catch (Exception e)
            {
                log.Error(e.ToString());
            }
        }
        public object GetDataFileValue(string key)
        {

            try
            {
                return GetDataFileValue<object>(key);
            }
            catch (Exception e)
            {
                log.Error(e.ToString());
            }
            return null;
        }
        public T GetDataFileValue<T>(string key)
        {

            try
            {
                var fm = DataFileManager.GetInstance();
                var saveFile = fm.ActiveFile;
                if (saveFile == null)
                    throw new Exception("No save file has been targed");
                var dictionary = saveFile.Properties;
                if (dictionary.ContainsKey(key))
                    return JsonConvert.DeserializeObject<T>(dictionary[key].ToString());
                else
                    throw new Exception("No key exists for " + key);

            }
            catch (Exception e)
            {
                log.Error(e.ToString());
            }
            return default;
        }


        public int GetNumOfAudioSamples()
        {

            try
            {
                var resourceData2 = ResourceFile.ReadAll($"data/audio/fronend_usages.json");
                var buildingMetaData = JsonConvert.DeserializeObject<List<SoundExample>>(resourceData2);
                return buildingMetaData.Count;
            }
            catch (Exception e)
            {
                log.Error(e.ToString());
            }
            return 0;
        }

        public async Task SendSaveToCloud()
        {
            //var api = new CDebugActionsAPI();
            //try
            //{
            //    var fm = DataFileManager.GetInstance();
            //    var save = fm.GetSave(Game.Player);
            //    if (save == null || fm.IsSaveInProgress)
            //        throw new Exception("Cannot send save to cloud, save in progress or save is null");
            //    fm.IsSaveInProgress = true;
            //    var identifier = Game.Player.Name;
            //    var path = $"Saves/{identifier}/";
            //    var files = save.GetSaveFiles().Where(e => e.HasUpdated);
            //    if (files.Count() > 0)
            //    {
            //        LogDebug($"Saving...");
            //        foreach (var file in files)
            //        {
            //            try
            //            {
            //                if (file == null)
            //                {
            //                    throw new Exception($"Cannot save file, current save file is null");
            //                };
            //                var json = JsonConvert.SerializeObject(file.Properties);
            //                LogDebug($"Saving file... {json}");

            //                var client = new EventClient("rponline-server");
            //                var args = await client.MakeRequest("WriteFile", new WriteFileRequest()
            //                {
            //                    Path = Path.Combine(path, file.Name + ".json"),
            //                    Value = json
            //                }); 
            //                file.HasUpdated = false;
            //            }
            //            catch (Exception e)
            //            {
            //                LogDebug(e.ToString());
            //            }
            //        }
            //    }
            //    else
            //    {
            //        LogDebug("Cannot send save to cloud, No save files to save");
            //    }

            //    if (save.HasChanged)
            //    {
            //        LogDebug($"Saving manifest...");
            //        var saveFiles = save.GetSaveFiles().Select(e => e.Name);

            //        var client = new EventClient("rponline-server");
            //        var args = await client.MakeRequest("WriteFile", new WriteFileRequest()
            //        {
            //            Path = Path.Combine(path, "Manifest.json"),
            //            Value = JsonConvert.SerializeObject(saveFiles)
            //        });

            //        save.HasChanged = false;
            //    }
            //    fm.IsSaveInProgress = false;
            //}
            //catch (Exception e)
            //{
            //    LogDebug(e.ToString());
            //}
        }
        public bool IsSaveInProgress()
        {

            try
            {
                var fm = DataFileManager.GetInstance();
                return fm.IsSaveInProgress;
            }
            catch (Exception e)
            {
                log.Error(e.ToString());
            }
            return false;
        }

        public int? GetSaveState()
        {

            try
            {
                var fm = DataFileManager.GetInstance();
                if (fm.LastSaveResult != null)
                {
                    var state = fm.LastSaveResult;
                    fm.LastSaveResult = null;
                    return state.Value;
                }
            }
            catch (Exception e)
            {
                log.Error(e.ToString());
            }
            return null;
        }

        public async Task PullSaveFromCloud()
        {
            //var api = new CDebugActionsAPI();
            //try
            //{
            //    LogDebug("Load Request put in");
            //    var fm = DataFileManager.GetInstance();
            //    api.LogInfo("Waiting for callback to be completed...");
            //    var identifier = Game.Player.Name;

            //    var client = new EventClient("rponline-server");
            //    var args = (string) await client.MakeRequest("ReadFile", new ReadFileRequest()
            //    {
            //        Value = $"Saves/{identifier}/Manifest.json"
            //    }); 

            //    if (args != null)
            //    {
            //        //var data = args.ToString();
            //        //if (data == null)
            //        //    throw new Exception("No manifest data for player save has been found");
            //        Debug.WriteLine(args);
            //        var manifest = JsonConvert.DeserializeObject<string[]>(args);
            //        var instance = DataFileManager.GetInstance();
            //        var save = instance.GetSave(Game.Player);
            //        foreach (var item in manifest)
            //        {

            //            var args2 = (string) await client.MakeRequest("ReadFile", new ReadFileRequest()
            //            {
            //                Value = $"Saves/{identifier}/{item}.json"
            //            });


            //            var result1 = args2.ToString();
            //            log.Debug(item);
            //            var properties = JsonConvert.DeserializeObject<Dictionary<string, object>>(result1);
            //            var saveFile = new SaveFile()
            //            {
            //                Name = item,
            //                Properties = properties,
            //            };
            //            save.InsertSaveFile(saveFile);
            //        }

            //        instance.HasLoadedFromNet = true;
            //        fm.HasLoadedFromNet = true;


            //    }
            //}
            //catch (Exception e)
            //{
            //    LogDebug(e.ToString());
            //}
        }

        public bool HasSaveLoaded()
        {

            try
            {
                var fm = DataFileManager.GetInstance();
                return fm.DoesPlayerHaveSave(Game.Player);
            }
            catch (Exception e)
            {
                log.Error(e.ToString());
            }
            return false;
        }

        public void GetAudioSamplesAtIndex(int id, out string audioId, out string audioRef, out bool enabled)
        {

            audioId = "";
            audioRef = "";
            enabled = false;
            try
            {
                var resourceData2 = ResourceFile.ReadAll($"data/audio/fronend_usages.json");
                var buildingMetaData = JsonConvert.DeserializeObject<List<SoundExample>>(resourceData2);
                audioId = buildingMetaData[id].AudioId;
                audioRef = buildingMetaData[id].AudioRef;
                enabled = buildingMetaData[id].Enabled;
            }
            catch (Exception e)
            {
                log.Error(e.ToString());
            }
        }
    }
}
