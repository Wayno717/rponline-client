﻿namespace Resource.Component.CDataStream.Data.Response
{
    public class ReadFileResponse
    {
        public string[] Files { get; set; }
    }
}
