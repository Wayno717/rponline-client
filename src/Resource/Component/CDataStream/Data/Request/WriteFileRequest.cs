﻿namespace Resource.Component.CDataStream.Data.Request
{
    public class WriteFileRequest
    {
        public string Value { get; set; }
        public string Path { get; set; }
    }
}
