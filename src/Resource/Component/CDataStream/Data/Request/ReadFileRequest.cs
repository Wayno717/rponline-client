﻿namespace Resource.Component.CDataStream.Data.Request
{
    public class ReadFileRequest
    {
        public string Value { get; set; }
    }
}
