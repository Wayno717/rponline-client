﻿using System.Threading.Tasks;

namespace Resource.Component.CCoreSystem
{
    public interface ICCoreSystemComponentController 
    {
        Task Delay(int ms);
        object[] GetEventData(object scriptInstance, string eventName);
        bool GetEventExists(object scriptInstance, string eventName);
        int GetInstanceCountOfScript(string scriptName);
        void MarkScriptAsNoLongerNeeded(object callingClass);
        void MarkScriptAsNoLongerNeeded(string scriptName);
        int StartNewScript(string scriptName, params object[] args);
        bool StartNewScriptV2(string sourceName, params object[] args);
        void TerminateScript(string scriptName);
        void TerminateScriptInstance(object scriptInstance);
        void TerminateScriptTask(int taskId);
        void TriggerScriptEvent(string eventName, params object[] args);
    }
}