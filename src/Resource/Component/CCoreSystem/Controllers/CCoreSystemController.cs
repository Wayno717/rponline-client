﻿using CitizenFX.Core;
using Common.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Resource.Component.CCoreSystem.Controllers
{
    public class CCoreSystemController : ICCoreSystemComponentController
    {
        private static readonly ILog log = Proline.Resource.Logging.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public void TriggerScriptEvent(string eventName, params object[] args)
        {
            try
            {
                var livescripts = ScriptManager.GetInstance().LiveScripts;
                foreach (var item in livescripts)
                {
                    item.EnqueueEvent(eventName, args);
                }
            }
            catch (Exception e)
            {
                log.Error(e.ToString());
            }
        }

        public bool GetEventExists(object scriptInstance, string eventName)
        {
            try
            {
                var script = ScriptManager.GetInstance().GetLiveScript(scriptInstance);


                if (script == null)
                    return false;

                return script.HasEvent(eventName);
            }
            catch (Exception e)
            {
                log.Error(e.ToString());
            }
            return false;
        }

        public object[] GetEventData(object scriptInstance, string eventName)
        {
            try
            {
                var script = ScriptManager.GetInstance().GetLiveScript(scriptInstance);

                if (script == null)
                    return new object[0];

                return script.DequeueEvent(eventName);
            }
            catch (Exception e)
            {
                log.Error(e.ToString());
            }
            return new object[0];
        }

        public int StartNewScript(string scriptName, params object[] args)
        {
            try
            {
                // Before we start a new script, attempt to clean up old scripts that are finished  
                // This is the only way we have to cleanup old scripts that have started and finished previously
                ScriptManager.GetInstance().TerminateMarkedAsNoLongerNeedeScripts();

                //

                // Get scripting config
                //var scriptingConfig = ScriptingConfigSection.ModuleConfig;

                // If we have a config, then we can load the levelscripts
                var scriptTypes = ScriptManager.GetInstance().GetAllScriptTypes();

                // Get type matching script name
                if (!scriptTypes.ContainsKey(scriptName))
                {
                    log.Debug(string.Format("Script {0} cannot be found", scriptName));
                    return -1;
                }

                // Get the script type from the dictionary that was created
                var type = scriptTypes[scriptName];

                log.Debug(string.Format("Creating script instance of {0}", scriptName));

                // Create an instance of that type found. There should be no constructor on these scripts
                var instance = Activator.CreateInstance(type);

                if (instance == null)
                {
                    log.Debug(string.Format("Unable to create script instance of {0}, instance came back null", scriptName));
                    return -1;
                }

                // Create a shell for the script
                var id = LiveScript.StartNew(instance, args);

                return id;
            }
            catch (Exception e)
            {
                log.Error(e.ToString());
            }
            return -1;
        }


        public int GetInstanceCountOfScript(string scriptName)
        {
            try
            {
                var livescripts = ScriptManager.GetInstance().LiveScripts;
                var count = livescripts.Where(e => e.Name.Equals(scriptName)).Count();

                //  log.Debug(String.Format("Getting the instance count of script {0} count: {1}", scriptName, count));
                return count;
            }
            catch (Exception e)
            {
                log.Error(e.ToString());
            }
            return 0;
        }

        public void MarkScriptAsNoLongerNeeded(object callingClass)
        {
            try
            {
                var livescripts = ScriptManager.GetInstance().LiveScripts;
                var script = livescripts.FirstOrDefault(e => e.Instance == callingClass);
                log.Debug(string.Format("Requesting that script instances by the name of {0} be marked as no longer needed", script.Name));
                script.IsMarkedForNolongerNeeded = true;
            }
            catch (Exception e)
            {
                log.Error(e.ToString());
            }
        }

        public void MarkScriptAsNoLongerNeeded(string scriptName)
        {
            try
            {
                var livescripts = ScriptManager.GetInstance().LiveScripts;
                var cls = scriptName;
                log.Debug(string.Format("Requesting that all script instances by the name of {0} be marked as no longer needed", scriptName));
                var scripts = livescripts.Where(e => e.Name.Equals(scriptName));
                foreach (var item in scripts)
                {
                    item.IsMarkedForNolongerNeeded = true;
                }
            }
            catch (Exception e)
            {
                log.Error(e.ToString());
            }
        }

        /// <summary>
        /// Terminates all script instances with the passed scriptName
        /// </summary>
        /// <param name="scriptName"></param>
        public void TerminateScript(string scriptName)
        {
            try
            {
                var livescripts = ScriptManager.GetInstance().LiveScripts;
                var scripts = livescripts.Where(e => e.Name.Equals(scriptName));
                log.Debug(string.Format("Requesting that all script instances by the name of {0} be terminated", scriptName));
                foreach (var script in scripts)
                {
                    script.Terminate();
                }
            }
            catch (Exception e)
            {
                log.Error(e.ToString());
            }
        }

        /// <summary>
        /// Terminates the passed script object task instance
        /// </summary>
        /// <param name="scriptInstance"></param>
        public void TerminateScriptInstance(object scriptInstance)
        {
            try
            {
                var livescripts = ScriptManager.GetInstance().LiveScripts;
                var script = livescripts.FirstOrDefault(e => e.Instance == scriptInstance);
                if (script == null)
                    return;
                log.Debug(string.Format("Requesting that a specific script instances by the name of {0} be terminated", script.Name));
                script.Terminate();
            }
            catch (Exception e)
            {
                log.Error(e.ToString());
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="taskId"></param>
        public void TerminateScriptTask(int taskId)
        {
            try
            {
                var livescripts = ScriptManager.GetInstance().LiveScripts;
                var script = livescripts.FirstOrDefault(e => e.ExecutionTask.Id == taskId);
                if (script == null)
                    return;
                log.Debug(string.Format("Requesting that a specific script instances by the name of {0} be terminated", script.Name));
                script.Terminate();
            }
            catch (Exception e)
            {
                log.Error(e.ToString());
            }
        }

        /// <summary>
        /// Waits a specific amount using the BaseScript.Delay function, 0 ms waits till the next frame is rendered
        /// </summary>
        /// <param name="ms"></param>
        /// <returns></returns>
        public async Task Delay(int ms)
        {
            try
            {
                await BaseScript.Delay(ms);
            }
            catch (Exception e)
            {
                log.Error(e.ToString());
            }
        }

        public bool StartNewScriptV2(string sourceName, params object[] args)
        {
            //try
            //{
            //    //FileInfo sourceFile = new FileInfo(sourceName);
            //    CodeDomProvider provider = null;
            //    bool compileOk = false;

            //    //// Select the code provider based on the input file extension.

            //    //var CodeDomProviderType = Type.GetType("Microsoft.CSharp.CSharpCodeProvider, System, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089");

            //    if (sourceName.EndsWith(".cs", StringComparison.CurrentCultureIgnoreCase))
            //    {
            //        log.Debug("File ends in cs, creating provider");
            //    }
            //    else
            //    {
            //        log.Debug("Source file must have a .cs or .vb extension");
            //        return false;
            //    }

            //    using (var CodeDomProviderType = new CSharpCodeProvider())
            //    {
            //        // Format the executable file name.
            //        // Build the output assembly path using the current directory
            //        // and <source>_cs.exe or <source>_vb.exe.

            //        //String exeName = String.Format(@"{0}\{1}.exe",
            //        //    System.Environment.CurrentDirectory,
            //        //    sourceFile.Name.Replace(".", "_"));


            //        //var st = new HybridDictionary();
            //        log.Debug(string.Empty);
            //        CompilerParameters cp = new CompilerParameters()
            //        {

            //            // Generate an executable instead of
            //            // a class library.
            //            GenerateExecutable = false,

            //            // Specify the assembly file name to generate.
            //            //cp.OutputAssembly = exeName;

            //            // Save the assembly as a physical file.
            //            GenerateInMemory = true,

            //            // Set whether to treat all warnings as errors.
            //            TreatWarningsAsErrors = false
            //        };

            //        cp.ReferencedAssemblies.Add(Assembly.GetExecutingAssembly().FullName);

            //        var sourceCode =
            //            "using System.IO;" +
            //            "using CodeProviders;" +
            //            "using System.Threading;" +
            //            "using System.Threading.Tasks; " +
            //            "namespace ScriptReferences" +
            //            "{" +
            //                "public class " + Path.GetFileNameWithoutExtension(sourceName) +
            //                "{" +
            //                     ResourceFile.ReadAll(sourceName) +
            //                "}" +
            //            "}";

            //        //// Invoke compilation of the source file.
            //        CompilerResults cr = provider.CompileAssemblyFromSource(null, sourceCode);

            //        if (cr.Errors.Count > 0)
            //        {
            //            // Display compilation errors.
            //            log.Debug(string.Format("Errors building {0} into {1}",
            //                sourceName, cr.PathToAssembly));
            //            foreach (CompilerError ce in cr.Errors)
            //            {
            //                log.Debug(string.Format("  {0}", ce.ToString()));
            //            }
            //        }
            //        else
            //        {
            //            // Display a successful compilation message.
            //            log.Debug(string.Format("Source {0} built into {1} successfully.",
            //                sourceName, cr.CompiledAssembly.FullName));
            //        }

            //        // Return the results of the compilation.
            //        if (cr.Errors.Count > 0)
            //        {
            //            compileOk = false;
            //        }
            //        else
            //        {
            //            compileOk = true;
            //        }
            //    }

            //    return compileOk;
            //}
            //catch (Exception e)
            //{
            //    log.Error(e.ToString());
            //}
            return false;
        }

        //public bool StartNewScriptV3(string sourceName, params object[] args)
        //{
        //    try
        //    {
        //        var CodeDomProviderType = new CSharpScriptExecution();

        //        CodeDomProviderType.AddAssembly(Assembly.GetExecutingAssembly().FullName);

        //        CodeDomProviderType.AddNamespace("RPOnline.Parts");

        //        CodeDomProviderType.ExecuteCode(ResourceFile.ReadAll(sourceName), new object[] {}, new CancellationTokenSource());

        //        return true;
        //    }
        //    catch (Exception e)
        //    {
        //        log.Error(e.ToString());
        //    }
        //    return false;
        //}
    }
}
