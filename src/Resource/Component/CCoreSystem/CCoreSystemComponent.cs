﻿using CitizenFX.Core;
using Common.Logging;

using Proline.Resource.Framework;
using Resource.Component.CCoreSystem.Commands;
using Resource.Component.CCoreSystem.Scripts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Resource.Component;

namespace Resource.Component.CCoreSystem
{
    internal class CCoreSystemComponent : ResourceComponent, ICCoreSystemComponent
    {
        private static readonly ILog log = Proline.Resource.Logging.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private readonly ResourceScript script;
        private readonly ICCoreSystemComponentController controller;
        private ScriptManager _sm;

        public CCoreSystemComponent(ResourceScript script, ICCoreSystemComponentController controller) : base(nameof(CCoreSystemComponent), null)
        {
            this.script = script;
            this.controller = controller;
        }

        public override void OnLoad()
        {
            RegisterScript<InitCore>();


            script.RegisterCommand<ListActiveScriptsCommand>();
            script.RegisterCommand<ListAllTasksCommand>();
            script.RegisterCommand<ListScriptsCommand>();
            script.RegisterCommand<StartNewScriptCommand>();
            script.RegisterCommand<StopAllScriptInstancesCommand>();

            _sm.GetAllScriptTypes();

        }

        public override void OnStart()
        {
            log.Info(controller);
            controller.StartNewScript("Main");
        }

    }
}
