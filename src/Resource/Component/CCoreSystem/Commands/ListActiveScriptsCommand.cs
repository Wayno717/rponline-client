﻿
using Common.Logging;
using Proline.Resource.Framework;
using Resource.Component.CCoreSystem;
using System.Linq;

namespace Resource.Component.CCoreSystem.Commands
{
    public class ListActiveScriptsCommand : ResourceCommand
    {
        private static readonly ILog log = Proline.Resource.Logging.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public ListActiveScriptsCommand()
        {
        }

        protected override void OnCommandExecute(params object[] args)
        {
            var sm = ScriptManager.GetInstance().LiveScripts;
            foreach (var script in sm)
            {
                var instances = sm.Where(e => e.Name.Equals(script.Name));
                var count = instances.Count();
                log.Debug(string.Format("Script: {0} Instances: {1}", script.Name, count));
                foreach (var instance in instances)
                {
                    log.Debug(string.Format("-GUID: {0}", script.InstanceId));
                    var scriptTask = script.ExecutionTask;
                    log.Debug(string.Format("-Task Id {0}, Is Complete {1}, Status {2} ", scriptTask.Id, scriptTask.IsCompleted, scriptTask.Status));
                }
            }
        }
    }
}
