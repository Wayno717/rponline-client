﻿
using Proline.Resource.Framework;
using Resource.Component.CCoreSystem;
using System.Linq;

namespace Resource.Component.CCoreSystem.Commands
{
    public class StopAllScriptInstancesCommand : ResourceCommand
    {
        public StopAllScriptInstancesCommand()
        {
        }

        protected override void OnCommandExecute(params object[] args)
        {
            var sm = ScriptManager.GetInstance().LiveScripts;
            if (args.Count() == 0)
            {
                foreach (var script in sm)
                {
                    script.Terminate();
                }
            }
            else
            {
                var scriptName = args[0].ToString();
                var scripts = sm.Where(e => e.Name.Equals(scriptName));
                foreach (var script in scripts)
                {
                    script.Terminate();
                }
            }
        }
    }
}
