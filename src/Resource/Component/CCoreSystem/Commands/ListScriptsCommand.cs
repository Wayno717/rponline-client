﻿
using Common.Logging;
using Proline.Resource.Framework;
using Resource.Component.CCoreSystem;

namespace Resource.Component.CCoreSystem.Commands
{
    public class ListScriptsCommand : ResourceCommand
    {
        private static readonly ILog log = Proline.Resource.Logging.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public ListScriptsCommand()
        {
        }

        protected override void OnCommandExecute(params object[] args)
        {
            foreach (var item in ScriptManager.GetInstance().GetScriptNames())
            {
                log.Debug(item);
            }
        }
    }
}
