﻿using Proline.Resource.Framework;
using System.Linq;

namespace Resource.Component.CCoreSystem.Commands
{
    public class StartNewScriptCommand : ResourceCommand
    {
        public StartNewScriptCommand()
        {
        }

        protected override void OnCommandExecute(params object[] args)
        {
            if (args.Count() == 0)
            {
                return;
            }
            var scriptName = args[0].ToString();
            ResourceAPI.StartNewScript(scriptName);
        }
    }
}
