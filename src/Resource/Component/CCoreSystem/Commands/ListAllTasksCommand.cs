﻿
using Common.Logging;
using Proline.Resource.Framework;
using Resource.Component.CCoreSystem;

namespace Resource.Component.CCoreSystem.Commands
{
    public class ListAllTasksCommand : ResourceCommand
    {
        private static readonly ILog log = Proline.Resource.Logging.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        protected override void OnCommandExecute(params object[] args)
        {
            foreach (var scriptTask in TaskManager.Tasks)
            {
                log.Debug(string.Format("Task Id {0}, Is Complete {1}, Status {2} ", scriptTask.Id, scriptTask.IsCompleted, scriptTask.Status));
            }
        }
    }
}
