﻿using Common.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Resource.Component.CCoreSystem
{
    internal class ScriptManager
    {
        private static readonly ILog log = Proline.Resource.Logging.LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private List<LiveScript> _liveScripts;
        private static ScriptManager _instance;
         
        public List<LiveScript> LiveScripts
        {
            get
            {
                if (_liveScripts == null)
                    _liveScripts = new List<LiveScript>();
                return _liveScripts;
            }
        }

        internal static ScriptManager GetInstance()
        {
            if (_instance == null)
                _instance = new ScriptManager();
            return _instance;
        }

        internal void ManageScript(LiveScript script)
        {
            LiveScripts.Add(script);
        }

        internal void UnmanageScript(LiveScript liveScript)
        {
            LiveScripts.Remove(liveScript);
        }

        internal void CleanUpCompletedScripts()
        {
            var instances = LiveScripts.Where(e => e.IsCompleted).ToArray();
            if (instances.Count() > 0)
                log.Debug(string.Format("Removing {0} script instances that have been completed", instances.Count()));
            int count = 0;
            foreach (var instance in instances)
            {
                var instanceCount = LiveScripts.Remove(instance);
                count++;
                log.Debug(string.Format("{0} Removed {1} instances", instance.Name, 1));
            }
            if (instances.Count() > 0)
                log.Debug(string.Format("Removed {0} script instances that have been completed", count));
        }

        internal LiveScript GetLiveScript(object scriptInstance)
        {
            return LiveScripts.FirstOrDefault(e => e.Instance == scriptInstance);
        }

        internal IEnumerable<string> GetScriptNames()
        {
            var types = GetAllScriptTypes();
            return types.Keys;
        }

        internal Dictionary<string, Type> GetAllScriptTypes()
        {  
            var scriptTypes = new Dictionary<string, Type>();
            log.Debug($"Scanning current assembly for scripts");
            var types = Assembly.GetExecutingAssembly().GetTypes().Where(e => (object)e.GetMethod("Execute") != null
            && e.IsClass && e.Namespace.Contains("Resource.Scripts"));
            log.Debug($"Found {types.Count()} scripts that have an execute method");
            foreach (var item in types)
            {
                //log.Debug($"{item.Name}");
                if (!scriptTypes.ContainsKey(item.Name))
                    scriptTypes.Add(item.Name, item);
                else
                    log.Debug($"{item.Name} DUPLICATE?????");
            }
            return scriptTypes;
        }


        internal void TerminateMarkedAsNoLongerNeedeScripts()
        {
            var instances = LiveScripts.Where(e => e.IsMarkedForNolongerNeeded).ToArray();
            if (instances.Count() > 0)
                log.Debug(string.Format("Several scripts have been marked as no longer needed {0}, terminating them", instances.Count()));
            foreach (var instance in instances)
            {
                TaskManager.StartNew(instance.Terminate);
            }
        }
    }
}
