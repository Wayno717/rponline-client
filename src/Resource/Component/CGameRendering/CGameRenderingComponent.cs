﻿using CitizenFX.Core;
using CitizenFX.Core.Native;
using Resource.Component.CScreenRendering;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Resource.Component.CGameRendering
{
    internal class CGameRenderingComponent : ResourceComponent, ICGameRenderingComponent
    {
        public CGameRenderingComponent() : base(nameof(CGameRenderingComponent), null)
        {

        }

        public override void OnLoad()
        {

        }

        public override void OnStart()
        {

        }

    }
}
