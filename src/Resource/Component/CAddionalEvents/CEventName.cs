﻿using CitizenFX.Core;
using Common.Logging;
using Newtonsoft.Json;
using Proline.Resource.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Resource.Component.CAddionalEvents
{
    internal class CEventName : ResourceEvent
    {
        private static readonly ILog log = Proline.Resource.Logging.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private readonly EventHandlerDictionary handler;

        public CEventName(EventHandlerDictionary handler)
        {
            this.handler = handler;
        }

        internal void OnCEventAcquaintancePedTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventAcquaintancePed", number, eventId, data);
        }
        internal void OnCEventAcquaintancePedDeadTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventAcquaintancePedDead", number, eventId, data);
        }
        internal void OnCEventAcquaintancePedDislikeTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventAcquaintancePedDislike", number, eventId, data);
        }
        internal void OnCEventAcquaintancePedHateTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventAcquaintancePedHate", number, eventId, data);
        }
        internal void OnCEventAcquaintancePedLikeTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventAcquaintancePedLike", number, eventId, data);
        }
        internal void OnCEventAcquaintancePedWantedTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventAcquaintancePedWanted", number, eventId, data);
        }
        internal void OnCEventAgitatedTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventAgitated", number, eventId, data);
        }

        internal void Subscribe()
        {
            handler.Add("CEventAcquaintancePed", new Action<object, object, object>(this.OnCEventAcquaintancePedTriggered));
            handler.Add("CEventAcquaintancePedDead", new Action<object, object, object>(this.OnCEventAcquaintancePedDeadTriggered));
            handler.Add("CEventAcquaintancePedDislike", new Action<object, object, object>(this.OnCEventAcquaintancePedDislikeTriggered));
            handler.Add("CEventAcquaintancePedHate", new Action<object, object, object>(this.OnCEventAcquaintancePedHateTriggered));
            handler.Add("CEventAcquaintancePedLike", new Action<object, object, object>(this.OnCEventAcquaintancePedLikeTriggered));
            handler.Add("CEventAcquaintancePedWanted", new Action<object, object, object>(this.OnCEventAcquaintancePedWantedTriggered));
            handler.Add("CEventAgitated", new Action<object, object, object>(this.OnCEventAgitatedTriggered));
            handler.Add("CEventAgitatedAction", new Action<object, object, object>(this.OnCEventAgitatedActionTriggered));
            handler.Add("CEventCallForCover", new Action<object, object, object>(this.OnCEventCallForCoverTriggered));
            handler.Add("CEventCarUndriveable", new Action<object, object, object>(this.OnCEventCarUndriveableTriggered));
            handler.Add("CEventClimbLadderOnRoute", new Action<object, object, object>(this.OnCEventClimbLadderOnRouteTriggered));
            handler.Add("CEventClimbNavMeshOnRoute", new Action<object, object, object>(this.OnCEventClimbNavMeshOnRouteTriggered));
            handler.Add("CEventCombatTaunt", new Action<object, object, object>(this.OnCEventCombatTauntTriggered));
            handler.Add("CEventCommunicateEvent", new Action<object, object, object>(this.OnCEventCommunicateEventTriggered));
            handler.Add("CEventCopCarBeingStolen", new Action<object, object, object>(this.OnCEventCopCarBeingStolenTriggered));
            handler.Add("CEventCrimeCryForHelp", new Action<object, object, object>(this.OnCEventCrimeCryForHelpTriggered));
            handler.Add("CEventCrimeReported", new Action<object, object, object>(this.OnCEventCrimeReportedTriggered));
            handler.Add("CEventDamage", new Action<object, object, object>(this.OnCEventDamageTriggered));
            handler.Add("CEventDataDecisionMaker", new Action<object, object, object>(this.OnCEventDataDecisionMakerTriggered));
            handler.Add("CEventDataFileMounter", new Action<object, object, object>(this.OnCEventDataFileMounterTriggered));
            handler.Add("CEventDataResponseAggressiveRubberneck", new Action<object, object, object>(this.OnCEventDataResponseAggressiveRubberneckTriggered));
            handler.Add("CEventDataResponseDeferToScenarioPointFlags", new Action<object, object, object>(this.OnCEventDataResponseDeferToScenarioPointFlagsTriggered));
            handler.Add("CEventDataResponseFriendlyAimedAt", new Action<object, object, object>(this.OnCEventDataResponseFriendlyAimedAtTriggered));
            handler.Add("CEventDataResponseFriendlyNearMiss", new Action<object, object, object>(this.OnCEventDataResponseFriendlyNearMissTriggered));
            handler.Add("CEventDataResponsePlayerDeath", new Action<object, object, object>(this.OnCEventDataResponsePlayerDeathTriggered));
            handler.Add("CEventDataResponsePoliceTaskWanted", new Action<object, object, object>(this.OnCEventDataResponsePoliceTaskWantedTriggered));
            handler.Add("CEventDataResponseSwatTaskWanted", new Action<object, object, object>(this.OnCEventDataResponseSwatTaskWantedTriggered));
            handler.Add("CEventDataResponseTask", new Action<object, object, object>(this.OnCEventDataResponseTaskTriggered));
            handler.Add("CEventDataResponseTaskAgitated", new Action<object, object, object>(this.OnCEventDataResponseTaskAgitatedTriggered));
            handler.Add("CEventDataResponseTaskCombat", new Action<object, object, object>(this.OnCEventDataResponseTaskCombatTriggered));
            handler.Add("CEventDataResponseTaskCower", new Action<object, object, object>(this.OnCEventDataResponseTaskCowerTriggered));
            handler.Add("CEventDataResponseTaskCrouch", new Action<object, object, object>(this.OnCEventDataResponseTaskCrouchTriggered));
            handler.Add("CEventDataResponseTaskDuckAndCover", new Action<object, object, object>(this.OnCEventDataResponseTaskDuckAndCoverTriggered));
            handler.Add("CEventDataResponseTaskEscapeBlast", new Action<object, object, object>(this.OnCEventDataResponseTaskEscapeBlastTriggered));
            handler.Add("CEventDataResponseTaskEvasiveStep", new Action<object, object, object>(this.OnCEventDataResponseTaskEvasiveStepTriggered));
            handler.Add("CEventDataResponseTaskExhaustedFlee", new Action<object, object, object>(this.OnCEventDataResponseTaskExhaustedFleeTriggered));
            handler.Add("CEventDataResponseTaskExplosion", new Action<object, object, object>(this.OnCEventDataResponseTaskExplosionTriggered));
            handler.Add("CEventDataResponseTaskFlee", new Action<object, object, object>(this.OnCEventDataResponseTaskFleeTriggered));
            handler.Add("CEventDataResponseTaskFlyAway", new Action<object, object, object>(this.OnCEventDataResponseTaskFlyAwayTriggered));
            handler.Add("CEventDataResponseTaskGrowlAndFlee", new Action<object, object, object>(this.OnCEventDataResponseTaskGrowlAndFleeTriggered));
            handler.Add("CEventDataResponseTaskGunAimedAt", new Action<object, object, object>(this.OnCEventDataResponseTaskGunAimedAtTriggered));
            handler.Add("CEventDataResponseTaskHandsUp", new Action<object, object, object>(this.OnCEventDataResponseTaskHandsUpTriggered));
            handler.Add("CEventDataResponseTaskHeadTrack", new Action<object, object, object>(this.OnCEventDataResponseTaskHeadTrackTriggered));
            handler.Add("CEventDataResponseTaskLeaveCarAndFlee", new Action<object, object, object>(this.OnCEventDataResponseTaskLeaveCarAndFleeTriggered));
            handler.Add("CEventDataResponseTaskScenarioFlee", new Action<object, object, object>(this.OnCEventDataResponseTaskScenarioFleeTriggered));
            handler.Add("CEventDataResponseTaskSharkAttack", new Action<object, object, object>(this.OnCEventDataResponseTaskSharkAttackTriggered));
            handler.Add("CEventDataResponseTaskShockingEventBackAway", new Action<object, object, object>(this.OnCEventDataResponseTaskShockingEventBackAwayTriggered));
            handler.Add("CEventDataResponseTaskShockingEventGoto", new Action<object, object, object>(this.OnCEventDataResponseTaskShockingEventGotoTriggered));
            handler.Add("CEventDataResponseTaskShockingEventHurryAway", new Action<object, object, object>(this.OnCEventDataResponseTaskShockingEventHurryAwayTriggered));
            handler.Add("CEventDataResponseTaskShockingEventReact", new Action<object, object, object>(this.OnCEventDataResponseTaskShockingEventReactTriggered));
            handler.Add("CEventDataResponseTaskShockingEventReactToAircraft", new Action<object, object, object>(this.OnCEventDataResponseTaskShockingEventReactToAircraftTriggered));
            handler.Add("CEventDataResponseTaskShockingEventStopAndStare", new Action<object, object, object>(this.OnCEventDataResponseTaskShockingEventStopAndStareTriggered));
            handler.Add("CEventDataResponseTaskShockingEventThreatResponse", new Action<object, object, object>(this.OnCEventDataResponseTaskShockingEventThreatResponseTriggered));
            handler.Add("CEventDataResponseTaskShockingEventWatch", new Action<object, object, object>(this.OnCEventDataResponseTaskShockingEventWatchTriggered));
            handler.Add("CEventDataResponseTaskShockingNiceCar", new Action<object, object, object>(this.OnCEventDataResponseTaskShockingNiceCarTriggered));
            handler.Add("CEventDataResponseTaskShockingPoliceInvestigate", new Action<object, object, object>(this.OnCEventDataResponseTaskShockingPoliceInvestigateTriggered));
            handler.Add("CEventDataResponseTaskThreat", new Action<object, object, object>(this.OnCEventDataResponseTaskThreatTriggered));
            handler.Add("CEventDataResponseTaskTurnToFace", new Action<object, object, object>(this.OnCEventDataResponseTaskTurnToFaceTriggered));
            handler.Add("CEventDataResponseTaskWalkAway", new Action<object, object, object>(this.OnCEventDataResponseTaskWalkAwayTriggered));
            handler.Add("CEventDataResponseTaskWalkRoundEntity", new Action<object, object, object>(this.OnCEventDataResponseTaskWalkRoundEntityTriggered));
            handler.Add("CEventDataResponseTaskWalkRoundFire", new Action<object, object, object>(this.OnCEventDataResponseTaskWalkRoundFireTriggered));
            handler.Add("CEventDeadPedFound", new Action<object, object, object>(this.OnCEventDeadPedFoundTriggered));
            handler.Add("CEventDeath", new Action<object, object, object>(this.OnCEventDeathTriggered));
            handler.Add("CEventDecisionMakerResponse", new Action<object, object, object>(this.OnCEventDecisionMakerResponseTriggered));
            handler.Add("CEventDisturbance", new Action<object, object, object>(this.OnCEventDisturbanceTriggered));
            handler.Add("CEventDraggedOutCar", new Action<object, object, object>(this.OnCEventDraggedOutCarTriggered));
            handler.Add("CEventEditableResponse", new Action<object, object, object>(this.OnCEventEditableResponseTriggered));
            handler.Add("CEventEncroachingPed", new Action<object, object, object>(this.OnCEventEncroachingPedTriggered));
            handler.Add("CEventEntityDamaged", new Action<object, object, object>(this.OnCEventEntityDamagedTriggered));
            handler.Add("CEventEntityDestroyed", new Action<object, object, object>(this.OnCEventEntityDestroyedTriggered));
            handler.Add("CEventExplosion", new Action<object, object, object>(this.OnCEventExplosionTriggered));
            handler.Add("CEventExplosionHeard", new Action<object, object, object>(this.OnCEventExplosionHeardTriggered));
            handler.Add("CEventFireNearby", new Action<object, object, object>(this.OnCEventFireNearbyTriggered));
            handler.Add("CEventFootStepHeard", new Action<object, object, object>(this.OnCEventFootStepHeardTriggered));
            handler.Add("CEventFriendlyAimedAt", new Action<object, object, object>(this.OnCEventFriendlyAimedAtTriggered));
            handler.Add("CEventFriendlyFireNearMiss", new Action<object, object, object>(this.OnCEventFriendlyFireNearMissTriggered));
            handler.Add("CEventGetOutOfWater", new Action<object, object, object>(this.OnCEventGetOutOfWaterTriggered));
            handler.Add("CEventGivePedTask", new Action<object, object, object>(this.OnCEventGivePedTaskTriggered));
            handler.Add("CEventGroupScriptAI", new Action<object, object, object>(this.OnCEventGroupScriptAITriggered));
            handler.Add("CEventGroupScriptNetwork", new Action<object, object, object>(this.OnCEventGroupScriptNetworkTriggered));
            handler.Add("CEventGunAimedAt", new Action<object, object, object>(this.OnCEventGunAimedAtTriggered));
            handler.Add("CEventGunShot", new Action<object, object, object>(this.OnCEventGunShotTriggered));
            handler.Add("CEventGunShotBulletImpact", new Action<object, object, object>(this.OnCEventGunShotBulletImpactTriggered));
            handler.Add("CEventGunShotWhizzedBy", new Action<object, object, object>(this.OnCEventGunShotWhizzedByTriggered));
            handler.Add("CEventHelpAmbientFriend", new Action<object, object, object>(this.OnCEventHelpAmbientFriendTriggered));
            handler.Add("CEventHurtTransition", new Action<object, object, object>(this.OnCEventHurtTransitionTriggered));
            handler.Add("CEventInAir", new Action<object, object, object>(this.OnCEventInAirTriggered));
            handler.Add("CEventInfo", new Action<object, object, object>(this.OnCEventInfoTriggered));
            handler.Add("CEventInfoBase", new Action<object, object, object>(this.OnCEventInfoBaseTriggered));
            handler.Add("CEventInjuredCryForHelp", new Action<object, object, object>(this.OnCEventInjuredCryForHelpTriggered));
            handler.Add("CEventLeaderEnteredCarAsDriver", new Action<object, object, object>(this.OnCEventLeaderEnteredCarAsDriverTriggered));
            handler.Add("CEventLeaderExitedCarAsDriver", new Action<object, object, object>(this.OnCEventLeaderExitedCarAsDriverTriggered));
            handler.Add("CEventLeaderHolsteredWeapon", new Action<object, object, object>(this.OnCEventLeaderHolsteredWeaponTriggered));
            handler.Add("CEventLeaderLeftCover", new Action<object, object, object>(this.OnCEventLeaderLeftCoverTriggered));
            handler.Add("CEventLeaderUnholsteredWeapon", new Action<object, object, object>(this.OnCEventLeaderUnholsteredWeaponTriggered));
            handler.Add("CEventMeleeAction", new Action<object, object, object>(this.OnCEventMeleeActionTriggered));
            handler.Add("CEventMustLeaveBoat", new Action<object, object, object>(this.OnCEventMustLeaveBoatTriggered));
            handler.Add("CEventNetworkAdminInvited", new Action<object, object, object>(this.OnCEventNetworkAdminInvitedTriggered));
            handler.Add("CEventNetworkAttemptHostMigration", new Action<object, object, object>(this.OnCEventNetworkAttemptHostMigrationTriggered));
            handler.Add("CEventNetworkBail", new Action<object, object, object>(this.OnCEventNetworkBailTriggered));
            handler.Add("CEventNetworkCashTransactionLog", new Action<object, object, object>(this.OnCEventNetworkCashTransactionLogTriggered));
            handler.Add("CEventNetworkCheatTriggered", new Action<object, object, object>(this.OnCEventNetworkCheatTriggeredTriggered));
            handler.Add("CEventNetworkClanInviteReceived", new Action<object, object, object>(this.OnCEventNetworkClanInviteReceivedTriggered));
            handler.Add("CEventNetworkClanJoined", new Action<object, object, object>(this.OnCEventNetworkClanJoinedTriggered));
            handler.Add("CEventNetworkClanKicked", new Action<object, object, object>(this.OnCEventNetworkClanKickedTriggered));
            handler.Add("CEventNetworkClanLeft", new Action<object, object, object>(this.OnCEventNetworkClanLeftTriggered));
            handler.Add("CEventNetworkClanRankChanged", new Action<object, object, object>(this.OnCEventNetworkClanRankChangedTriggered));
            handler.Add("CEventNetworkCloudEvent", new Action<object, object, object>(this.OnCEventNetworkCloudEventTriggered));
            handler.Add("CEventNetworkCloudFileResponse", new Action<object, object, object>(this.OnCEventNetworkCloudFileResponseTriggered));
            handler.Add("CEventNetworkEmailReceivedEvent", new Action<object, object, object>(this.OnCEventNetworkEmailReceivedEventTriggered));
            handler.Add("CEventNetworkEndMatch", new Action<object, object, object>(this.OnCEventNetworkEndMatchTriggered));
            handler.Add("CEventNetworkEndSession", new Action<object, object, object>(this.OnCEventNetworkEndSessionTriggered));
            handler.Add("CEventNetworkEntityDamage", new Action<object, object, object>(this.OnCEventNetworkEntityDamageTriggered));
            handler.Add("CEventNetworkFindSession", new Action<object, object, object>(this.OnCEventNetworkFindSessionTriggered));
            handler.Add("CEventNetworkFollowInviteReceived", new Action<object, object, object>(this.OnCEventNetworkFollowInviteReceivedTriggered));
            handler.Add("CEventNetworkHostMigration", new Action<object, object, object>(this.OnCEventNetworkHostMigrationTriggered));
            handler.Add("CEventNetworkHostSession", new Action<object, object, object>(this.OnCEventNetworkHostSessionTriggered));
            handler.Add("CEventNetworkIncrementStat", new Action<object, object, object>(this.OnCEventNetworkIncrementStatTriggered));
            handler.Add("CEventNetworkInviteAccepted", new Action<object, object, object>(this.OnCEventNetworkInviteAcceptedTriggered));
            handler.Add("CEventNetworkInviteConfirmed", new Action<object, object, object>(this.OnCEventNetworkInviteConfirmedTriggered));
            handler.Add("CEventNetworkInviteRejected", new Action<object, object, object>(this.OnCEventNetworkInviteRejectedTriggered));
            handler.Add("CEventNetworkJoinSession", new Action<object, object, object>(this.OnCEventNetworkJoinSessionTriggered));
            handler.Add("CEventNetworkJoinSessionResponse", new Action<object, object, object>(this.OnCEventNetworkJoinSessionResponseTriggered));
            handler.Add("CEventNetworkOnlinePermissionsUpdated", new Action<object, object, object>(this.OnCEventNetworkOnlinePermissionsUpdatedTriggered));
            handler.Add("CEventNetworkPedLeftBehind", new Action<object, object, object>(this.OnCEventNetworkPedLeftBehindTriggered));
            handler.Add("CEventNetworkPickupRespawned", new Action<object, object, object>(this.OnCEventNetworkPickupRespawnedTriggered));
            handler.Add("CEventNetworkPlayerArrest", new Action<object, object, object>(this.OnCEventNetworkPlayerArrestTriggered));
            handler.Add("CEventNetworkPlayerCollectedAmbientPickup", new Action<object, object, object>(this.OnCEventNetworkPlayerCollectedAmbientPickupTriggered));
            handler.Add("CEventNetworkPlayerCollectedPickup", new Action<object, object, object>(this.OnCEventNetworkPlayerCollectedPickupTriggered));
            handler.Add("CEventNetworkPlayerCollectedPortablePickup", new Action<object, object, object>(this.OnCEventNetworkPlayerCollectedPortablePickupTriggered));
            handler.Add("CEventNetworkPlayerDroppedPortablePickup", new Action<object, object, object>(this.OnCEventNetworkPlayerDroppedPortablePickupTriggered));
            handler.Add("CEventNetworkPlayerJoinScript", new Action<object, object, object>(this.OnCEventNetworkPlayerJoinScriptTriggered));
            handler.Add("CEventNetworkPlayerLeftScript", new Action<object, object, object>(this.OnCEventNetworkPlayerLeftScriptTriggered));
            handler.Add("CEventNetworkPlayerScript", new Action<object, object, object>(this.OnCEventNetworkPlayerScriptTriggered));
            handler.Add("CEventNetworkPlayerSession", new Action<object, object, object>(this.OnCEventNetworkPlayerSessionTriggered));
            handler.Add("CEventNetworkPlayerSpawn", new Action<object, object, object>(this.OnCEventNetworkPlayerSpawnTriggered));
            handler.Add("CEventNetworkPresenceInvite", new Action<object, object, object>(this.OnCEventNetworkPresenceInviteTriggered));
            handler.Add("CEventNetworkPresenceInviteRemoved", new Action<object, object, object>(this.OnCEventNetworkPresenceInviteRemovedTriggered));
            handler.Add("CEventNetworkPresenceInviteReply", new Action<object, object, object>(this.OnCEventNetworkPresenceInviteReplyTriggered));
            handler.Add("CEventNetworkPresenceTriggerEvent", new Action<object, object, object>(this.OnCEventNetworkPresenceTriggerEventTriggered));
            handler.Add("CEventNetworkPresence_StatUpdate", new Action<object, object, object>(this.OnCEventNetworkPresence_StatUpdateTriggered));
            handler.Add("CEventNetworkPrimaryClanChanged", new Action<object, object, object>(this.OnCEventNetworkPrimaryClanChangedTriggered));
            handler.Add("CEventNetworkRequestDelay", new Action<object, object, object>(this.OnCEventNetworkRequestDelayTriggered));
            handler.Add("CEventNetworkRosChanged", new Action<object, object, object>(this.OnCEventNetworkRosChangedTriggered));
            handler.Add("CEventNetworkScAdminPlayerUpdated", new Action<object, object, object>(this.OnCEventNetworkScAdminPlayerUpdatedTriggered));
            handler.Add("CEventNetworkScAdminReceivedCash", new Action<object, object, object>(this.OnCEventNetworkScAdminReceivedCashTriggered));
            handler.Add("CEventNetworkScriptEvent", new Action<object, object, object>(this.OnCEventNetworkScriptEventTriggered));
            handler.Add("CEventNetworkSessionEvent", new Action<object, object, object>(this.OnCEventNetworkSessionEventTriggered));
            handler.Add("CEventNetworkShopTransaction", new Action<object, object, object>(this.OnCEventNetworkShopTransactionTriggered));
            handler.Add("CEventNetworkSignInStateChanged", new Action<object, object, object>(this.OnCEventNetworkSignInStateChangedTriggered));
            handler.Add("CEventNetworkSocialClubAccountLinked", new Action<object, object, object>(this.OnCEventNetworkSocialClubAccountLinkedTriggered));
            handler.Add("CEventNetworkSpectateLocal", new Action<object, object, object>(this.OnCEventNetworkSpectateLocalTriggered));
            handler.Add("CEventNetworkStartMatch", new Action<object, object, object>(this.OnCEventNetworkStartMatchTriggered));
            handler.Add("CEventNetworkStartSession", new Action<object, object, object>(this.OnCEventNetworkStartSessionTriggered));
            handler.Add("CEventNetworkStorePlayerLeft", new Action<object, object, object>(this.OnCEventNetworkStorePlayerLeftTriggered));
            handler.Add("CEventNetworkSummon", new Action<object, object, object>(this.OnCEventNetworkSummonTriggered));
            handler.Add("CEventNetworkSystemServiceEvent", new Action<object, object, object>(this.OnCEventNetworkSystemServiceEventTriggered));
            handler.Add("CEventNetworkTextMessageReceived", new Action<object, object, object>(this.OnCEventNetworkTextMessageReceivedTriggered));
            handler.Add("CEventNetworkTimedExplosion", new Action<object, object, object>(this.OnCEventNetworkTimedExplosionTriggered));
            handler.Add("CEventNetworkTransitionEvent", new Action<object, object, object>(this.OnCEventNetworkTransitionEventTriggered));
            handler.Add("CEventNetworkTransitionGamerInstruction", new Action<object, object, object>(this.OnCEventNetworkTransitionGamerInstructionTriggered));
            handler.Add("CEventNetworkTransitionMemberJoined", new Action<object, object, object>(this.OnCEventNetworkTransitionMemberJoinedTriggered));
            handler.Add("CEventNetworkTransitionMemberLeft", new Action<object, object, object>(this.OnCEventNetworkTransitionMemberLeftTriggered));
            handler.Add("CEventNetworkTransitionParameterChanged", new Action<object, object, object>(this.OnCEventNetworkTransitionParameterChangedTriggered));
            handler.Add("CEventNetworkTransitionStarted", new Action<object, object, object>(this.OnCEventNetworkTransitionStartedTriggered));
            handler.Add("CEventNetworkTransitionStringChanged", new Action<object, object, object>(this.OnCEventNetworkTransitionStringChangedTriggered));
            handler.Add("CEventNetworkVehicleUndrivable", new Action<object, object, object>(this.OnCEventNetworkVehicleUndrivableTriggered));
            handler.Add("CEventNetworkVoiceConnectionRequested", new Action<object, object, object>(this.OnCEventNetworkVoiceConnectionRequestedTriggered));
            handler.Add("CEventNetworkVoiceConnectionResponse", new Action<object, object, object>(this.OnCEventNetworkVoiceConnectionResponseTriggered));
            handler.Add("CEventNetworkVoiceConnectionTerminated", new Action<object, object, object>(this.OnCEventNetworkVoiceConnectionTerminatedTriggered));
            handler.Add("CEventNetworkVoiceSessionEnded", new Action<object, object, object>(this.OnCEventNetworkVoiceSessionEndedTriggered));
            handler.Add("CEventNetworkVoiceSessionStarted", new Action<object, object, object>(this.OnCEventNetworkVoiceSessionStartedTriggered));
            handler.Add("CEventNetworkWithData", new Action<object, object, object>(this.OnCEventNetworkWithDataTriggered));
            handler.Add("CEventNetwork_InboxMsgReceived", new Action<object, object, object>(this.OnCEventNetwork_InboxMsgReceivedTriggered));
            handler.Add("CEventNewTask", new Action<object, object, object>(this.OnCEventNewTaskTriggered));
            handler.Add("CEventObjectCollision", new Action<object, object, object>(this.OnCEventObjectCollisionTriggered));
            handler.Add("CEventOnFire", new Action<object, object, object>(this.OnCEventOnFireTriggered));
            handler.Add("CEventOpenDoor", new Action<object, object, object>(this.OnCEventOpenDoorTriggered));
            handler.Add("CEventPedCollisionWithPed", new Action<object, object, object>(this.OnCEventPedCollisionWithPedTriggered));
            handler.Add("CEventPedCollisionWithPlayer", new Action<object, object, object>(this.OnCEventPedCollisionWithPlayerTriggered));
            handler.Add("CEventPedEnteredMyVehicle", new Action<object, object, object>(this.OnCEventPedEnteredMyVehicleTriggered));
            handler.Add("CEventPedJackingMyVehicle", new Action<object, object, object>(this.OnCEventPedJackingMyVehicleTriggered));
            handler.Add("CEventPedOnCarRoof", new Action<object, object, object>(this.OnCEventPedOnCarRoofTriggered));
            handler.Add("CEventPedSeenDeadPed", new Action<object, object, object>(this.OnCEventPedSeenDeadPedTriggered));
            handler.Add("CEventPlayerCollisionWithPed", new Action<object, object, object>(this.OnCEventPlayerCollisionWithPedTriggered));
            handler.Add("CEventPlayerDeath", new Action<object, object, object>(this.OnCEventPlayerDeathTriggered));
            handler.Add("CEventPlayerUnableToEnterVehicle", new Action<object, object, object>(this.OnCEventPlayerUnableToEnterVehicleTriggered));
            handler.Add("CEventPotentialBeWalkedInto", new Action<object, object, object>(this.OnCEventPotentialBeWalkedIntoTriggered));
            handler.Add("CEventPotentialBlast", new Action<object, object, object>(this.OnCEventPotentialBlastTriggered));
            handler.Add("CEventPotentialGetRunOver", new Action<object, object, object>(this.OnCEventPotentialGetRunOverTriggered));
            handler.Add("CEventPotentialWalkIntoVehicle", new Action<object, object, object>(this.OnCEventPotentialWalkIntoVehicleTriggered));
            handler.Add("CEventProvidingCover", new Action<object, object, object>(this.OnCEventProvidingCoverTriggered));
            handler.Add("CEventRanOverPed", new Action<object, object, object>(this.OnCEventRanOverPedTriggered));
            handler.Add("CEventReactionEnemyPed", new Action<object, object, object>(this.OnCEventReactionEnemyPedTriggered));
            handler.Add("CEventReactionInvestigateDeadPed", new Action<object, object, object>(this.OnCEventReactionInvestigateDeadPedTriggered));
            handler.Add("CEventReactionInvestigateThreat", new Action<object, object, object>(this.OnCEventReactionInvestigateThreatTriggered));
            handler.Add("CEventRequestHelp", new Action<object, object, object>(this.OnCEventRequestHelpTriggered));
            handler.Add("CEventRequestHelpWithConfrontation", new Action<object, object, object>(this.OnCEventRequestHelpWithConfrontationTriggered));
            handler.Add("CEventRespondedToThreat", new Action<object, object, object>(this.OnCEventRespondedToThreatTriggered));
            handler.Add("CEventScanner", new Action<object, object, object>(this.OnCEventScannerTriggered));
            handler.Add("CEventScenarioForceAction", new Action<object, object, object>(this.OnCEventScenarioForceActionTriggered));
            handler.Add("CEventScriptCommand", new Action<object, object, object>(this.OnCEventScriptCommandTriggered));
            handler.Add("CEventScriptWithData", new Action<object, object, object>(this.OnCEventScriptWithDataTriggered));
            handler.Add("CEventShocking", new Action<object, object, object>(this.OnCEventShockingTriggered));
            handler.Add("CEventShockingBicycleCrash", new Action<object, object, object>(this.OnCEventShockingBicycleCrashTriggered));
            handler.Add("CEventShockingBicycleOnPavement", new Action<object, object, object>(this.OnCEventShockingBicycleOnPavementTriggered));
            handler.Add("CEventShockingCarAlarm", new Action<object, object, object>(this.OnCEventShockingCarAlarmTriggered));
            handler.Add("CEventShockingCarChase", new Action<object, object, object>(this.OnCEventShockingCarChaseTriggered));
            handler.Add("CEventShockingCarCrash", new Action<object, object, object>(this.OnCEventShockingCarCrashTriggered));
            handler.Add("CEventShockingCarOnCar", new Action<object, object, object>(this.OnCEventShockingCarOnCarTriggered));
            handler.Add("CEventShockingCarPileUp", new Action<object, object, object>(this.OnCEventShockingCarPileUpTriggered));
            handler.Add("CEventShockingDangerousAnimal", new Action<object, object, object>(this.OnCEventShockingDangerousAnimalTriggered));
            handler.Add("CEventShockingDeadBody", new Action<object, object, object>(this.OnCEventShockingDeadBodyTriggered));
            handler.Add("CEventShockingDrivingOnPavement", new Action<object, object, object>(this.OnCEventShockingDrivingOnPavementTriggered));
            handler.Add("CEventShockingEngineRevved", new Action<object, object, object>(this.OnCEventShockingEngineRevvedTriggered));
            handler.Add("CEventShockingExplosion", new Action<object, object, object>(this.OnCEventShockingExplosionTriggered));
            handler.Add("CEventShockingFire", new Action<object, object, object>(this.OnCEventShockingFireTriggered));
            handler.Add("CEventShockingGunFight", new Action<object, object, object>(this.OnCEventShockingGunFightTriggered));
            handler.Add("CEventShockingGunshotFired", new Action<object, object, object>(this.OnCEventShockingGunshotFiredTriggered));
            handler.Add("CEventShockingHelicopterOverhead", new Action<object, object, object>(this.OnCEventShockingHelicopterOverheadTriggered));
            handler.Add("CEventShockingHornSounded", new Action<object, object, object>(this.OnCEventShockingHornSoundedTriggered));
            handler.Add("CEventShockingInDangerousVehicle", new Action<object, object, object>(this.OnCEventShockingInDangerousVehicleTriggered));
            handler.Add("CEventShockingInjuredPed", new Action<object, object, object>(this.OnCEventShockingInjuredPedTriggered));
            handler.Add("CEventShockingMadDriver", new Action<object, object, object>(this.OnCEventShockingMadDriverTriggered));
            handler.Add("CEventShockingMadDriverBicycle", new Action<object, object, object>(this.OnCEventShockingMadDriverBicycleTriggered));
            handler.Add("CEventShockingMadDriverExtreme", new Action<object, object, object>(this.OnCEventShockingMadDriverExtremeTriggered));
            handler.Add("CEventShockingMugging", new Action<object, object, object>(this.OnCEventShockingMuggingTriggered));
            handler.Add("CEventShockingNonViolentWeaponAimedAt", new Action<object, object, object>(this.OnCEventShockingNonViolentWeaponAimedAtTriggered));
            handler.Add("CEventShockingParachuterOverhead", new Action<object, object, object>(this.OnCEventShockingParachuterOverheadTriggered));
            handler.Add("CEventShockingPedKnockedIntoByPlayer", new Action<object, object, object>(this.OnCEventShockingPedKnockedIntoByPlayerTriggered));
            handler.Add("CEventShockingPedRunOver", new Action<object, object, object>(this.OnCEventShockingPedRunOverTriggered));
            handler.Add("CEventShockingPedShot", new Action<object, object, object>(this.OnCEventShockingPedShotTriggered));
            handler.Add("CEventShockingPlaneFlyby", new Action<object, object, object>(this.OnCEventShockingPlaneFlybyTriggered));
            handler.Add("CEventShockingPotentialBlast", new Action<object, object, object>(this.OnCEventShockingPotentialBlastTriggered));
            handler.Add("CEventShockingPropertyDamage", new Action<object, object, object>(this.OnCEventShockingPropertyDamageTriggered));
            handler.Add("CEventShockingRunningPed", new Action<object, object, object>(this.OnCEventShockingRunningPedTriggered));
            handler.Add("CEventShockingRunningStampede", new Action<object, object, object>(this.OnCEventShockingRunningStampedeTriggered));
            handler.Add("CEventShockingSeenCarStolen", new Action<object, object, object>(this.OnCEventShockingSeenCarStolenTriggered));
            handler.Add("CEventShockingSeenConfrontation", new Action<object, object, object>(this.OnCEventShockingSeenConfrontationTriggered));
            handler.Add("CEventShockingSeenGangFight", new Action<object, object, object>(this.OnCEventShockingSeenGangFightTriggered));
            handler.Add("CEventShockingSeenInsult", new Action<object, object, object>(this.OnCEventShockingSeenInsultTriggered));
            handler.Add("CEventShockingSeenMeleeAction", new Action<object, object, object>(this.OnCEventShockingSeenMeleeActionTriggered));
            handler.Add("CEventShockingSeenNiceCar", new Action<object, object, object>(this.OnCEventShockingSeenNiceCarTriggered));
            handler.Add("CEventShockingSeenPedKilled", new Action<object, object, object>(this.OnCEventShockingSeenPedKilledTriggered));
            handler.Add("CEventShockingSiren", new Action<object, object, object>(this.OnCEventShockingSirenTriggered));
            handler.Add("CEventShockingStudioBomb", new Action<object, object, object>(this.OnCEventShockingStudioBombTriggered));
            handler.Add("CEventShockingVehicleTowed", new Action<object, object, object>(this.OnCEventShockingVehicleTowedTriggered));
            handler.Add("CEventShockingVisibleWeapon", new Action<object, object, object>(this.OnCEventShockingVisibleWeaponTriggered));
            handler.Add("CEventShockingWeaponThreat", new Action<object, object, object>(this.OnCEventShockingWeaponThreatTriggered));
            handler.Add("CEventShockingWeirdPed", new Action<object, object, object>(this.OnCEventShockingWeirdPedTriggered));
            handler.Add("CEventShockingWeirdPedApproaching", new Action<object, object, object>(this.OnCEventShockingWeirdPedApproachingTriggered));
            handler.Add("CEventShoutBlockingLos", new Action<object, object, object>(this.OnCEventShoutBlockingLosTriggered));
            handler.Add("CEventShoutTargetPosition", new Action<object, object, object>(this.OnCEventShoutTargetPositionTriggered));
            handler.Add("CEventShovePed", new Action<object, object, object>(this.OnCEventShovePedTriggered));
            handler.Add("CEventSoundBase", new Action<object, object, object>(this.OnCEventSoundBaseTriggered));
            handler.Add("CEventStatChangedValue", new Action<object, object, object>(this.OnCEventStatChangedValueTriggered));
            handler.Add("CEventStaticCountReachedMax", new Action<object, object, object>(this.OnCEventStaticCountReachedMaxTriggered));
            handler.Add("CEventStuckInAir", new Action<object, object, object>(this.OnCEventStuckInAirTriggered));
            handler.Add("CEventSuspiciousActivity", new Action<object, object, object>(this.OnCEventSuspiciousActivityTriggered));
            handler.Add("CEventSwitch2NM", new Action<object, object, object>(this.OnCEventSwitch2NMTriggered));
            handler.Add("CEventUnidentifiedPed", new Action<object, object, object>(this.OnCEventUnidentifiedPedTriggered));
            handler.Add("CEventVehicleCollision", new Action<object, object, object>(this.OnCEventVehicleCollisionTriggered));
            handler.Add("CEventVehicleDamage", new Action<object, object, object>(this.OnCEventVehicleDamageTriggered));
            handler.Add("CEventVehicleDamageWeapon", new Action<object, object, object>(this.OnCEventVehicleDamageWeaponTriggered));
            handler.Add("CEventVehicleOnFire", new Action<object, object, object>(this.OnCEventVehicleOnFireTriggered));
            handler.Add("CEventWrith", new Action<object, object, object>(this.OnCEventWrithTriggered));
        }

        internal void OnCEventAgitatedActionTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventAgitatedAction", number, eventId, data);
        }
        internal void OnCEventCallForCoverTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventCallForCover", number, eventId, data);
        }
        internal void OnCEventCarUndriveableTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventCarUndriveable", number, eventId, data);
        }
        internal void OnCEventClimbLadderOnRouteTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventClimbLadderOnRoute", number, eventId, data);
        }
        internal void OnCEventClimbNavMeshOnRouteTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventClimbNavMeshOnRoute", number, eventId, data);
        }
        internal void OnCEventCombatTauntTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventCombatTaunt", number, eventId, data);
        }
        internal void OnCEventCommunicateEventTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventCommunicateEvent", number, eventId, data);
        }
        internal void OnCEventCopCarBeingStolenTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventCopCarBeingStolen", number, eventId, data);
        }
        internal void OnCEventCrimeCryForHelpTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventCrimeCryForHelp", number, eventId, data);
        }
        internal void OnCEventCrimeReportedTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventCrimeReported", number, eventId, data);
        }
        internal void OnCEventDamageTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventDamage", number, eventId, data);
        }
        internal void OnCEventDataDecisionMakerTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventDataDecisionMaker", number, eventId, data);
        }
        internal void OnCEventDataFileMounterTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventDataFileMounter", number, eventId, data);
        }
        internal void OnCEventDataResponseAggressiveRubberneckTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventDataResponseAggressiveRubberneck", number, eventId, data);
        }
        internal void OnCEventDataResponseDeferToScenarioPointFlagsTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventDataResponseDeferToScenarioPointFlags", number, eventId, data);
        }
        internal void OnCEventDataResponseFriendlyAimedAtTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventDataResponseFriendlyAimedAt", number, eventId, data);
        }
        internal void OnCEventDataResponseFriendlyNearMissTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventDataResponseFriendlyNearMiss", number, eventId, data);
        }
        internal void OnCEventDataResponsePlayerDeathTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventDataResponsePlayerDeath", number, eventId, data);
        }
        internal void OnCEventDataResponsePoliceTaskWantedTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventDataResponsePoliceTaskWanted", number, eventId, data);
        }
        internal void OnCEventDataResponseSwatTaskWantedTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventDataResponseSwatTaskWanted", number, eventId, data);
        }
        internal void OnCEventDataResponseTaskTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventDataResponseTask", number, eventId, data);
        }
        internal void OnCEventDataResponseTaskAgitatedTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventDataResponseTaskAgitated", number, eventId, data);
        }
        internal void OnCEventDataResponseTaskCombatTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventDataResponseTaskCombat", number, eventId, data);
        }
        internal void OnCEventDataResponseTaskCowerTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventDataResponseTaskCower", number, eventId, data);
        }
        internal void OnCEventDataResponseTaskCrouchTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventDataResponseTaskCrouch", number, eventId, data);
        }
        internal void OnCEventDataResponseTaskDuckAndCoverTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventDataResponseTaskDuckAndCover", number, eventId, data);
        }
        internal void OnCEventDataResponseTaskEscapeBlastTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventDataResponseTaskEscapeBlast", number, eventId, data);
        }
        internal void OnCEventDataResponseTaskEvasiveStepTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventDataResponseTaskEvasiveStep", number, eventId, data);
        }
        internal void OnCEventDataResponseTaskExhaustedFleeTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventDataResponseTaskExhaustedFlee", number, eventId, data);
        }
        internal void OnCEventDataResponseTaskExplosionTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventDataResponseTaskExplosion", number, eventId, data);
        }
        internal void OnCEventDataResponseTaskFleeTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventDataResponseTaskFlee", number, eventId, data);
        }
        internal void OnCEventDataResponseTaskFlyAwayTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventDataResponseTaskFlyAway", number, eventId, data);
        }
        internal void OnCEventDataResponseTaskGrowlAndFleeTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventDataResponseTaskGrowlAndFlee", number, eventId, data);
        }
        internal void OnCEventDataResponseTaskGunAimedAtTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventDataResponseTaskGunAimedAt", number, eventId, data);
        }
        internal void OnCEventDataResponseTaskHandsUpTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventDataResponseTaskHandsUp", number, eventId, data);
        }
        internal void OnCEventDataResponseTaskHeadTrackTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventDataResponseTaskHeadTrack", number, eventId, data);
        }
        internal void OnCEventDataResponseTaskLeaveCarAndFleeTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventDataResponseTaskLeaveCarAndFlee", number, eventId, data);
        }
        internal void OnCEventDataResponseTaskScenarioFleeTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventDataResponseTaskScenarioFlee", number, eventId, data);
        }
        internal void OnCEventDataResponseTaskSharkAttackTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventDataResponseTaskSharkAttack", number, eventId, data);
        }
        internal void OnCEventDataResponseTaskShockingEventBackAwayTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventDataResponseTaskShockingEventBackAway", number, eventId, data);
        }
        internal void OnCEventDataResponseTaskShockingEventGotoTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventDataResponseTaskShockingEventGoto", number, eventId, data);
        }
        internal void OnCEventDataResponseTaskShockingEventHurryAwayTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventDataResponseTaskShockingEventHurryAway", number, eventId, data);
        }
        internal void OnCEventDataResponseTaskShockingEventReactTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventDataResponseTaskShockingEventReact", number, eventId, data);
        }
        internal void OnCEventDataResponseTaskShockingEventReactToAircraftTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventDataResponseTaskShockingEventReactToAircraft", number, eventId, data);
        }
        internal void OnCEventDataResponseTaskShockingEventStopAndStareTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventDataResponseTaskShockingEventStopAndStare", number, eventId, data);
        }
        internal void OnCEventDataResponseTaskShockingEventThreatResponseTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventDataResponseTaskShockingEventThreatResponse", number, eventId, data);
        }
        internal void OnCEventDataResponseTaskShockingEventWatchTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventDataResponseTaskShockingEventWatch", number, eventId, data);
        }
        internal void OnCEventDataResponseTaskShockingNiceCarTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventDataResponseTaskShockingNiceCar", number, eventId, data);
        }
        internal void OnCEventDataResponseTaskShockingPoliceInvestigateTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventDataResponseTaskShockingPoliceInvestigate", number, eventId, data);
        }
        internal void OnCEventDataResponseTaskThreatTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventDataResponseTaskThreat", number, eventId, data);
        }
        internal void OnCEventDataResponseTaskTurnToFaceTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventDataResponseTaskTurnToFace", number, eventId, data);
        }
        internal void OnCEventDataResponseTaskWalkAwayTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventDataResponseTaskWalkAway", number, eventId, data);
        }
        internal void OnCEventDataResponseTaskWalkRoundEntityTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventDataResponseTaskWalkRoundEntity", number, eventId, data);
        }
        internal void OnCEventDataResponseTaskWalkRoundFireTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventDataResponseTaskWalkRoundFire", number, eventId, data);
        }
        internal void OnCEventDeadPedFoundTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventDeadPedFound", number, eventId, data);
        }
        internal void OnCEventDeathTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventDeath", number, eventId, data);
        }
        internal void OnCEventDecisionMakerResponseTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventDecisionMakerResponse", number, eventId, data);
        }
        internal void OnCEventDisturbanceTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventDisturbance", number, eventId, data);
        }
        internal void OnCEventDraggedOutCarTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventDraggedOutCar", number, eventId, data);
        }
        internal void OnCEventEditableResponseTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventEditableResponse", number, eventId, data);
        }
        internal void OnCEventEncroachingPedTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventEncroachingPed", number, eventId, data);
        }
        internal void OnCEventEntityDamagedTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventEntityDamaged", number, eventId, data);
        }
        internal void OnCEventEntityDestroyedTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventEntityDestroyed", number, eventId, data);
        }
        internal void OnCEventExplosionTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventExplosion", number, eventId, data);
        }
        internal void OnCEventExplosionHeardTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventExplosionHeard", number, eventId, data);
        }
        internal void OnCEventFireNearbyTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventFireNearby", number, eventId, data);
        }
        internal void OnCEventFootStepHeardTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventFootStepHeard", number, eventId, data);
        }
        internal void OnCEventFriendlyAimedAtTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventFriendlyAimedAt", number, eventId, data);
        }
        internal void OnCEventFriendlyFireNearMissTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventFriendlyFireNearMiss", number, eventId, data);
        }
        internal void OnCEventGetOutOfWaterTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventGetOutOfWater", number, eventId, data);
        }
        internal void OnCEventGivePedTaskTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventGivePedTask", number, eventId, data);
        }
        internal void OnCEventGroupScriptAITriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventGroupScriptAI", number, eventId, data);
        }
        internal void OnCEventGroupScriptNetworkTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventGroupScriptNetwork", number, eventId, data);
        }
        internal void OnCEventGunAimedAtTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventGunAimedAt", number, eventId, data);
        }
        internal void OnCEventGunShotTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventGunShot", number, eventId, data);
        }
        internal void OnCEventGunShotBulletImpactTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventGunShotBulletImpact", number, eventId, data);
        }
        internal void OnCEventGunShotWhizzedByTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventGunShotWhizzedBy", number, eventId, data);
        }
        internal void OnCEventHelpAmbientFriendTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventHelpAmbientFriend", number, eventId, data);
        }
        internal void OnCEventHurtTransitionTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventHurtTransition", number, eventId, data);
        }
        internal void OnCEventInAirTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventInAir", number, eventId, data);
        }
        internal void OnCEventInfoTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventInfo", number, eventId, data);
        }
        internal void OnCEventInfoBaseTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventInfoBase", number, eventId, data);
        }
        internal void OnCEventInjuredCryForHelpTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventInjuredCryForHelp", number, eventId, data);
        }
        internal void OnCEventLeaderEnteredCarAsDriverTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventLeaderEnteredCarAsDriver", number, eventId, data);
        }
        internal void OnCEventLeaderExitedCarAsDriverTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventLeaderExitedCarAsDriver", number, eventId, data);
        }
        internal void OnCEventLeaderHolsteredWeaponTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventLeaderHolsteredWeapon", number, eventId, data);
        }
        internal void OnCEventLeaderLeftCoverTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventLeaderLeftCover", number, eventId, data);
        }
        internal void OnCEventLeaderUnholsteredWeaponTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventLeaderUnholsteredWeapon", number, eventId, data);
        }
        internal void OnCEventMeleeActionTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventMeleeAction", number, eventId, data);
        }
        internal void OnCEventMustLeaveBoatTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventMustLeaveBoat", number, eventId, data);
        }
        internal void OnCEventNetworkAdminInvitedTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventNetworkAdminInvited", number, eventId, data);
        }
        internal void OnCEventNetworkAttemptHostMigrationTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventNetworkAttemptHostMigration", number, eventId, data);
        }
        internal void OnCEventNetworkBailTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventNetworkBail", number, eventId, data);
        }
        internal void OnCEventNetworkCashTransactionLogTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventNetworkCashTransactionLog", number, eventId, data);
        }
        internal void OnCEventNetworkCheatTriggeredTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventNetworkCheatTriggered", number, eventId, data);
        }
        internal void OnCEventNetworkClanInviteReceivedTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventNetworkClanInviteReceived", number, eventId, data);
        }
        internal void OnCEventNetworkClanJoinedTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventNetworkClanJoined", number, eventId, data);
        }
        internal void OnCEventNetworkClanKickedTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventNetworkClanKicked", number, eventId, data);
        }
        internal void OnCEventNetworkClanLeftTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventNetworkClanLeft", number, eventId, data);
        }
        internal void OnCEventNetworkClanRankChangedTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventNetworkClanRankChanged", number, eventId, data);
        }
        internal void OnCEventNetworkCloudEventTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventNetworkCloudEvent", number, eventId, data);
        }
        internal void OnCEventNetworkCloudFileResponseTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventNetworkCloudFileResponse", number, eventId, data);
        }
        internal void OnCEventNetworkEmailReceivedEventTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventNetworkEmailReceivedEvent", number, eventId, data);
        }
        internal void OnCEventNetworkEndMatchTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventNetworkEndMatch", number, eventId, data);
        }
        internal void OnCEventNetworkEndSessionTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventNetworkEndSession", number, eventId, data);
        }
        internal void OnCEventNetworkEntityDamageTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventNetworkEntityDamage", number, eventId, data);
        }
        internal void OnCEventNetworkFindSessionTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventNetworkFindSession", number, eventId, data);
        }
        internal void OnCEventNetworkFollowInviteReceivedTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventNetworkFollowInviteReceived", number, eventId, data);
        }
        internal void OnCEventNetworkHostMigrationTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventNetworkHostMigration", number, eventId, data);
        }
        internal void OnCEventNetworkHostSessionTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventNetworkHostSession", number, eventId, data);
        }
        internal void OnCEventNetworkIncrementStatTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventNetworkIncrementStat", number, eventId, data);
        }
        internal void OnCEventNetworkInviteAcceptedTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventNetworkInviteAccepted", number, eventId, data);
        }
        internal void OnCEventNetworkInviteConfirmedTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventNetworkInviteConfirmed", number, eventId, data);
        }
        internal void OnCEventNetworkInviteRejectedTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventNetworkInviteRejected", number, eventId, data);
        }
        internal void OnCEventNetworkJoinSessionTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventNetworkJoinSession", number, eventId, data);
        }
        internal void OnCEventNetworkJoinSessionResponseTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventNetworkJoinSessionResponse", number, eventId, data);
        }
        internal void OnCEventNetworkOnlinePermissionsUpdatedTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventNetworkOnlinePermissionsUpdated", number, eventId, data);
        }
        internal void OnCEventNetworkPedLeftBehindTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventNetworkPedLeftBehind", number, eventId, data);
        }
        internal void OnCEventNetworkPickupRespawnedTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventNetworkPickupRespawned", number, eventId, data);
        }
        internal void OnCEventNetworkPlayerArrestTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventNetworkPlayerArrest", number, eventId, data);
        }
        internal void OnCEventNetworkPlayerCollectedAmbientPickupTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventNetworkPlayerCollectedAmbientPickup", number, eventId, data);
        }
        internal void OnCEventNetworkPlayerCollectedPickupTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventNetworkPlayerCollectedPickup", number, eventId, data);
        }
        internal void OnCEventNetworkPlayerCollectedPortablePickupTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventNetworkPlayerCollectedPortablePickup", number, eventId, data);
        }
        internal void OnCEventNetworkPlayerDroppedPortablePickupTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventNetworkPlayerDroppedPortablePickup", number, eventId, data);
        }
        internal void OnCEventNetworkPlayerJoinScriptTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventNetworkPlayerJoinScript", number, eventId, data);
        }
        internal void OnCEventNetworkPlayerLeftScriptTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventNetworkPlayerLeftScript", number, eventId, data);
        }
        internal void OnCEventNetworkPlayerScriptTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventNetworkPlayerScript", number, eventId, data);
        }
        internal void OnCEventNetworkPlayerSessionTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventNetworkPlayerSession", number, eventId, data);
        }
        internal void OnCEventNetworkPlayerSpawnTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventNetworkPlayerSpawn", number, eventId, data);
        }
        internal void OnCEventNetworkPresenceInviteTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventNetworkPresenceInvite", number, eventId, data);
        }
        internal void OnCEventNetworkPresenceInviteRemovedTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventNetworkPresenceInviteRemoved", number, eventId, data);
        }
        internal void OnCEventNetworkPresenceInviteReplyTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventNetworkPresenceInviteReply", number, eventId, data);
        }
        internal void OnCEventNetworkPresenceTriggerEventTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventNetworkPresenceTriggerEvent", number, eventId, data);
        }
        internal void OnCEventNetworkPresence_StatUpdateTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventNetworkPresence_StatUpdate", number, eventId, data);
        }
        internal void OnCEventNetworkPrimaryClanChangedTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventNetworkPrimaryClanChanged", number, eventId, data);
        }
        internal void OnCEventNetworkRequestDelayTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventNetworkRequestDelay", number, eventId, data);
        }
        internal void OnCEventNetworkRosChangedTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventNetworkRosChanged", number, eventId, data);
        }
        internal void OnCEventNetworkScAdminPlayerUpdatedTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventNetworkScAdminPlayerUpdated", number, eventId, data);
        }
        internal void OnCEventNetworkScAdminReceivedCashTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventNetworkScAdminReceivedCash", number, eventId, data);
        }
        internal void OnCEventNetworkScriptEventTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventNetworkScriptEvent", number, eventId, data);
        }
        internal void OnCEventNetworkSessionEventTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventNetworkSessionEvent", number, eventId, data);
        }
        internal void OnCEventNetworkShopTransactionTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventNetworkShopTransaction", number, eventId, data);
        }
        internal void OnCEventNetworkSignInStateChangedTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventNetworkSignInStateChanged", number, eventId, data);
        }
        internal void OnCEventNetworkSocialClubAccountLinkedTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventNetworkSocialClubAccountLinked", number, eventId, data);
        }
        internal void OnCEventNetworkSpectateLocalTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventNetworkSpectateLocal", number, eventId, data);
        }
        internal void OnCEventNetworkStartMatchTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventNetworkStartMatch", number, eventId, data);
        }
        internal void OnCEventNetworkStartSessionTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventNetworkStartSession", number, eventId, data);
        }
        internal void OnCEventNetworkStorePlayerLeftTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventNetworkStorePlayerLeft", number, eventId, data);
        }
        internal void OnCEventNetworkSummonTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventNetworkSummon", number, eventId, data);
        }
        internal void OnCEventNetworkSystemServiceEventTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventNetworkSystemServiceEvent", number, eventId, data);
        }
        internal void OnCEventNetworkTextMessageReceivedTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventNetworkTextMessageReceived", number, eventId, data);
        }
        internal void OnCEventNetworkTimedExplosionTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventNetworkTimedExplosion", number, eventId, data);
        }
        internal void OnCEventNetworkTransitionEventTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventNetworkTransitionEvent", number, eventId, data);
        }
        internal void OnCEventNetworkTransitionGamerInstructionTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventNetworkTransitionGamerInstruction", number, eventId, data);
        }
        internal void OnCEventNetworkTransitionMemberJoinedTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventNetworkTransitionMemberJoined", number, eventId, data);
        }
        internal void OnCEventNetworkTransitionMemberLeftTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventNetworkTransitionMemberLeft", number, eventId, data);
        }
        internal void OnCEventNetworkTransitionParameterChangedTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventNetworkTransitionParameterChanged", number, eventId, data);
        }
        internal void OnCEventNetworkTransitionStartedTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventNetworkTransitionStarted", number, eventId, data);
        }
        internal void OnCEventNetworkTransitionStringChangedTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventNetworkTransitionStringChanged", number, eventId, data);
        }
        internal void OnCEventNetworkVehicleUndrivableTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventNetworkVehicleUndrivable", number, eventId, data);
        }
        internal void OnCEventNetworkVoiceConnectionRequestedTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventNetworkVoiceConnectionRequested", number, eventId, data);
        }
        internal void OnCEventNetworkVoiceConnectionResponseTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventNetworkVoiceConnectionResponse", number, eventId, data);
        }
        internal void OnCEventNetworkVoiceConnectionTerminatedTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventNetworkVoiceConnectionTerminated", number, eventId, data);
        }
        internal void OnCEventNetworkVoiceSessionEndedTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventNetworkVoiceSessionEnded", number, eventId, data);
        }
        internal void OnCEventNetworkVoiceSessionStartedTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventNetworkVoiceSessionStarted", number, eventId, data);
        }
        internal void OnCEventNetworkWithDataTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventNetworkWithData", number, eventId, data);
        }
        internal void OnCEventNetwork_InboxMsgReceivedTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventNetwork_InboxMsgReceived", number, eventId, data);
        }
        internal void OnCEventNewTaskTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventNewTask", number, eventId, data);
        }
        internal void OnCEventObjectCollisionTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventObjectCollision", number, eventId, data);
        }
        internal void OnCEventOnFireTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventOnFire", number, eventId, data);
        }
        internal void OnCEventOpenDoorTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventOpenDoor", number, eventId, data);
        }
        internal void OnCEventPedCollisionWithPedTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventPedCollisionWithPed", number, eventId, data);
        }
        internal void OnCEventPedCollisionWithPlayerTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventPedCollisionWithPlayer", number, eventId, data);
        }
        internal void OnCEventPedEnteredMyVehicleTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventPedEnteredMyVehicle", number, eventId, data);
        }
        internal void OnCEventPedJackingMyVehicleTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventPedJackingMyVehicle", number, eventId, data);
        }
        internal void OnCEventPedOnCarRoofTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventPedOnCarRoof", number, eventId, data);
        }
        internal void OnCEventPedSeenDeadPedTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventPedSeenDeadPed", number, eventId, data);
        }
        internal void OnCEventPlayerCollisionWithPedTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventPlayerCollisionWithPed", number, eventId, data);
        }
        internal void OnCEventPlayerDeathTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventPlayerDeath", number, eventId, data);
        }
        internal void OnCEventPlayerUnableToEnterVehicleTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventPlayerUnableToEnterVehicle", number, eventId, data);
        }
        internal void OnCEventPotentialBeWalkedIntoTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventPotentialBeWalkedInto", number, eventId, data);
        }
        internal void OnCEventPotentialBlastTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventPotentialBlast", number, eventId, data);
        }
        internal void OnCEventPotentialGetRunOverTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventPotentialGetRunOver", number, eventId, data);
        }
        internal void OnCEventPotentialWalkIntoVehicleTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventPotentialWalkIntoVehicle", number, eventId, data);
        }
        internal void OnCEventProvidingCoverTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventProvidingCover", number, eventId, data);
        }
        internal void OnCEventRanOverPedTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventRanOverPed", number, eventId, data);
        }
        internal void OnCEventReactionEnemyPedTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventReactionEnemyPed", number, eventId, data);
        }
        internal void OnCEventReactionInvestigateDeadPedTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventReactionInvestigateDeadPed", number, eventId, data);
        }
        internal void OnCEventReactionInvestigateThreatTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventReactionInvestigateThreat", number, eventId, data);
        }
        internal void OnCEventRequestHelpTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventRequestHelp", number, eventId, data);
        }
        internal void OnCEventRequestHelpWithConfrontationTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventRequestHelpWithConfrontation", number, eventId, data);
        }
        internal void OnCEventRespondedToThreatTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventRespondedToThreat", number, eventId, data);
        }
        internal void OnCEventScannerTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventScanner", number, eventId, data);
        }
        internal void OnCEventScenarioForceActionTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventScenarioForceAction", number, eventId, data);
        }
        internal void OnCEventScriptCommandTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventScriptCommand", number, eventId, data);
        }
        internal void OnCEventScriptWithDataTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventScriptWithData", number, eventId, data);
        }
        internal void OnCEventShockingTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventShocking", number, eventId, data);
        }
        internal void OnCEventShockingBicycleCrashTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventShockingBicycleCrash", number, eventId, data);
        }
        internal void OnCEventShockingBicycleOnPavementTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventShockingBicycleOnPavement", number, eventId, data);
        }
        internal void OnCEventShockingCarAlarmTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventShockingCarAlarm", number, eventId, data);
        }
        internal void OnCEventShockingCarChaseTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventShockingCarChase", number, eventId, data);
        }
        internal void OnCEventShockingCarCrashTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventShockingCarCrash", number, eventId, data);
        }
        internal void OnCEventShockingCarOnCarTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventShockingCarOnCar", number, eventId, data);
        }
        internal void OnCEventShockingCarPileUpTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventShockingCarPileUp", number, eventId, data);
        }
        internal void OnCEventShockingDangerousAnimalTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventShockingDangerousAnimal", number, eventId, data);
        }
        internal void OnCEventShockingDeadBodyTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventShockingDeadBody", number, eventId, data);
        }
        internal void OnCEventShockingDrivingOnPavementTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventShockingDrivingOnPavement", number, eventId, data);
        }
        internal void OnCEventShockingEngineRevvedTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventShockingEngineRevved", number, eventId, data);
        }
        internal void OnCEventShockingExplosionTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventShockingExplosion", number, eventId, data);
        }
        internal void OnCEventShockingFireTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventShockingFire", number, eventId, data);
        }
        internal void OnCEventShockingGunFightTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventShockingGunFight", number, eventId, data);
        }
        internal void OnCEventShockingGunshotFiredTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventShockingGunshotFired", number, eventId, data);
        }
        internal void OnCEventShockingHelicopterOverheadTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventShockingHelicopterOverhead", number, eventId, data);
        }
        internal void OnCEventShockingHornSoundedTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventShockingHornSounded", number, eventId, data);
        }
        internal void OnCEventShockingInDangerousVehicleTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventShockingInDangerousVehicle", number, eventId, data);
        }
        internal void OnCEventShockingInjuredPedTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventShockingInjuredPed", number, eventId, data);
        }
        internal void OnCEventShockingMadDriverTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventShockingMadDriver", number, eventId, data);
        }
        internal void OnCEventShockingMadDriverBicycleTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventShockingMadDriverBicycle", number, eventId, data);
        }
        internal void OnCEventShockingMadDriverExtremeTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventShockingMadDriverExtreme", number, eventId, data);
        }
        internal void OnCEventShockingMuggingTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventShockingMugging", number, eventId, data);
        }
        internal void OnCEventShockingNonViolentWeaponAimedAtTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventShockingNonViolentWeaponAimedAt", number, eventId, data);
        }
        internal void OnCEventShockingParachuterOverheadTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventShockingParachuterOverhead", number, eventId, data);
        }
        internal void OnCEventShockingPedKnockedIntoByPlayerTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventShockingPedKnockedIntoByPlayer", number, eventId, data);
        }
        internal void OnCEventShockingPedRunOverTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventShockingPedRunOver", number, eventId, data);
        }
        internal void OnCEventShockingPedShotTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventShockingPedShot", number, eventId, data);
        }
        internal void OnCEventShockingPlaneFlybyTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventShockingPlaneFlyby", number, eventId, data);
        }
        internal void OnCEventShockingPotentialBlastTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventShockingPotentialBlast", number, eventId, data);
        }
        internal void OnCEventShockingPropertyDamageTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventShockingPropertyDamage", number, eventId, data);
        }
        internal void OnCEventShockingRunningPedTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventShockingRunningPed", number, eventId, data);
        }
        internal void OnCEventShockingRunningStampedeTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventShockingRunningStampede", number, eventId, data);
        }
        internal void OnCEventShockingSeenCarStolenTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventShockingSeenCarStolen", number, eventId, data);
        }
        internal void OnCEventShockingSeenConfrontationTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventShockingSeenConfrontation", number, eventId, data);
        }
        internal void OnCEventShockingSeenGangFightTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventShockingSeenGangFight", number, eventId, data);
        }
        internal void OnCEventShockingSeenInsultTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventShockingSeenInsult", number, eventId, data);
        }
        internal void OnCEventShockingSeenMeleeActionTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventShockingSeenMeleeAction", number, eventId, data);
        }
        internal void OnCEventShockingSeenNiceCarTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventShockingSeenNiceCar", number, eventId, data);
        }
        internal void OnCEventShockingSeenPedKilledTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventShockingSeenPedKilled", number, eventId, data);
        }
        internal void OnCEventShockingSirenTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventShockingSiren", number, eventId, data);
        }
        internal void OnCEventShockingStudioBombTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventShockingStudioBomb", number, eventId, data);
        }
        internal void OnCEventShockingVehicleTowedTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventShockingVehicleTowed", number, eventId, data);
        }
        internal void OnCEventShockingVisibleWeaponTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventShockingVisibleWeapon", number, eventId, data);
        }
        internal void OnCEventShockingWeaponThreatTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventShockingWeaponThreat", number, eventId, data);
        }
        internal void OnCEventShockingWeirdPedTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventShockingWeirdPed", number, eventId, data);
        }
        internal void OnCEventShockingWeirdPedApproachingTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventShockingWeirdPedApproaching", number, eventId, data);
        }
        internal void OnCEventShoutBlockingLosTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventShoutBlockingLos", number, eventId, data);
        }
        internal void OnCEventShoutTargetPositionTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventShoutTargetPosition", number, eventId, data);
        }
        internal void OnCEventShovePedTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventShovePed", number, eventId, data);
        }
        internal void OnCEventSoundBaseTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventSoundBase", number, eventId, data);
        }
        internal void OnCEventStatChangedValueTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventStatChangedValue", number, eventId, data);
        }
        internal void OnCEventStaticCountReachedMaxTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventStaticCountReachedMax", number, eventId, data);
        }
        internal void OnCEventStuckInAirTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventStuckInAir", number, eventId, data);
        }
        internal void OnCEventSuspiciousActivityTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventSuspiciousActivity", number, eventId, data);
        }
        internal void OnCEventSwitch2NMTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventSwitch2NM", number, eventId, data);
        }
        internal void OnCEventUnidentifiedPedTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventUnidentifiedPed", number, eventId, data);
        }
        internal void OnCEventVehicleCollisionTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventVehicleCollision", number, eventId, data);
        }
        internal void OnCEventVehicleDamageTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventVehicleDamage", number, eventId, data);
        }
        internal void OnCEventVehicleDamageWeaponTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventVehicleDamageWeapon", number, eventId, data);
        }
        internal void OnCEventVehicleOnFireTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventVehicleOnFire", number, eventId, data);
        }
        internal void OnCEventWrithTriggered(object number, object eventId, object data)
        {
            OutputEvent("CEventWrith", number, eventId, data);
        }

        private void OutputEvent(string v, object number, object eventId, object data)
        {
            log.Debug($"{v} {JsonConvert.SerializeObject(number)} {JsonConvert.SerializeObject(eventId)} {JsonConvert.SerializeObject(data)}");
        }
    }
}
