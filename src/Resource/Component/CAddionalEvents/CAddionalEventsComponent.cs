﻿using CitizenFX.Core;
using Proline.Resource.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Resource.Component;

namespace Resource.Component.CAddionalEvents
{
    internal class CAddionalEventsComponent : ResourceComponent, ICAddionalEventsComponent
    {
        private ResourceScript script;

        public CAddionalEventsComponent(ResourceScript script) : base(nameof(CAddionalEventsComponent), null)
        {
            this.script = script;
        }

        public override void OnLoad()
        {
            var handler = script.ResourceEvents;

            var get = new gameEventTriggered(handler);
            get.Subscribe();
            var ge2t = new CEventName(handler);
            ge2t.Subscribe();

        }

        public override void OnStart()
        {
        }
    }
}
