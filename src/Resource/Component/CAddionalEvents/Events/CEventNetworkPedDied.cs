﻿using Common.Logging;
using Proline.Resource.Framework;


namespace Resource.Component.CAddionalEvents.Events
{
    public class CEventNetworkPedDied : ResourceEvent
    {
        private static readonly ILog log = Proline.Resource.Logging.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected override void OnEventTriggered(params object[] args)
        {
            log.Debug(GetType().Name + " Invoked");
        }
    }
}
