﻿using Common.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Resource.Component.CDebugActions.Controllers
{
    internal class CDebugActionsComponentController : ICDebugActionsComponentController
    {
        protected static ILog log = Proline.Resource.Logging.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public void LogDebug(object obj, bool outputToServer = false)
        {
            try
            {
                // Log in memory
                log.Debug(obj.ToString());
                if (outputToServer)
                    WriteLine(obj.ToString());
                // Duplciate to server
            }
            catch (Exception)
            {

                throw;
            }
        }


        public void LogWarn(object obj, bool outputToServer = false)
        {
            try
            {
                // Log in memory
                log.Warn(obj.ToString());
                if (outputToServer)
                    WriteLine(obj.ToString());
                // Duplciate to server
            }
            catch (Exception)
            {

                throw;
            }
        }

        public void LogInfo(object obj, bool outputToServer = false)
        {
            try
            {
                // Log in memory
                log.Info(obj.ToString()); ;
                if (outputToServer)
                    WriteLine(obj.ToString());
                // Duplciate to server
            }
            catch (Exception)
            {

                throw;
            }
        }

        public void LogError(object obj, bool outputToServer = false)
        {
            try
            {
                // Log in memory
                log.Error(obj.ToString());
                if (outputToServer)
                    WriteLine(obj.ToString());
                // Duplciate to server
            }
            catch (Exception e)
            {

                throw;
            }
        }

        private async Task<string> WriteLine(string data)
        {
            try
            {
                //var x = new EventCaller("ConsoleWriteLineHandler");
                //x.OnEventCallback((args) =>
                //{

                //});
                //x.Invoke(data);
                //await x.WaitForCallback();
                return null;
            }
            catch (Exception e)
            {

                throw;
            }
        }
    }
}
