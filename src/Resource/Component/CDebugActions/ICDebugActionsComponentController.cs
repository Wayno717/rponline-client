﻿using System.Threading.Tasks;

namespace Resource.Component.CDebugActions
{
    internal interface ICDebugActionsComponentController 
    {
        void LogDebug(object obj, bool outputToServer = false);
        void LogError(object obj, bool outputToServer = false);
        void LogInfo(object obj, bool outputToServer = false);
        void LogWarn(object obj, bool outputToServer = false); 
    }
}