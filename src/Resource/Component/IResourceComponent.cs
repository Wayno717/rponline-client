﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Resource.Component
{
    public interface IResourceComponent
    {
        string Name { get; }
        string[] Depedencies { get; }

        void OnLoad();
        void OnStart();

        Task OnExecuteScript(string scriptName);
    }
}