﻿namespace Resource.Component.CGameLogic.Data
{
    public class PersonalWeapon
    {
        public uint Hash { get; set; }
        public int AmmoCount { get; set; }
    }
}
