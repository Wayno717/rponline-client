﻿namespace Resource.Component.CGameLogic.Data
{
    public struct PedFeature
    {
        public float Value { get; set; }
    }
}
