﻿using CitizenFX.Core;

namespace Resource.Component.CGameLogic.Data
{
    public class PersonalVehicle
    {
        public int ModelHash { get; set; }
        public Vector3 LastPosition { get; set; }
    }
}
