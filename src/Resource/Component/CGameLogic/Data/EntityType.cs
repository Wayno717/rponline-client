﻿namespace Resource.Component.CGameLogic.Data
{
    public enum EntityType
    {
        PED,
        VEHICLE,
        PROP,
        PICKUP,
    }
}