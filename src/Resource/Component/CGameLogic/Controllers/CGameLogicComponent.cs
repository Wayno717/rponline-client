﻿using Proline.Resource.Framework;
using Resource.Component.CGameLogic.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Resource.Component;
using Resource.Component.CGameLogic.Data;
using static CitizenFX.Core.Native.API;
using Proline.Resource.IO;
using Newtonsoft.Json;
using Resource.Component.CDataStream;
using Proline.OnlineEngine.Core;
using CitizenFX.Core;
using Common.Logging;

namespace Resource.Component.CGameLogic
{
    internal class CGameLogicComponentController :ICGameLogicComponentController
    {
        private ResourceScript script;
        private readonly ICDataStreamComponentController dataComponent;


        protected static ILog log = Proline.Resource.Logging.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
         
        public CGameLogicComponentController(ICDataStreamComponentController dataComponent)
        { 
            this.dataComponent = dataComponent;
        }

        public void SetPedLooks(int pedHandle, int mother, int father, float parent, float skin)
        {

            try
            {
                var looks = new CharacterLooks()
                {
                    Mother = mother,
                    Father = father,
                    Resemblence = parent,
                    SkinTone = skin,
                };
                //if (CharacterGlobals.Character != null)
                //    CharacterGlobals.Character.Looks = looks;
                SetPedHeadBlendData(pedHandle, looks.Father, looks.Mother, 0, looks.Father, looks.Mother, 0, looks.Resemblence, looks.SkinTone, 0, true);

                if (looks.Hair != null)
                    SetPedHairColor(pedHandle, looks.Hair.Color, looks.Hair.HighlightColor);
                SetPedEyeColor(pedHandle, looks.EyeColor);

                if (looks.Overlays != null)
                {
                    for (int i = 0; i < looks.Overlays.Length; i++)
                    {
                        SetPedHeadOverlay(pedHandle, i, looks.Overlays[i].Index, looks.Overlays[i].Opacity);
                        SetPedHeadOverlayColor(pedHandle, i, looks.Overlays[i].ColorType, looks.Overlays[i].PrimaryColor, looks.Overlays[i].SecondaryColor);
                    }

                }
                if (looks.Features != null)
                {
                    for (int i = 0; i < looks.Features.Length; i++)
                    {
                        SetPedFaceFeature(pedHandle, i, looks.Features[i].Value);
                    }
                }
            }
            catch (Exception e)
            {
                log.Error(e);
            }
        }


        public void SetPedOutfit(string outfitName, int handle)
        {

            try
            {
                var outfitJson = ResourceFile.ReadAll($"data/character/outfits/{outfitName}.json");
                var characterPedOutfitM = JsonConvert.DeserializeObject<CharacterOutfit>(outfitJson);
                var components = characterPedOutfitM.Components;
                for (int i = 0; i < components.Length; i++)
                {
                    var component = components[i];
                    SetPedComponentVariation(handle, i, component.ComponentIndex, component.ComponentTexture, component.ComponentPallet);
                }
            }
            catch (Exception e)
            {
                log.Error(e);
            }
        }

        public void AddValueToBankBalance(object payout)
        {
            throw new NotImplementedException();
        }

        public CharacterLooks GetPedLooks(int pedHandle)
        {

            try
            {
                //int x = 0;
                //API.GetPedHeadBlendData(pedHandle,ref x);
                //CDebugActions.EngineLogDebug(x);
                if (HasCharacter())
                    return Character.PlayerCharacter.Looks;
                else
                    return null;
            }
            catch (Exception e)
            {
                log.Error(e);
            }
            return null;
        }

        public CharacterStats GetCharacterStats()
        {
            return Character.PlayerCharacter.Stats;
        }

        public void SetCharacter(int character)
        {
            if (CharacterManager.Characters == null)
                return;
            Character.PlayerCharacter = CharacterManager.Characters[character];
        }
        public bool HasCharacter()
        {
            return Character.PlayerCharacter != null;
        }

        public void SetPedGender(int handle, char gender)
        {
            try
            {

            }
            catch (Exception e)
            {

                throw;
            }
        }

        public void SetCharacterBankBalance(long value)
        {

            try
            {
                Character.BankBalance = value;
                var bankBalanceStat = MPStat.GetStat<long>("BANK_BALANCE");
                bankBalanceStat.SetValue(Character.BankBalance);
                var id = "PlayerInfo";

                if (dataComponent.DoesDataFileExist(id))
                {
                    dataComponent.SelectDataFile(id);
                    if (dataComponent.DoesDataFileValueExist("BankBalance"))
                        dataComponent.SetDataFileValue("BankBalance", Character.BankBalance);
                    else
                        dataComponent.AddDataFileValue("BankBalance", Character.BankBalance);
                }
            }
            catch (Exception e)
            {
                log.Error(e);
            }
        }

        public long GetCharacterMaxWalletBalance()
        {
            return Character.MaxWalletCapacity;
        }
        public void SetCharacterMaxWalletBalance(int value)
        {
            Character.MaxWalletCapacity = value;
        }


        public bool HasBankBalance(long price)
        {
            return Character.BankBalance > price;
        }



        public void AddValueToBankBalance(long value)
        {

            try
            {

                Character.BankBalance += value;
                var bankBalanceStat = MPStat.GetStat<long>("BANK_BALANCE");
                bankBalanceStat.SetValue(Character.BankBalance);
                var id = "PlayerInfo";

                if (dataComponent.DoesDataFileExist(id))
                {
                    dataComponent.SelectDataFile(id);
                    if (dataComponent.DoesDataFileValueExist("BankBalance"))
                        dataComponent.SetDataFileValue("BankBalance", Character.BankBalance);
                    else
                        dataComponent.AddDataFileValue("BankBalance", Character.BankBalance);
                }
            }
            catch (Exception e)
            {
                log.Error(e);
            }
        }


        public void SubtractValueFromBankBalance(long value)
        {

            try
            {

                Character.BankBalance -= value;
                var bankBalanceStat = MPStat.GetStat<long>("BANK_BALANCE");
                bankBalanceStat.SetValue(Character.BankBalance);
                var id = "PlayerInfo";

                if (dataComponent.DoesDataFileExist(id))
                {
                    dataComponent.SelectDataFile(id);
                    if (dataComponent.DoesDataFileValueExist("BankBalance"))
                        dataComponent.SetDataFileValue("BankBalance", Character.BankBalance);
                    else
                        dataComponent.AddDataFileValue("BankBalance", Character.BankBalance);
                }
            }
            catch (Exception e)
            {
                log.Error(e);
            }
        }

        public void SetCharacterWalletBalance(long value)
        {

            try
            {

                Character.WalletBalance = value;
                var walletBalanceStat = MPStat.GetStat<long>("MP0_WALLET_BALANCE");
                walletBalanceStat.SetValue(Character.WalletBalance);
                var id = "PlayerInfo";
                if (dataComponent.DoesDataFileExist(id))
                {
                    dataComponent.SelectDataFile(id);
                    if (dataComponent.DoesDataFileValueExist("WalletBalance"))
                        dataComponent.SetDataFileValue("WalletBalance", Character.WalletBalance);
                    else
                        dataComponent.AddDataFileValue("WalletBalance", Character.WalletBalance);
                }
            }
            catch (Exception e)
            {
                log.Error(e);
            }
        }

        public void AddValueToWalletBalance(long value)
        {

            try
            {

                Character.WalletBalance += value;
                var walletBalanceStat = MPStat.GetStat<long>("MP0_WALLET_BALANCE");
                walletBalanceStat.SetValue(Character.WalletBalance);
                var id = "PlayerInfo";
                if (dataComponent.DoesDataFileExist(id))
                {
                    dataComponent.SelectDataFile(id);
                    if (dataComponent.DoesDataFileValueExist("WalletBalance"))
                        dataComponent.SetDataFileValue("WalletBalance", Character.WalletBalance);
                    else
                        dataComponent.AddDataFileValue("WalletBalance", Character.WalletBalance);
                }
            }
            catch (Exception e)
            {
                log.Error(e);
            }
        }

        public void SubtractValueFromWalletBalance(long value)
        {

            try
            {

                Character.WalletBalance -= value;
                var walletBalanceStat = MPStat.GetStat<long>("MP0_WALLET_BALANCE");
                walletBalanceStat.SetValue(Character.WalletBalance);
                var id = "PlayerInfo";

                if (dataComponent.DoesDataFileExist(id))
                {
                    dataComponent.SelectDataFile(id);
                    if (dataComponent.DoesDataFileValueExist("WalletBalance"))
                        dataComponent.SetDataFileValue("WalletBalance", Character.WalletBalance);
                    else
                        dataComponent.AddDataFileValue("WalletBalance", Character.WalletBalance);
                }
            }
            catch (Exception e)
            {
                log.Error(e);
            }
        }


        public long GetCharacterWalletBalance()
        {

            try
            {
                return Character.WalletBalance;
            }
            catch (Exception e)
            {
                log.Error(e);
            }
            return 0;
        }

        public long GetCharacterBankBalance()
        {

            try
            {

                return Character.BankBalance;

            }
            catch (Exception e)
            {
                log.Error(e);
            }
            return 0;
        }

        public bool IsInPersonalVehicle()
        {

            try
            {
                if (Character.PersonalVehicle == null)
                    return false;
                return Game.PlayerPed.IsInVehicle() && Game.PlayerPed.CurrentVehicle == Character.PersonalVehicle;
            }
            catch (Exception e)
            {
                log.Error(e);
            }
            return false;
        }
        public Entity GetPersonalVehicle()
        {
            return Character.PersonalVehicle;
        }

        public void DeletePersonalVehicle()
        {
            foreach (var item in Character.PersonalVehicle.AttachedBlips)
            {
                item.Delete();
            }
            Character.PersonalVehicle.IsPersistent = false;
            Character.PersonalVehicle.Delete();
        }


        public void SetCharacterPersonalVehicle(int handle)
        {
            Character.PersonalVehicle = new CharacterPersonalVehicle(handle);
        }

        public int CreateNewCharacter()
        {
            if (CharacterManager.Characters == null)
                CharacterManager.Characters = new List<PlayerCharacter>();
            var character = new PlayerCharacter(Game.PlayerPed.Handle)
            {
                Stats = new CharacterStats()
            };
            CharacterManager.Characters.Add(character);
            var index = CharacterManager.NextCharacterIndex;
            CharacterManager.NextCharacterIndex++;
            return index;
        }

        public void SetStatLong(string stat, long value)
        {
            if (Character.PlayerCharacter.Stats == null)
                Character.PlayerCharacter.Stats = new CharacterStats();
            Character.PlayerCharacter.Stats.SetStat(stat, value);
        }
    }
}
