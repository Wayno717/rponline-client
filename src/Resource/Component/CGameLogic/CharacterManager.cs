﻿using Resource.Component.CGameLogic.Data;
using System.Collections.Generic;

namespace Resource.Component.CGameLogic
{
    internal static class CharacterManager
    {
        internal static int NextCharacterIndex { get; set; }
        internal static List<PlayerCharacter> Characters { get; set; }
    }
}
