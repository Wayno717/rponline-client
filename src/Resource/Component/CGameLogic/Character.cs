﻿using CitizenFX.Core;
using Resource.Component.CGameLogic.Data;

namespace Resource.Component.CGameLogic
{
    internal static class Character
    {
        internal static int MaxWalletCapacity { get; set; }
        internal static Entity PersonalVehicle { get; set; }
        internal static PlayerCharacter PlayerCharacter { get; set; }
        internal static long BankBalance { get; set; }
        internal static long WalletBalance { get; set; }
    }
}
