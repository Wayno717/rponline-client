﻿using Proline.Resource.Framework;
using Resource.Component.CGameLogic.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Resource.Component;
using Resource.Component.CGameLogic.Data;
using static CitizenFX.Core.Native.API;
using Proline.Resource.IO;
using Newtonsoft.Json;
using Resource.Component.CDataStream;
using Proline.OnlineEngine.Core;
using CitizenFX.Core;

namespace Resource.Component.CGameLogic
{
    internal class CGameLogicComponent : ResourceComponent, ICGameLogicComponent
    {
        private ResourceScript script;
        private readonly ICDataStreamComponentController dataComponent;

        public CGameLogicComponent(ResourceScript script, ICDataStreamComponentController dataComponent) : base(nameof(CGameLogicComponent), null)
        {
            this.script = script;
            this.dataComponent = dataComponent;
        }

        public override void OnLoad()
        {
            script.RegisterCommand<DoOutfitThingCommand>();
            script.RegisterCommand<KillSelfCommand>();
            script.RegisterCommand<SetBankBalanceCommand>();
            script.RegisterCommand<SetPlayerPedLooksCommand>();
            script.RegisterCommand<SetWalletBalanceCommand>();
            script.RegisterCommand<SpawnMarkerCommand>();
            script.RegisterCommand<SpawnMoneyBagCommand>();
            script.RegisterCommand<SpawnVehicleCommand>();
        }

        public override void OnStart()
        {

        }

    }
}
