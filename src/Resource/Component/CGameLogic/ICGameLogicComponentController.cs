﻿using CitizenFX.Core;
using Resource.Component.CGameLogic.Data;

namespace Resource.Component.CGameLogic
{
    internal interface ICGameLogicComponentController 
    {
        void AddValueToBankBalance(long value);
        void AddValueToBankBalance(object payout);
        void AddValueToWalletBalance(long value);
        int CreateNewCharacter();
        void DeletePersonalVehicle();
        long GetCharacterBankBalance();
        long GetCharacterMaxWalletBalance();
        CharacterStats GetCharacterStats();
        long GetCharacterWalletBalance();
        CharacterLooks GetPedLooks(int pedHandle);
        Entity GetPersonalVehicle();
        bool HasBankBalance(long price);
        bool HasCharacter();
        bool IsInPersonalVehicle();
        void SetCharacter(int character);
        void SetCharacterBankBalance(long value);
        void SetCharacterMaxWalletBalance(int value);
        void SetCharacterPersonalVehicle(int handle);
        void SetCharacterWalletBalance(long value);
        void SetPedGender(int handle, char gender);
        void SetPedLooks(int pedHandle, int mother, int father, float parent, float skin);
        void SetPedOutfit(string outfitName, int handle);
        void SetStatLong(string stat, long value);
        void SubtractValueFromBankBalance(long value);
        void SubtractValueFromWalletBalance(long value);
    }
}