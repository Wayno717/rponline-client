﻿using Proline.Resource.Framework;

namespace Resource.Component.CGameLogic.Commands
{
    public class SetWalletBalanceCommand : ResourceCommand
    {
        public SetWalletBalanceCommand()
        {
        }

        protected override void OnCommandExecute(params object[] args)
        {
            if (args.Length > 0)
            {
                long.TryParse(args[0].ToString(), out var value);
                ResourceAPI.SetCharacterWalletBalance(value);
            }

        }
    }
}
