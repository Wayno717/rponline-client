﻿using CitizenFX.Core;
using Common.Logging;
using Proline.Resource.Framework;
using System;

namespace Resource.Component.CGameLogic.Commands
{
    public class SpawnMarkerCommand : ResourceCommand
    {
        private static readonly ILog log = Proline.Resource.Logging.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public SpawnMarkerCommand()
        {
        }

        protected override void OnCommandExecute(params object[] args)
        {
            if (args.Length > 0)
            {
                try
                {
                    VehicleHash randomBar;
                    if (Enum.TryParse(args[0].ToString(), true, out randomBar))
                    {
                        World.CreateVehicle(new Model(randomBar), World.GetNextPositionOnStreet(Game.PlayerPed.Position));
                    };
                }
                catch (Exception)
                {
                    log.Debug("Vehicle could not be found");
                }
            }
        }
    }
}
