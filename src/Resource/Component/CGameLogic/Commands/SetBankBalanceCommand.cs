﻿using Proline.Resource.Framework;

namespace Resource.Component.CGameLogic.Commands
{
    public class SetBankBalanceCommand : ResourceCommand
    {
        public SetBankBalanceCommand()
        {
        }

        protected override void OnCommandExecute(params object[] args)
        {
            if (args.Length > 0)
            {
                long.TryParse(args[0].ToString(), out var value);
                ResourceAPI.SetCharacterBankBalance(value);
            }

        }
    }
}
