﻿using CitizenFX.Core;
using Proline.Resource.Framework;

namespace Resource.Component.CGameLogic.Commands
{
    public class KillSelfCommand : ResourceCommand
    {

        protected override void OnCommandExecute(params object[] args)
        {
            Game.PlayerPed.Kill();
        }
    }
}
