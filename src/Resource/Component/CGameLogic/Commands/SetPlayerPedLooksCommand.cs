﻿using CitizenFX.Core;


using Proline.Resource.Framework;

namespace Resource.Component.CGameLogic.Commands
{
    public class SetPlayerPedLooksCommand : ResourceCommand
    {
        public SetPlayerPedLooksCommand()
        {
        }

        protected override void OnCommandExecute(params object[] args)
        {
            if (args.Length == 3)
            {
                var Father = int.Parse(args[0].ToString());
                var Mother = int.Parse(args[1].ToString());
                var Resemblence = float.Parse(args[2].ToString());
                ResourceAPI.SetPedLooks(Game.PlayerPed.Handle, Mother, Father, Resemblence, 0f);
            }

        }
    }
}
