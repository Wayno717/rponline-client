﻿using CitizenFX.Core;
using Newtonsoft.Json;
using Proline.Resource.IO;
using Resource.Component;
using Resource.Component.CDataStream;
using Resource.Component.CDataStream.Data;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Resource
{
    public static partial class ResourceAPI
    {

        public static void CreateDataFile()
        {
            GetComponent<ICDataStreamComponentController>().CreateDataFile();
        }

        public static string GetSelectedDataFile()
        {
            return GetComponent<ICDataStreamComponentController>().GetSelectedDataFile(); 
        }

        public static bool DoesDataFileExist(string id)
        {
            return GetComponent<ICDataStreamComponentController>().DoesDataFileExist(id); 
        }

        public static void SaveDataFile(string identifier)
        {
            GetComponent<ICDataStreamComponentController>().SaveDataFile(identifier);
        }
        public static void SelectDataFile(string identifier)
        {
            GetComponent<ICDataStreamComponentController>().SelectDataFile(identifier);

        }
        public static bool DoesDataFileValueExist(string key)
        {
            return GetComponent<ICDataStreamComponentController>()
             .DoesDataFileExist(key);
        }

        public static void AddDataFileValue(string key, object value)
        {
             GetComponent<ICDataStreamComponentController>() 
            .AddDataFileValue(key, value);
        }

        public static void SetDataFileValue(string key, object value)
        { 
             GetComponent<ICDataStreamComponentController>()
            .SetDataFileValue(key, value);
        }
        public static object GetDataFileValue(string key)
        {
            return GetComponent<ICDataStreamComponentController>()
             .GetDataFileValue(key);

        }
        public static T GetDataFileValue<T>(string key)
        {
            return GetComponent<ICDataStreamComponentController>()
             .GetDataFileValue<T>(key);

        }


        public static int GetNumOfAudioSamples()
        {
            return GetComponent<ICDataStreamComponentController>()
             .GetNumOfAudioSamples();
        }

        public static async Task SendSaveToCloud()
        {
            await GetComponent<ICDataStreamComponentController>()
             .SendSaveToCloud();
        }
        public static bool IsSaveInProgress()
        {
            return GetComponent<ICDataStreamComponentController>()
             .IsSaveInProgress();
        }

        public static int? GetSaveState()
        {
            return GetComponent<ICDataStreamComponentController>()
             .GetSaveState();
        }

        public static async Task PullSaveFromCloud()
        {
            await GetComponent<ICDataStreamComponentController>()
             .PullSaveFromCloud();
        }

        public static bool HasSaveLoaded()
        {
            return GetComponent<ICDataStreamComponentController>()
             .HasSaveLoaded();
        }

        public static void GetAudioSamplesAtIndex(int id, out string audioId, out string audioRef, out bool enabled)
        {
             GetComponent<ICDataStreamComponentController>()
            .GetAudioSamplesAtIndex(id, out audioId, out audioRef, out enabled);

        }

    }
}
