﻿using CitizenFX.Core;
using CitizenFX.Core.Native;
using Newtonsoft.Json;
using Proline.OnlineEngine.Core;
using Proline.Resource.IO;
using Resource.Component.CGameLogic;
using Resource.Component.CGameLogic.Data;
using System;
using System.Collections.Generic;
using static CitizenFX.Core.Native.API;

namespace Resource
{

    public static partial class ResourceAPI
    {

        public static void SetPedLooks(int pedHandle, int mother, int father, float parent, float skin)
        {
            GetComponent<ICGameLogicComponentController>().SetPedLooks(pedHandle, mother, father, parent, skin);
        }

        public static void SetPedOutfit(string outfitName, int handle)
        {
            GetComponent<ICGameLogicComponentController>().SetPedOutfit(outfitName, handle);
        }

        public static void AddValueToBankBalance(object payout)
        {
            GetComponent<ICGameLogicComponentController>().AddValueToBankBalance(payout);
        }

        public static CharacterLooks GetPedLooks(int pedHandle)
        {
            return GetComponent<ICGameLogicComponentController>().GetPedLooks(pedHandle);
        }

        public static CharacterStats GetCharacterStats()
        {
            return GetComponent<ICGameLogicComponentController>().GetCharacterStats();
        }

        public static void SetCharacter(int character)
        {
            GetComponent<ICGameLogicComponentController>().SetCharacter(character);
        }
        public static bool HasCharacter()
        {
            return GetComponent<ICGameLogicComponentController>().HasCharacter();
        }

        public static void SetPedGender(int handle, char gender)
        {
            GetComponent<ICGameLogicComponentController>().SetPedGender(handle, gender);
        }

        public static void SetCharacterBankBalance(long value)
        {
            GetComponent<ICGameLogicComponentController>().SetCharacterBankBalance(value);
        }

        public static long GetCharacterMaxWalletBalance()
        {
            return GetComponent<ICGameLogicComponentController>().GetCharacterMaxWalletBalance();
        }
        public static void SetCharacterMaxWalletBalance(int value)
        {
             GetComponent<ICGameLogicComponentController>().SetCharacterMaxWalletBalance(value);
        }

        public static bool HasBankBalance(long price)
        {
            return GetComponent<ICGameLogicComponentController>().HasBankBalance(price);
        }

        public static void AddValueToBankBalance(long value)
        {
             GetComponent<ICGameLogicComponentController>().AddValueToBankBalance(value);
        }


        public static void SubtractValueFromBankBalance(long value)
        {
             GetComponent<ICGameLogicComponentController>().SubtractValueFromBankBalance(value);
        }

        public static void SetCharacterWalletBalance(long value)
        {
            GetComponent<ICGameLogicComponentController>().SetCharacterWalletBalance(value);
        }

        public static void AddValueToWalletBalance(long value)
        {
            GetComponent<ICGameLogicComponentController>().AddValueToWalletBalance(value);
        }

        public static void SubtractValueFromWalletBalance(long value)
        {
            GetComponent<ICGameLogicComponentController>().SubtractValueFromWalletBalance(value);
        }


        public static long GetCharacterWalletBalance()
        {
            return GetComponent<ICGameLogicComponentController>().GetCharacterWalletBalance();
        }

        public static long GetCharacterBankBalance()
        {
            return GetComponent<ICGameLogicComponentController>().GetCharacterBankBalance();
        }

        public static bool IsInPersonalVehicle()
        {
            return GetComponent<ICGameLogicComponentController>().IsInPersonalVehicle();
        }
        public static Entity GetPersonalVehicle()
        {
            return GetComponent<ICGameLogicComponentController>().GetPersonalVehicle();
        }

        public static void DeletePersonalVehicle()
        {
            GetComponent<ICGameLogicComponentController>().DeletePersonalVehicle();
        } 

        public static void SetCharacterPersonalVehicle(int handle)
        {
            GetComponent<ICGameLogicComponentController>().SetCharacterPersonalVehicle(handle);
        }

        public static int CreateNewCharacter()
        {
            return GetComponent<ICGameLogicComponentController>().CreateNewCharacter();
        }

        public static void SetStatLong(string stat, long value)
        {
            GetComponent<ICGameLogicComponentController>().SetStatLong(stat, value);
        }
    }
}
