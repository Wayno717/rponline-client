﻿using CitizenFX.Core;
using CitizenFX.Core.Native;
using Resource.Component.CGameRendering;
using System;
using System.Drawing;

namespace Resource
{

    public static partial class ResourceAPI
    {
        public static void DrawDebugText3D(string text, Vector3 vector3, float scale2, int font)
        { 
            GetComponent<ICGameRenderingComponentController>().DrawDebugText3D(text, vector3, scale2, font);
        }

        public static void DrawEntityBoundingBox(int ent, int r, int g, int b, int a)
        { 
            GetComponent<ICGameRenderingComponentController>().DrawEntityBoundingBox(ent, r, g, b, a);
        }


        public static void DrawBoundingBox(Vector3 start, Vector3 end, int r, int g, int b, int a)
        {
            GetComponent<ICGameRenderingComponentController>().DrawBoundingBox(start, end, r,g, b, a); 
        }


        public static void DrawBoundingBoxFromPoints(Vector3[] points, int r, int g, int b, int a)
        { 
            GetComponent<ICGameRenderingComponentController>().DrawBoundingBoxFromPoints(points, r, g, b, a);
        }


        public static void DrawBoundingPlaneFromPoints(Vector3[] points, int r, int g, int b, int a)
        { 
            GetComponent<ICGameRenderingComponentController>().DrawBoundingPlaneFromPoints(points, r, g, b, a);
        }

        public static Vector3[] GetBoundingBox(Vector3 start, Vector3 end, float heading = 0f)
        { 
            return GetComponent<ICGameRenderingComponentController>().GetBoundingBox(start, end, heading);
        }

        public static Vector3 ConvertWorldToLocal(Vector3 origin, float originRotation, Vector3 Worldposition)
        {
            return GetComponent<ICGameRenderingComponentController>().ConvertWorldToLocal(origin, originRotation, Worldposition); 
        }

        public static Vector3 ConvertLocalToWorld(Vector3 origin, float originRotation, Vector3 localPosition)
        {
            return GetComponent<ICGameRenderingComponentController>().ConvertLocalToWorld(origin, originRotation, localPosition); 
        }


        public static Vector3[] GetEntityBoundingBox(int entity)
        {
            return GetComponent<ICGameRenderingComponentController>().GetEntityBoundingBox(entity); 
        }

    }
}
