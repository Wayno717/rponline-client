﻿using CitizenFX.Core;
using Resource.Component.CMissionManager;
using System;

namespace Resource
{
    public static partial class ResourceAPI
    {
        public static bool BeginMission()
        {
            return GetComponent<ICMissionManagerComponentController>().BeginMission(); 
        }

        public static void EndMission()
        {
            GetComponent<ICMissionManagerComponentController>().EndMission(); 
        }
        public static bool GetMissionFlag()
        {
            return GetComponent<ICMissionManagerComponentController>().GetMissionFlag(); 
        }
        public static bool IsInMissionVehicle()
        {
            return GetComponent<ICMissionManagerComponentController>().IsInMissionVehicle(); 
        }
        public static void SetMissionFlag(bool enable)
        {
            GetComponent<ICMissionManagerComponentController>().SetMissionFlag(enable); 
        }
        public static void TrackPoolObjectForMission(PoolObject obj)
        {
            GetComponent<ICMissionManagerComponentController>().TrackPoolObjectForMission(obj); 
        }
    }
}
