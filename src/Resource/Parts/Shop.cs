﻿using Resource.Component.CShopCatalogue;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Resource
{
    public static partial class ResourceAPI
    {
        public static async Task<string> GetVehicleNames()
        {
            return await GetComponent<ICShopCatalogueComponentController>().GetVehicleNames();
        }

        public static void PutVehicleIn(string name)
        {
            GetComponent<ICShopCatalogueComponentController>().PutVehicle(name);
        }

        public static void DeleteVehicle(string name)
        {
            GetComponent<ICShopCatalogueComponentController>().DeleteVehicle(name); 
        }

        public static void BuyVehicle(string vehicleName)
        { 
            GetComponent<ICShopCatalogueComponentController>().BuyVehicle(vehicleName);
        }
    }
}
