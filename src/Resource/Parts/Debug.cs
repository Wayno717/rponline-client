﻿using Common.Logging;
using Resource.Component.CDebugActions;
using System;
using System.Threading.Tasks;

namespace Resource
{
    public static partial class ResourceAPI
    {
        public static void LogDebug(object obj, bool outputToServer = false)
        {
            GetComponent<ICDebugActionsComponentController>().LogDebug(obj, outputToServer);
        }


        public static void LogWarn(object obj, bool outputToServer = false)
        { 
            GetComponent<ICDebugActionsComponentController>().LogWarn(obj, outputToServer);
        }

        public static void LogInfo(object obj, bool outputToServer = false)
        { 
            GetComponent<ICDebugActionsComponentController>().LogInfo(obj, outputToServer);
        }

        public static void LogError(object obj, bool outputToServer = false)
        { 
            GetComponent<ICDebugActionsComponentController>().LogError(obj, outputToServer);
        }


    }
}
