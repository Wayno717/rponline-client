﻿using CitizenFX.Core;
using CitizenFX.Core.Native;
using Resource.Component.CGameLogic.Data;
using Resource.Component.CPoolObjects;
using System;
using System.Collections.Generic;

namespace Resource
{

    public static partial class ResourceAPI
    {

        public static int[] GetEntityHandlesByTypes(int type)
        { 
            return GetComponent<ICPoolObjectsComponentController>().GetEntityHandlesByTypes(type);

        }

        public static Entity GetNeariestEntity(int type)
        { 
            return GetComponent<ICPoolObjectsComponentController>().GetNeariestEntity(type);
        }


        public static int[] GetAllExistingPoolObjects()
        { 
            return GetComponent<ICPoolObjectsComponentController>().GetAllExistingPoolObjects();
        }
    }
}
