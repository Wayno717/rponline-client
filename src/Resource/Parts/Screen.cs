﻿using CitizenFX.Core;
using CitizenFX.Core.Native;
using Resource.Component.CScreenRendering;
using System;
using System.Drawing;
using System.Threading.Tasks;

namespace Resource
{
    public static partial class ResourceAPI
    {
        public static void Draw2DBox(float x, float y, float width, float heigth, Color color)
        {
            GetComponent<ICScreenRenderingComponentController>().Draw2DBox(x,y,width,heigth,color); 
        }

        public static Vector3 ScreenRelToWorld(Vector3 camPos, Vector3 camRot, Vector2 coord, out Vector3 forwardDirection)
        { 
            return GetComponent<ICScreenRenderingComponentController>().ScreenRelToWorld(camPos, camRot, coord, out forwardDirection);
        }


        public static void DrawDebug2DBox(PointF start, PointF end, Color color)
        { 
            GetComponent<ICScreenRenderingComponentController>().DrawDebug2DBox(start, end, color);
        }

        public static void DrawDebugText2D(string text, PointF vector2, float scale, int font)
        { 
            GetComponent<ICScreenRenderingComponentController>().DrawDebugText2D(text, vector2, scale, font);
        }

        public static async Task FlashBlip(Blip blip, int duration = 100)
        { 
            await GetComponent<ICScreenRenderingComponentController>().FlashBlip(blip, duration);
        }

        public static async Task FlashBlip(int blipHandle, int duration = 100)
        { 
            await GetComponent<ICScreenRenderingComponentController>().FlashBlip(blipHandle, duration);
        }
    }
}
