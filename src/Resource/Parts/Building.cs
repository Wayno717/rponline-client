﻿using CitizenFX.Core;
using CitizenFX.Core.Native;
using Newtonsoft.Json;
using Proline.Resource.IO;
using Resource.Component.CWorldObjects;
using Resource.Component.CWorldObjects.Data;
using Resource.Component.CWorldObjects.Data.Ownership;
using System;
using System.Linq;

namespace Resource
{
    public static partial class ResourceAPI
    {
        public static string GetNearestBuilding()
        {
            return GetComponent<ICWorldObjectsComponentController>().GetNearestBuilding();
        }

        public static Vector3 GetBuildingPosition(string buildingId)
        { 
            return GetComponent<ICWorldObjectsComponentController>().GetBuildingPosition(buildingId);
        }

        public static Vector3 GetBuildingEntrance(string buildingId, int entranceId = 0)
        { 
            return GetComponent<ICWorldObjectsComponentController>().GetBuildingEntrance(buildingId, entranceId);
        }

        public static int GetNumOfBuldingEntrances(string buildingId)
        { 
            return GetComponent<ICWorldObjectsComponentController>().GetNumOfBuldingEntrances(buildingId);
        }


        public static string GetBuildingEntranceString(string buildingId, int entranceId = 0)
        { 
            return GetComponent<ICWorldObjectsComponentController>().GetBuildingEntranceString(buildingId, entranceId);
        }


        public static string GetNearestBuildingEntrance(string building)
        {
            return GetComponent<ICWorldObjectsComponentController>().GetNearestBuildingEntrance(building); 
        }
        public static Vector3 GetBuildingWorldPos(string buildingId)
        { 
            return GetComponent<ICWorldObjectsComponentController>().GetBuildingWorldPos(buildingId);
        }

        public static string EnterBuilding(string buildingId, string buildingEntrance)
        { 
            return GetComponent<ICWorldObjectsComponentController>().EnterBuilding(buildingId, buildingEntrance);
        }
        public static Vector3 ExitBuilding(string buildingId, string accessPoint, int type = 0)
        { 
            return GetComponent<ICWorldObjectsComponentController>().ExitBuilding(buildingId, accessPoint, type);
        }

        public static void PlaceVehicleInGarageSlot(string propertyId, int index, Entity vehicle)
        {
            GetComponent<ICWorldObjectsComponentController>().PlaceVehicleInGarageSlot(propertyId, index, vehicle); 
        }

        public static int GetPropertyGarageLimit(string propertyId)
        {
            return GetComponent<ICWorldObjectsComponentController>().GetPropertyGarageLimit(propertyId); 
        }

        public static Vector3 GetBuildingWorldPos(object neariestBulding)
        {
            return GetComponent<ICWorldObjectsComponentController>().GetBuildingWorldPos(neariestBulding); 
        }


        public static Vector3 EnterInterior(string interiorId, string entranceId)
        { 
            return GetComponent<ICWorldObjectsComponentController>().EnterInterior(interiorId, entranceId);
        }

        public static string ExitInterior(string interiorId, string exitId)
        { 
            return GetComponent<ICWorldObjectsComponentController>().ExitInterior(interiorId, exitId);
        }


        public static string GetNearestInterior()
        {
            return GetComponent<ICWorldObjectsComponentController>().GetNearestInterior(); 
        }

        public static Vector3 GetBuildingInterior(string buildingId)
        { 
            return GetComponent<ICWorldObjectsComponentController>().GetBuildingInterior(buildingId);
        }
        public static string GetNearestInteriorExit(string interiorId = null)
        {
            return GetComponent<ICWorldObjectsComponentController>().GetNearestInteriorExit(interiorId); 
        }

        public static int CreateMarker(Vector3 position, float activationRange = 2f)
        {
            return GetComponent<ICWorldObjectsComponentController>().CreateMarker(position, activationRange); 
        }

        public static void DrawMarker(int handle)
        { 
             GetComponent<ICWorldObjectsComponentController>().DrawMarker(handle);
        }

        public static bool IsInMarker(int handle, int obj)
        {
            return GetComponent<ICWorldObjectsComponentController>().IsInMarker(handle, obj); 
        }

        public static void DeleteMarker(int handle)
        {
             GetComponent<ICWorldObjectsComponentController>().DeleteMarker(handle); 
        }
         
        public static Vector3 EnterProperty(string propertyId, string propertyPart, string entranceId)
        {
            return GetComponent<ICWorldObjectsComponentController>().EnterProperty(propertyId, propertyPart, entranceId); 
        } 

        public static string GetPropertyExit(string propertyPart, string functionType, string neariestExit)
        { 
            return GetComponent<ICWorldObjectsComponentController>().GetPropertyExit(propertyPart, functionType, neariestExit);
        }

        public static string GetPropertyEntry(string propertyPart, string functionType, string entranceString)
        { 
            return GetComponent<ICWorldObjectsComponentController>().GetPropertyEntry(propertyPart, functionType, entranceString); 
        }

        public static string GetPropertyApartment(string propertyId)
        { 
            return GetComponent<ICWorldObjectsComponentController>().GetPropertyApartment(propertyId); 
        }

        public static string GetPropertyGarage(string propertyId)
        {
            return GetComponent<ICWorldObjectsComponentController>().GetPropertyGarage(propertyId); 
        }

        public static string GetPropertyGarageLayout(string propertyId)
        { 
            return GetComponent<ICWorldObjectsComponentController>().GetPropertyGarageLayout(propertyId); 
        }

        public static string GetPropertyInterior(string propertyId, string propertyType)
        { 
            return GetComponent<ICWorldObjectsComponentController>().GetPropertyInterior(propertyId, propertyType);
        }


        public static string GetPropertyPartType(string partName)
        {
            return GetComponent<ICWorldObjectsComponentController>().GetPropertyPartType(partName); 
        }

        public static string ExitProperty(string propertyId, string propertyPart, string exitId)
        { 
            return GetComponent<ICWorldObjectsComponentController>().ExitProperty(propertyId, propertyPart, exitId);
        }

        public static string GetPropertyBuilding(string propertyId)
        { 
            return GetComponent<ICWorldObjectsComponentController>().GetPropertyBuilding(propertyId);
        }

        public static string GetInteriorExitString(string interiorId, int exitId = 0)
        { 
            return GetComponent<ICWorldObjectsComponentController>().GetInteriorExitString(interiorId, exitId);
        }

        public static Vector3 GetInteriorExit(string interiorId, int exitId = 0)
        {
            return GetComponent<ICWorldObjectsComponentController>().GetInteriorExit(interiorId, exitId); 
        }

        public static int GetNumOfInteriorExits(string interiorId)
        { 
            return GetComponent<ICWorldObjectsComponentController>().GetNumOfInteriorExits(interiorId);
        }
    }
}
