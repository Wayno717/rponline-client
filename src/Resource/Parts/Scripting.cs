﻿using CitizenFX.Core;
using Proline.Resource.Framework;
using Resource.Component.CCoreSystem;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Resource
{
    public static partial class ResourceAPI
    {
        public static void TriggerScriptEvent(string eventName, params object[] args)
        {
            var component = GetComponent<ICCoreSystemComponentController>();
            component.TriggerScriptEvent(eventName, args);
        }

        public static bool GetEventExists(object scriptInstance, string eventName)
        {
            var component = GetComponent<ICCoreSystemComponentController>();
            return component.GetEventExists(scriptInstance, eventName);
        }

        public static object[] GetEventData(object scriptInstance, string eventName)
        {
            var component = GetComponent<ICCoreSystemComponentController>();
            return component.GetEventData(scriptInstance, eventName);
        }

        public static int StartNewScript(string scriptName, params object[] args)
        {
            var component = GetComponent<ICCoreSystemComponentController>();
            return component.StartNewScript(scriptName, args);
        }


        public static int GetInstanceCountOfScript(string scriptName)
        {
            var component = GetComponent<ICCoreSystemComponentController>();
            return component.GetInstanceCountOfScript(scriptName);
        }

        public static void MarkScriptAsNoLongerNeeded(object callingClass)
        {
            var component = GetComponent<ICCoreSystemComponentController>();
            component.MarkScriptAsNoLongerNeeded(callingClass);
        }

        public static void MarkScriptAsNoLongerNeeded(string scriptName)
        {
            var component = GetComponent<ICCoreSystemComponentController>();
            component.MarkScriptAsNoLongerNeeded(scriptName);
        }

        /// <summary>
        /// Terminates all script instances with the passed scriptName
        /// </summary>
        /// <param name="scriptName"></param>
        public static void TerminateScript(string scriptName)
        {
            var component = GetComponent<ICCoreSystemComponentController>();
            component.TerminateScript(scriptName);
        }

        /// <summary>
        /// Terminates the passed script object task instance
        /// </summary>
        /// <param name="scriptInstance"></param>
        public static void TerminateScriptInstance(object scriptInstance)
        {
            var component = GetComponent<ICCoreSystemComponentController>();
            component.TerminateScriptInstance(scriptInstance);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="taskId"></param>
        public static void TerminateScriptTask(int taskId)
        {
            var component = GetComponent<ICCoreSystemComponentController>();
            component.TerminateScriptTask(taskId);
        }

        /// <summary>
        /// Waits a specific amount using the BaseScript.Delay function, 0 ms waits till the next frame is rendered
        /// </summary>
        /// <param name="ms"></param>
        /// <returns></returns>
        public static async Task Delay(int ms)
        {
            var component = GetComponent<ICCoreSystemComponentController>();
            await component.Delay(ms);
        }
         
    }
}
