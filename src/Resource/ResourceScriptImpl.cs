﻿using Autofac; 
using Proline.Resource.Framework; 
using Resource.Commands;
using Resource.Component;
using Resource.Component.CDebugActions;
using Resource.Component.CMissionManager;
using Resource.Component.CGameRendering;
using Resource.Component.CScreenRendering;
using Resource.Component.CAddionalEvents;
using Resource.Component.CCoreSystem;
using Resource.Component.CDataStream;
using Resource.Component.CGameLogic;
using Resource.Component.CPoolObjects;
using Resource.Component.CScriptAreas;
using Resource.Component.CScriptObjs;
using Resource.Component.CShopCatalogue;
using Resource.Component.CWorldObjects;  
using System;
using System.Collections.Generic;
using System.Linq; 
using System.Threading.Tasks;
using CitizenFX.Core.Native;
using CitizenFX.Core.UI;
using CitizenFX.Core;
using System.Reflection;
using Newtonsoft.Json;

namespace Resource
{
    public class ResourceScriptImpl : ResourceScript
    {
        private ResourceScriptImpl _root;
        private IContainer _container;
        private ILifetimeScope _scope;
        private Dictionary<string, IResourceComponent> _componentDictionary;

        public static ResourceScriptImpl Instance; 

        public static T GetComponent<T>()
        {
            return Instance._scope.Resolve<T>();
        }

        public static object GetComponent(Type type)
        {
            return Instance._scope.Resolve(type);
        }


        public override async Task Execute()
        {
            //var manifestJson = ResourceFile.ReadAll("data/manifest.json");
            //var dataFilePaths = JsonConvert.DeserializeObject<Dictionary<string,object>>(manifestJson);

            Instance = this;
            Screen.Fading.FadeOut(0);


#if DEBUG
            RegisterCommand<SetLogLevelCommand>();
#endif 
            RegisterAPI(nameof(ResourceAPI.StartNewScript), new Func<string, object[], int>(ResourceAPI.StartNewScript));



            // Initializing the game
            var builder = new ContainerBuilder();
            builder.RegisterInstance(this).As<ResourceScript>(); 

            var types = Assembly.GetExecutingAssembly()
                .GetTypes()
                .Where(e => e.Name.EndsWith("Component") && e.Name.StartsWith("IC") && e.IsInterface)
                .ToArray();
             

            var assembly = Assembly.GetExecutingAssembly();

            var componentsToRegister = assembly.GetTypes().Where(e=> e.Name.EndsWith("Component") && e.Name.StartsWith("C") && e.BaseType == typeof(ResourceComponent));

            foreach (var item in componentsToRegister)
            {
                log.Debug(item.Name);
                log.Debug(item.Namespace);

                builder.RegisterType(item).AsImplementedInterfaces().SingleInstance();
                 
                try
                {
                    var controllersThatExistInNamespaceComponent = assembly.GetTypes().Where(e => e.Namespace.Contains(item.Namespace));

                    foreach (var item2 in controllersThatExistInNamespaceComponent)
                    {
                        if(item2.Name.EndsWith("Controller") && item2.Name.StartsWith("C"))
                        {
                            log.Debug(item2);
                            builder.RegisterType(item2).AsImplementedInterfaces();
                        }
                    } 
                }
                catch (Exception e)
                {
                    // THIS THROWS A VERY ODD ERROR THAT APPARENTLY GETS IGNORED??? .Where linq throws it above then continues the code
                }  
            } 

            _container = builder.Build();
            log.Info("Container built");
            _scope = _container.BeginLifetimeScope(); 


            var wow = GetComponent<ICDataStreamComponentController>();
            //wow.OnLoad();

            //using (var scope = _container.BeginLifetimeScope())
            {  
                // OnLoad - PreScript stage execution
                // OnStart - PostScript stage execution
                 
                var stage = 0;
                while(stage < 4)
                {
                    double percent = (((stage + 1) / 4.0)) * 100.00;
                    Screen.LoadingPrompt.Show($"Loading Resource {percent}%");
                    await BaseScript.Delay(1000);  
                    switch (stage)
                    {
                        case 0:
                            log.Info($"Loading {types.Length} Components");
                            for (int i = 0; i < types.Length; i++)
                            {
                                try
                                { 
                                    var item = types[i]; 
                                    log.Info($"Loading {item.Name} {i + 1}/{types.Length}");
                                    var component = (IResourceComponent)GetComponent(item);
                                    component.OnLoad();
                                    _componentDictionary.Add(component.Name, component);
                                }
                                catch (Exception e)
                                {

                                }
                            }
                            break;
                        case 1:
                            log.Debug("Initiating core scripts from components");
                            await ExecuteScript(types, "InitCore");
                            break;
                        case 2:
                            log.Debug("Initiating session scripts from components");
                            await ExecuteScript(types, "InitSession");
                            break;

                        case 3:
                            log.Info($"Starting {types.Length} Components");
                            for (int i = 0; i < types.Length; i++)
                            {
                                try
                                {
                                    var item = types[i];
                                    log.Info($"Starting {item.Name} {i + 1}/{types.Length}");
                                    var component = (IResourceComponent)GetComponent(item);
                                    component.OnStart();
                                }
                                catch (Exception e)
                                {

                                }
                            }
                            break;
                        case 5:
                            //var x = new FakeSocket(EventHandlers);
                            //x.Bind(API.GetCurrentResourceName());
                            //await x.Connect(null, "s-rponline");
                            //await Delay(1000);
                            //x.Send("Random");
                        break;
                    }
                    stage++;
                } 
            }  
        }

        private async Task ExecuteScript(Type[] components, string scriptName)
        {
            if (string.IsNullOrEmpty(scriptName))
            {
                throw new ArgumentException($"'{nameof(scriptName)}' cannot be null or empty.", nameof(scriptName));
            }

            var componentCount = components.Length;


            for (int i = 0; i < componentCount; i++)
            {
                // TODO: SKIP COMPONENTS THAT HAVE NOT FULLY INITIALIZED

                var name = components[i].Name;
                log.Info($"Executing {name} {scriptName} {i + 1}/{componentCount}");
                var component = (IResourceComponent)GetComponent(components[i]);
                await component.OnExecuteScript(scriptName);
            }  
        } 
         
    }
}