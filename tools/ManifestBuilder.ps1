$items = Get-ChildItem -Path "..\root\data\" -Recurse | %{$_.FullName} 

$manifestPath = "..\root\data\manifest.json" 

if (Test-Path "$manifestPath") {
	Remove-Item "$manifestPath"
}

$json = "{""" + "files" + """" + ":" + "[" 
foreach($item in $items){  
    if(Test-Path -Path $item -PathType Leaf){ 
        $relativePath = Get-Item $item | Resolve-Path -Relative
        $path = """" + $relativePath.Replace("..\root\", "").Replace("\","\\")  + """" + ',' 
        $json += $path
    }
}
$json += "]}"
$json = $json.Replace(",]", "]")
Write-Output $json >> $manifestPath
