artifacts - the leftover files from the build script 
scripts - build and deploy scripts to create the resource
data - data files used in the resource
	common - data files used between components
src - source code for the resource
	components - client code for components loaded in via the resource-framework system 
	games - an entrance to the start of the game under the resource-framework system
	ext - exteneral code projects that are not directly needed for the core system to work
	libs - client specific libraries 
	resources - indivudal resources under the FiveM framework system